﻿using UnityEngine;
using Savings;

namespace General
{
    public class Settings : MonoBehaviour
    {
        [Header("Skins")]
        [SerializeField] private SkinStickman _enemy1StickmanSkin;
        [SerializeField] private SkinStickman _enemy2StickmanSkin;

        [SerializeField] private SkinTank _playerTankSkin;
        [SerializeField] private SkinTank _enemy1TankSkin;
        [SerializeField] private SkinTank _enemy2TankSkin;

        [SerializeField] private TowerSkin _emptyDefaultTower;
        [SerializeField] private TowerSkin _emptyShootingTower;
        [SerializeField] private TowerSkin _emptyAttackingTower;
        [SerializeField] private TowerSkin _emptyDefendingTower;

        [SerializeField] private TowerSkin _enemy1DefaultTower;
        [SerializeField] private TowerSkin _enemy1ShootingTower;
        [SerializeField] private TowerSkin _enemy1AttackingTower;
        [SerializeField] private TowerSkin _enemy1DefendingTower;

        [SerializeField] private TowerSkin _enemy2DefaultTower;
        [SerializeField] private TowerSkin _enemy2ShootingTower;
        [SerializeField] private TowerSkin _enemy2AttackingTower;
        [SerializeField] private TowerSkin _enemy2DefendingTower;

        [Header("Other -------------")]
        [SerializeField] private bool _isDoubleStickman;
        [Tooltip("Player can select all tower")]
        [SerializeField] private bool _isFreePlay;

        private SkinStickman _playerStickmanSkin;
        private TowerSkin _playerDefaultTower;
        private TowerSkin _playerShootingTower;
        private TowerSkin _playerAttackingTower;
        private TowerSkin _playerDefendingTower;
        private SaveMaster _saveMaster;

        public static Settings Instance { get; private set; }
        public bool IsDoubleStickman
        {
            get
            {
                return _isDoubleStickman;
            }
            set
            {
                _isDoubleStickman = value;
            }
        }
        public bool IsFreePlay => _isFreePlay;

        public SkinStickman PlayerStickmanSkin
        {
            get
            {
                return _playerStickmanSkin;
            }

            set
            {
                _playerStickmanSkin = value;
                _saveMaster.SaveFile.SkinStickman = value;
                _saveMaster.SaveData();
            }
        }
        public TowerSkin PlayerDefaultTower
        {
            get
            {
                return _playerDefaultTower;
            }
            set
            {
                _playerDefaultTower = value;
                _saveMaster.SaveFile.DefaultTower = value;
                _saveMaster.SaveData();
            }
        }
        public TowerSkin PlayerShootingTower
        {
            get
            {
                return _playerShootingTower;
            }
            set
            {
                _playerShootingTower = value;
                _saveMaster.SaveFile.ShootingTower = value;
                _saveMaster.SaveData();
            }
        }
        public TowerSkin PlayerAttackingTower
        {
            get
            {
                return _playerAttackingTower;
            }
            set
            {
                _playerAttackingTower = value;
                _saveMaster.SaveFile.AttackingTower = value;
                _saveMaster.SaveData();
            }
        }
        public TowerSkin PlayerDefendingTower
        {
            get
            {
                return _playerDefendingTower;
            }
            set
            {
                _playerDefendingTower = value;
                _saveMaster.SaveFile.DefendingTower = value;
                _saveMaster.SaveData();
            }
        }

        public SkinStickman Enemy1StickmanSkin => _enemy1StickmanSkin;
        public SkinStickman Enemy2StickmanSkin => _enemy2StickmanSkin;

        public SkinTank SelectedPlayerTankSkin => _playerTankSkin;
        public SkinTank SelectedEnemy1TankSkin => _enemy1TankSkin;
        public SkinTank SelectedEnemy2TankSkin => _enemy2TankSkin;

        public TowerSkin EmptyDefaultTower => _emptyDefaultTower;
        public TowerSkin EmptyShootingTower => _emptyShootingTower;
        public TowerSkin EmptyAttackingTower => _emptyAttackingTower;
        public TowerSkin EmptyDefendingTower => _emptyDefendingTower;
        public TowerSkin Enemy1DefaultTower => _enemy1DefaultTower;
        public TowerSkin Enemy1ShootingTower => _enemy1ShootingTower;
        public TowerSkin Enemy1AttackingTower => _enemy1AttackingTower;
        public TowerSkin Enemy1DefendingTower => _enemy1DefendingTower;
        public TowerSkin Enemy2DefaultTower => _enemy2DefaultTower;
        public TowerSkin Enemy2ShootingTower => _enemy2ShootingTower;
        public TowerSkin Enemy2AttackingTower => _enemy2AttackingTower;
        public TowerSkin Enemy2DefendingTower => _enemy2DefendingTower;

        private void Awake()
        {
            Instance = this;
            _saveMaster = SaveMaster.Instance;
            _playerStickmanSkin = _saveMaster.SaveFile.SkinStickman;
            _playerDefaultTower = _saveMaster.SaveFile.DefaultTower;
            _playerShootingTower = _saveMaster.SaveFile.ShootingTower;
            _playerAttackingTower = _saveMaster.SaveFile.AttackingTower;
            _playerDefendingTower = _saveMaster.SaveFile.DefendingTower;
        }
    }
}