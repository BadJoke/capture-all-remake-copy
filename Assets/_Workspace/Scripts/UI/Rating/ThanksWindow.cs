﻿using UnityEngine;

namespace UI.Rating
{
    public class ThanksWindow : MonoBehaviour
    {
        [SerializeField] private string _showAnimation;
        [SerializeField] private string _hideAnimation;
        [SerializeField] private Animation _animation;

        public void Show()
        {
            gameObject.SetActive(true);
            _animation.Play(_showAnimation);
        }

        public void Hide()
        {
            _animation.Play(_hideAnimation);
        }

        private void SetDeactive()
        {
            gameObject.SetActive(false);
        }
    }
}