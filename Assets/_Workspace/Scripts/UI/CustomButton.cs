﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CustomButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    [SerializeField] private Sprite _clickedSprite;
    [SerializeField] private float _pressingDepth = 6f;
    [SerializeField] private bool _isActive;

    [SerializeField] private GameObject _blackoutPanel;
    [SerializeField] private GameObject _objectWhenAtivated;
    [SerializeField] private GameObject _objectWhenDeativated;

    [Space(15f)]
    [SerializeField] private UnityEvent _actions;

    private Image _buttonImage;
    private Sprite _defaultSprite;
    private RectTransform _selfRTransform;
    private Animator _animator;

    public event Action Activated;
    public event Action Deactivated;
    
    private void Awake()
    {
        _selfRTransform = GetComponent<RectTransform>();
        _buttonImage = GetComponent<Image>();
        _animator = GetComponent<Animator>();
        
        _defaultSprite = _buttonImage.sprite;
    }

    public void OnPointerClick(PointerEventData pointerData)
    {
        if (_isActive == false)
        {
            return;
        }
        
        _actions?.Invoke();
    }

    public void OnPointerDown(PointerEventData pointerData)
    {
        if (_isActive == false)
        {
            return;
        }
        
        if (_animator)
        {
            _animator.enabled = false;
        }

        _selfRTransform.Translate(Vector2.down * _pressingDepth);

        if (_clickedSprite)
        {
            _buttonImage.sprite = _clickedSprite;
        }
    }

    public void OnPointerUp(PointerEventData pointerData)
    {
        if (_isActive == false)
        {
            return;
        }
        
        if (_animator)
        {
            _animator.enabled = true;
        }

        _selfRTransform.Translate(Vector2.up * _pressingDepth);

        if (_clickedSprite)
        {
            _buttonImage.sprite = _defaultSprite;
        }
    }

    public void Deactivate()
    {
        if (_blackoutPanel)
            _blackoutPanel.SetActive(true);
        
        if (_objectWhenAtivated) 
            _objectWhenAtivated.SetActive(false);

        if (_objectWhenDeativated)
            _objectWhenDeativated.SetActive(true);

        _isActive = false;
        Deactivated?.Invoke();
    }

    public void Activate()
    {
        if (_blackoutPanel)
            _blackoutPanel.SetActive(false);

        if (_objectWhenAtivated)
            _objectWhenAtivated.SetActive(true);

        if (_objectWhenDeativated)
            _objectWhenDeativated.SetActive(false);

        _isActive = true;
        Activated?.Invoke();
    }
}