﻿using System;
using UI.Boosts;
using Units;
using UnityEngine;
using UnityEngine.UI;

namespace Towers
{
    [RequireComponent(typeof(TowerLevelManager))]
    public class TowerPower : MonoBehaviour
    {
        [SerializeField, Range(0, 70)] private int _startValue = 10;
        [SerializeField, Range(1, 70)] private int _maxValue = 70;
        [SerializeField] private Text _amountText;
        [SerializeField] private AdditionalPower _additionalPower;

        private bool _isNotMax;

        private const string MaxText = "MAX";

        public event Action<int> Changed;
        public event Action<Fraction> FractionChanged;
        public event Action MaxValueReached;

        public int Amount { get; private set; }

        private void Awake()
        {
            Amount = _startValue;
            _isNotMax = true;
            _amountText.text = _startValue.ToString();
        }

        protected void Start()
        {
            Changed?.Invoke(Amount);
        }

        public void ApplyUnit(IDamager damager, Fraction towerFraction)
        {
            if (damager.Fraction == towerFraction)
            {
                int damage = damager.Damage;

                if (damager.Type == TowerType.Defending)
                {
                    damage *= 2;
                }

                Increase(damage);
            }
            else
            {
                int damage = CheckDamage(damager);

                Decrease(damage, damager.Fraction);
            }
        }

        public void AddPower(int value)
        {
            Increase(value);

            _additionalPower.Init(value);
        }
        
        protected void Increase()
        {
            Increase(1);
        }

        private void Increase(int value)
        {
            Amount += value;

            if (Amount < _maxValue)
            {
                _amountText.text = Amount.ToString();
            }
            else
            {
                if (Amount > _maxValue)
                {
                    Amount = _maxValue;
                }

                if (_isNotMax)
                {
                    _isNotMax = false;
                    _amountText.text = MaxText;

                    MaxValueReached?.Invoke();
                }
            }

            Changed?.Invoke(Amount);
        }

        private void Decrease(int value, Fraction fraction)
        {
            Amount -= value;
            _isNotMax = true;

            if (Amount < 1)
            {
                Amount = 1 - Amount;
                FractionChanged?.Invoke(fraction);
            }

            _amountText.text = Amount.ToString();
            Changed?.Invoke(Amount);
        }

        private int CheckDamage(IDamager damager)
        {
            int damage = damager.Damage;

            switch (damager.Type)
            {
                case TowerType.Attacking:
                    damage *= 2;
                    break;
                case TowerType.Shooting:
                    if (Amount <= damage && damager.Fraction == Fraction.Empty)
                    {
                        damage = 0;
                    }
                    break;
            }

            return damage;
        }
    }
}