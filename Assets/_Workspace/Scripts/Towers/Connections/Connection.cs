﻿using Gates;
using General;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Towers.Connections
{
    [RequireComponent(typeof(BoxCollider))]
    public class Connection : MonoBehaviour
    {
        [SerializeField] private LineRenderer _line;
        [SerializeField] private ParticleSystem _arrows;
        [SerializeField] private BoxCollider _collider;

        private float _targetOffset = 0.001f;
        private Connection _pair;
        private Gate[] _gates;

        public event Action<Connection> Deactivated;

        public UnitTower Owner { get; private set; }
        public Tower Target { get; private set; }
        public Gate[] Gates => _gates;
        public bool IsFriendly => Owner.CurrentFraction == Target.CurrentFraction;

        private void OnDestroy()
        {
            if (_pair)
            {
                _pair.Deactivated -= OnPairDeativated;
            }
        }

        public void Init(UnitTower owner, Tower target)
        {
            Owner = owner;
            Target = target;
            FindGates(owner, target);

            Vector3 startPosition = Owner.transform.position;
            float distance = Vector3.Distance(startPosition, target.transform.position);

            transform.position = startPosition;
            transform.LookAt(target.transform);

            SetSize(distance);
        }

        private void FindGates(UnitTower owner, Tower target)
        {
            Vector3 origin = owner.transform.position;
            Vector3 direction = target.transform.position - origin;
            float rayLength = direction.magnitude;
            RaycastHit[] hits = Physics.RaycastAll(origin, direction.normalized, rayLength);
            List<Gate> gates = new List<Gate>();

            foreach (var hit in hits)
            {
                if (hit.transform.TryGetComponent(out Gate gate))
                {
                    gates.Add(gate);
                }
            }

            _gates = gates.ToArray();
        }

        public void SetPair(Connection pair)
        {
            _pair = pair;

            Resize();
        }

        public void Deactivate()
        {
            _line.SetPosition(1, Vector3.zero);
            _arrows.Stop();
            _arrows.Clear();

            Deactivated?.Invoke(this);
            Destroy(gameObject);
        }

        private void SetSize(float distance)
        {
            SetLine(Owner.CurrentFraction, distance);
            SetCollider(Owner.CurrentFraction, distance);
            SetArrows(distance);
        }

        private void Resize()
        {
            float distance = _line.GetPosition(1).z;

            if (_pair)
            {
                distance *= 0.5f;
                _pair.Deactivated += OnPairDeativated;
            }
            else
            {
                distance *= 2f;
            }

            SetSize(distance);
        }

        private void SetLine(Fraction fraction, float distance)
        {
            _line.material = ColorManager.Instance.GetLineMaterial(fraction);
            _line.SetPosition(0, Vector3.zero);
            _line.SetPosition(1, new Vector3(0f, _targetOffset, distance));
        }

        private void SetArrows(float distance)
        {
            ParticleSystem.MainModule main = _arrows.main;
            float last = main.startLifetimeMultiplier;
            main.startLifetimeMultiplier = distance / main.startSpeedMultiplier;

            if (last != main.startLifetimeMultiplier)
            {
                _arrows.Stop();
            }

            _arrows.Play();
        }

        private void SetCollider(Fraction fraction, float distance)
        {
            if (fraction != Fraction.Player && Settings.Instance.IsFreePlay == false)
            {
                _collider.enabled = false;
                return;
            }

            Vector3 size = new Vector3(_line.startWidth, 1f, distance);
            _collider.enabled = true;
            _collider.center = new Vector3(0f, 0.1f, distance / 2f);
            _collider.size = size;
        }

        private void OnPairDeativated(Connection pair)
        {
            _pair.Deactivated -= OnPairDeativated;
            _pair = null;

            Resize();
        }
    }
}