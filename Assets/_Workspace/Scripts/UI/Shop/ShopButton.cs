﻿using UnityEngine;

namespace UI.Shop
{
    public class ShopButton : MonoBehaviour
    {
        [SerializeField] private BuyButton _buyButton;
        [SerializeField] private SelectButton _selectButton;

        private void Start()
        {
            _selectButton.SetActive(true, true);
            _buyButton.SetActive(false, 0, AdRewardType.skin_1);
        }

        public void SetModel(PreviewModel model)
        {
            bool isOpen = model.IsOpen();

            _selectButton.SetActive(isOpen, model.IsCurrent());
            _buyButton.SetActive(!isOpen, model ? model.Cost : 0, model.RewardType);
        }
    }
}