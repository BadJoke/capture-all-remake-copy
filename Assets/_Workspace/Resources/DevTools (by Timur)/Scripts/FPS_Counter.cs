using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FPS_Counter : MonoBehaviour
{
    [Range (1, 200)]
    [SerializeField] private int _samplingBorders = 60;
    [Header("Max fps:")]
    [Tooltip("Write -1 if you dont't want to set max fps")]
    [SerializeField] private int _targetFps = 60;
    [SerializeField] private GraphUI _graph;
    [SerializeField] private Text _textComponent;


    public int CurrentFps => _currentFps;

    private float _time;
    private int _frames;
    private int _currentFps;
    private List<int> _savedFps;

    private void Start()
    {
        // Applying Max FPS
        _savedFps = new List<int>();
        if(_targetFps > 0)
            Application.targetFrameRate = _targetFps;
    }

    private void Update()
    {
        _time += Time.deltaTime;
        _frames++;
        if (_frames >= _samplingBorders)
        {
            // Applying fps value
            _currentFps = (int)(_frames / _time);
            _savedFps.Add(_currentFps);

            // Visualize
            if(_textComponent != null)
                _textComponent.text = _currentFps.ToString();
            if (_graph != null)
            {
                // Recontructing lists to send to Graph:

                List<float> newFpsSave = new List<float>();
                if (_savedFps.Count > _graph.MaxSamples)
                {
                    for (int i = 0; i < _graph.MaxSamples; i++)
                    {
                        newFpsSave.Add(_savedFps[_savedFps.Count -1 - i]);
                    }
                    _graph.SetValues(newFpsSave.ToArray());
                }
                else
                {
                    for (int i = 0; i < _savedFps.Count; i++)
                    {
                        newFpsSave.Add(_savedFps[_savedFps.Count -1 - i]);
                    }
                    _graph.SetValues(newFpsSave.ToArray());
                }
            }

            // Resetting
            _frames = 0;
            _time = 0;
        }
    }
}
