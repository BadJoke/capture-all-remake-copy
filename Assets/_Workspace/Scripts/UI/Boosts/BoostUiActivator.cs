﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Boosts
{
    public class BoostUiActivator : MonoBehaviour
    {
        [SerializeField] private float _cooldown = 10f;
        [SerializeField] private Text _countDisplay;
        [SerializeField] private Image _darker;
        [SerializeField] private Boost _boost;
        [SerializeField] private Advertizer _advertizer;
        [SerializeField] private GameObject _adObject;

        private int _count;
        private CustomButton _button;

        public event Action<AdRewardType> Used;
        public event Action<AdRewardType> Added;

        public AdRewardType BoostType => _boost.RewardType;

        private void Start()
        {
            _button = GetComponent<CustomButton>();
            
            CheckCount();
        }

        public void SetCount(int value)
        {
            _count = value;
            _countDisplay.text = value.ToString();
        }

        public void OnClick()
        {
            if (_count == 0)
            {
                _adObject.SetActive(false);
                _advertizer.BoostTaken += OnBoostTaken;
                _advertizer.ShowBoostAd(_boost.RewardType);
                _button.Deactivate();
            }
            else
            {
                _countDisplay.text = (--_count).ToString();
                Use();
                Used?.Invoke(_boost.RewardType);   
            }
        }

        private void CheckCount()
        {
            if (_count != 0)
            {
                return;
            }

            _adObject.SetActive(true);
            _advertizer.BoostAdOffer(_boost.RewardType);
        }
        
        private void Use()
        {
            _darker.fillAmount = 1f;

            _boost.Activate();
            _button.Deactivate();
            _advertizer.RaiseConsumableEvent(_boost.RewardType);

            StartCoroutine(CooldownRoutine());
        }

        private IEnumerator CooldownRoutine()
        {
            float timer = 0f;

            while (timer < _cooldown)
            {
                yield return null;

                timer += Time.deltaTime;
                _darker.fillAmount = 1f - timer / _cooldown;
            }
            
            CheckCount();

            _button.Activate();
            _darker.fillAmount = 1f;
        }

        private void OnBoostTaken(AdRewardType rewardType)
        {
            if (rewardType != _boost.RewardType)
            {
                return;
            }
            
            _advertizer.BoostTaken -= OnBoostTaken;
            _button.Activate();
            
            Added?.Invoke(rewardType);
        }
    }
}