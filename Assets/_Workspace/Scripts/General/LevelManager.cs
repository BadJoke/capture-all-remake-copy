﻿using Savings;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace General
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] private int _level = 0;

        private int _currentLevel = 1;
        private int _currentTestLevel = 0;
        private SaveMaster _saveMaster;

        public int CurrentLevel => IsTestLaunch ? _currentTestLevel : _currentLevel;
        private bool IsTestLaunch => _level > 0;

        public static LevelManager Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            _saveMaster = SaveMaster.Instance;

            if (IsTestLaunch)
            {
                _currentTestLevel = _level;
            }
            else
            {
                _currentLevel = _saveMaster.SaveFile.CurrentLevel + 1;
            }

            LoadLevel();
        }

        public void LoadLevel()
        {
            if (_level < 0)
            {
                _saveMaster.SaveFile.CurrentLevel = 1;
                _saveMaster.SaveData();
            }

            SceneManager.LoadScene(SelectLevel(CurrentLevel));
        }

        public void SaveProgress()
        {
            if (IsTestLaunch)
            {
                _currentTestLevel++;
            }
            else
            {
                _saveMaster.SaveFile.CurrentLevel = ++_currentLevel - 1;
                _saveMaster.SaveData();
            }
        }

        private int SelectLevel(int number)
        {
            int gameScenesCount = SceneManager.sceneCountInBuildSettings - 1;

            if (number > gameScenesCount)
            {
                Random.InitState(number - gameScenesCount);
                return Random.Range(2, gameScenesCount + 1);
            }
            else
            {
                return number;
            }
        }
    }
}