﻿using UnityEngine;

namespace UI
{
    public class TutorialScreen : MonoBehaviour
    {
        [SerializeField] private Animation _animation;

        public void Activate()
        {
            gameObject.SetActive(true);
            _animation.Play();
        }

        public void Deactivate()
        {
            _animation.Stop();
            gameObject.SetActive(false);
        }
    }
}