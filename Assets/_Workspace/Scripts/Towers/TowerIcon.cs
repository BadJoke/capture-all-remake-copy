﻿using DG.Tweening;
using UnityEngine;

namespace Towers
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class TowerIcon : MonoBehaviour
    {
        [SerializeField] private float _shiftPosition;
        
        private Sequence _pulsation;

        public void Show(bool hasKey)
        {
            if (hasKey)
            {
                transform.DOLocalMoveY(7f, 0.25f);
            }

            DOTween.Sequence()
                .Append(transform.DOScale(1.2f, 0.2f))
                .Append(transform.DOScale(1f, 0.05f))
                .onComplete += () =>
                {
                    _pulsation = DOTween.Sequence()
                        .Append(transform.DOScale(0.9f, 0.25f))
                        .Append(transform.DOScale(1.1f, 0.5f))
                        .Append(transform.DOScale(1f, 0.25f))
                        .SetLoops(-1);
                };
        }

        public void Hide()
        {
            _pulsation?.Kill();

            transform.DOLocalMoveY(4f, 0.25f);

            DOTween.Sequence()
                .Append(transform.DOScale(1f, 0.05f))
                .Append(transform.DOScale(0f, 0.2f));
        }

        public void Down()
        {
            transform.DOLocalMoveY(4f, 0.25f);
        }

        public void Shift()
        {
            transform.DOLocalMoveX(_shiftPosition, 0.1f);
        }

        public void Unshift()
        {
            transform.DOLocalMoveX(0f, 0.1f);
        }
    }
}