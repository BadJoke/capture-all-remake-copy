using FUGames.Pooling;
using General;
using Towers;
using UnityEngine;
using DeathEffects;

namespace Units
{
    public class Tank : Unit
    {
        [SerializeField] private UnitSkin _skin;

        private UnitSkin Skin => _skin;

        public override void Multiply(int multiplier)
        {
            if (multiplier < 10)
            {
                _skin.SetRelativePosition(0);
                
                for (int i = 1; i < multiplier; i++)
                {
                    Tank newOne = PoolManager.Instance.Take<Tank>();
                    float relativePos = 0;
                    
                    if (i % 2 == 1 && i != multiplier - 1)
                    {
                        if (i % 2 == 1)
                            relativePos = UnitRadius;
                        else
                            relativePos = -UnitRadius;
                    }
                    
                    Vector3 pos = transform.position - (transform.forward * (i+1) / 2) * UnitRadius;
                    newOne.InitializeClone(this, pos,true);
                    newOne.Skin.SetRelativePosition(relativePos);
                }
            }
            else
            {
                for (int i = 1; i < multiplier; i++)
                {
                    Tank newOne = PoolManager.Instance.Take<Tank>();
                    float relativePos = Mathf.Lerp(-UnitRadius, UnitRadius, Random.Range(0, 1f)) * 1.5f;
                    Vector3 pos = transform.position - (transform.forward * i / 2) * UnitRadius;
                    newOne.InitializeClone(this, pos, true);
                    newOne.Skin.SetRelativePosition(relativePos);
                }
            }
        }

        protected override void SpecialInitialize()
        {
            _skin.SetScale(0);
            _skin.SetScaleAnimated(1, Random.Range(0.2f, 0.4f));
            _skin.ForceClearRelativePosition();
            _skin.UpdateColor(Fraction);
            _skin.RefreshSkin(this);
        }

        private void PlayDeathEffect()
        {
            DeathEffect deathEffect = PoolManager.Instance.Take<TankDE>();
            deathEffect.StartCountdown();
            deathEffect.transform.position = transform.position + Vector3.up;
            var main = deathEffect.PS.main;
            main.startColor = ColorManager.Instance.GetColorKit(Fraction).Unit.color;
        }

        public override void TakeDamage(IDamager damager)
        {
            if (damager.Fraction != Fraction)
            {
                Lives -= damager.Damage;
                if (Lives < 1)
                {
                    Die(true);
                }
            }
        }

        public override void Die(bool withEffects)
        {
            if (IsDied)
                return;
            IsDied = true;
            //������
            {
                //�������
                if (withEffects)
                    PlayDeathEffect();
                //����������� � ���
                PoolManager.Instance.Put(this);

            }
            CallDiedEvent();
        }
    }
}
