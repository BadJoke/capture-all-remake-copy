using UnityEngine;
using UnityEngine.UI;
using Savings;
using General;

namespace UI
{
    public class FinishPanel : MonoBehaviour
    {
        [SerializeField] private Advertizer _advertizer;
        [SerializeField] private Text _collectedCoinsText;
        [SerializeField] private Text _levelNumberText;
        [SerializeField] private Wallet _wallet;
        [SerializeField] private Animation _anim;
        [SerializeField] private Button _noThanksButton;
        [SerializeField] private CustomButton _loseClaimButton;
        [SerializeField] private CustomButton _winClaimButton;

        private int _collectedCoins;
        private bool _win;
        private SaveMaster _saveMaster;

        private void OnEnable()
        {
            _advertizer.RewardDoubled += OnRewardDoubled;
        }

        private void Start()
        {
            _saveMaster = SaveMaster.Instance;
        }

        private void OnDisable()
        {
            _advertizer.RewardDoubled -= OnRewardDoubled;
        }

        public void WatchAdDoubleCoins()
        {
            DeactivateButtons();

            _noThanksButton.interactable = true;
            _advertizer.ShowDoubleCoinsAd();
        }

        public void ClaimAndLoad()
        {
            if(_saveMaster.SaveFile.CurrentLevel != 1)
            {
                _advertizer.ShowInterstitialAd();
            }

            ForceLoad();
        }

        public void ShowUIInfo(bool win)
        {
            _win = win;

            int lvl = _saveMaster.SaveFile.CurrentLevel - 1;
            if(lvl == 0)
                RealizeUIActionsBasedOnLevelResult(false);
            else
                RealizeUIActionsBasedOnLevelResult(win);

            if (win)
                _collectedCoins = Einstein.CalculateLevelCoinsRewardWhenWin(_saveMaster.SaveFile.CurrentLevel - 1);
            else
                _collectedCoins = Einstein.CalculateLevelCoinsRewardWhenLose(_saveMaster.SaveFile.CurrentLevel - 1);

            DisplayUIInfo(win);

            _advertizer.AdOfferCoinsX2();
        }

        public void ClaimDublicatedCoins()
        {
            _noThanksButton.interactable = false;
            _collectedCoins *= 2;
            ForceLoad();
        }

        private void RealizeUIActionsBasedOnLevelResult(bool win)
        {
            _anim.Play();
            _noThanksButton.gameObject.SetActive(win);
            _loseClaimButton.gameObject.SetActive(!win);
            _winClaimButton.gameObject.SetActive(win);
        }

        private void DisplayUIInfo(bool win)
        {
            int lvl = _saveMaster.SaveFile.CurrentLevel - 1;

            if (lvl == 0)
                _levelNumberText.text = "TUTORIAL";
            else
                _levelNumberText.text = "LEVEL " + lvl.ToString();

            if (win)
                _levelNumberText.text += "\nCOMPLETED!";
            else
                _levelNumberText.text += "\nFAILED";

            _collectedCoinsText.text = _collectedCoins.ToString();

            _saveMaster.SaveFile.Money += _collectedCoins;
            _saveMaster.SaveData();
        }

        private void ForceLoad()
        {
            DeactivateButtons();
            _wallet.ChangeCoinsCount(_collectedCoins, true);
            Invoke(nameof(LoadNextLevel), 2);
        }

        private void LoadNextLevel()
        {
            LevelManager.Instance.LoadLevel();
        }        

        private void DeactivateButtons()
        {
            _noThanksButton.interactable = false;
            _loseClaimButton.Deactivate();
            _winClaimButton.Deactivate();
        }

        private void OnRewardDoubled()
        {
            ClaimDublicatedCoins();
        }
    }
}

