﻿using General;
using Savings;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Shop
{
    public class UnitButton : PreviewButton
    {
        [SerializeField] private SkinStickman _skinStickman;
        [SerializeField] private GameObject _skin;

        public GameObject Skin => _skin;
        public SkinStickman SkinStickman => _skinStickman;

        public override bool IsOpen()
        {
            return SaveMaster.Instance.SaveFile.OpenedStickmanSkins[(int)_skinStickman];
        }
    }
}