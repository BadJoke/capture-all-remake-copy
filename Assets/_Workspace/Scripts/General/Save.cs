﻿using General;
using System;

namespace Savings
{
    [Serializable]
    public class Save
    {
        public int Money;
        public int CurrentLevel; // 0 - туториал, 1 - уровень 1 и т.д.
        public int RateShowingLevel;
        public int[] ImprovedLevels;
        public string LastEnterGameDate;

        public SkinStickman SkinStickman;
        public bool[] OpenedStickmanSkins;

        public TowerSkin DefaultTower;
        public bool[] OpenedDefaultTowerSkins;

        public TowerSkin ShootingTower;
        public bool[] OpenedShootingTowerSkins;

        public TowerSkin AttackingTower;
        public bool[] OpenedAttackingTowerSkins;

        public TowerSkin DefendingTower;
        public bool[] OpenedDefendingTowerSkins;

        public bool IsNotRated;

        public int FreezeBoosts;
        public int SpeedUpBoosts;
        public int PointsBoosts;

        public Save()
        {
            Money = 0;
            RateShowingLevel = 8;
            ImprovedLevels = new[] { 1, 1, 1 };

            SkinStickman = SkinStickman.Default;
            OpenedStickmanSkins = new bool[Enum.GetValues(typeof(SkinStickman)).Length];
            OpenedStickmanSkins[0] = true;

            int towerSkinCount = Enum.GetValues(typeof(TowerSkin)).Length;

            DefaultTower = TowerSkin.Default;
            OpenedDefaultTowerSkins = new bool[towerSkinCount];
            OpenedDefaultTowerSkins[0] = true;

            ShootingTower = TowerSkin.Default;
            OpenedShootingTowerSkins = new bool[towerSkinCount];
            OpenedShootingTowerSkins[0] = true;

            AttackingTower = TowerSkin.Default;
            OpenedAttackingTowerSkins = new bool[towerSkinCount];
            OpenedAttackingTowerSkins[0] = true;

            DefendingTower = TowerSkin.Default;
            OpenedDefendingTowerSkins = new bool[towerSkinCount];
            OpenedDefendingTowerSkins[0] = true;

            IsNotRated = true;
            
            FreezeBoosts = 1;
            SpeedUpBoosts = 1;
            PointsBoosts = 1;
        }
    }
}

