using DG.Tweening;
using UnityEngine;

namespace UI.Boosts
{
    public class BoostBuyersPanel : MonoBehaviour
    {
        [SerializeField] private float _xPosition = 190;
        [SerializeField] private float _duration;
        
        public void Show()
        { 
            gameObject.SetActive(true);

            ((RectTransform) transform).DOAnchorPosX(-_xPosition, _duration);
        }
        
        public void Hide()
        {
            ((RectTransform) transform).DOAnchorPosX(_xPosition, _duration)
                .onComplete += () => {  gameObject.SetActive(false); };
        }
    }
}