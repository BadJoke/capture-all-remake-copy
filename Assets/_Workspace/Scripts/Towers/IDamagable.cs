﻿using Units;
using UnityEngine;

namespace Towers
{
    public interface IDamagable
    {
        public float Radius { get; }
        public Transform Transform { get; }
        void TakeDamage(IDamager damager);
    }
}