﻿using DG.Tweening;
using UnityEngine;

namespace UI.Shop
{
    public class PanelSwiper : MonoBehaviour
    {
        [SerializeField] private float _time = 0.2f;
        [SerializeField] private int _setCount = 3;
        [SerializeField] private GameObject _leftArrow;
        [SerializeField] private GameObject _rightArrow;

        private float _position;
        private RectTransform _rect;

        private const float Offset = 1080f;

        private void Start()
        {
            _position = 0f;
            _rect = transform as RectTransform;

            _leftArrow.SetActive(false);
            _rightArrow.SetActive(true);
        }

        public void LeftSwipe()
        {
            Swipe(Offset);
        }

        public void RightSwipe()
        {
            Swipe(-Offset);
        }

        private void Swipe(float offset)
        {
            _position += offset;

            if (_position >= 0)
            {
                _leftArrow.SetActive(false);
                _rightArrow.SetActive(true);
            }
            else if (_position <= (_setCount - 1) * -Offset)
            {
                _leftArrow.SetActive(true);
                _rightArrow.SetActive(false);
            }
            else
            {
                _leftArrow.SetActive(true);
                _rightArrow.SetActive(true);
            }


            _rect.DOAnchorPosX(_position, _time);
        }
    }
}