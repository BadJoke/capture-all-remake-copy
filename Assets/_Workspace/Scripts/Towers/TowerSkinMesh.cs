﻿using System;
using UnityEngine;

namespace Towers
{
    [Serializable]
    public class TowerSkinMesh
    {
        [SerializeField] private Mesh _mesh;
        [SerializeField] private int _fractionIndex;

        public Mesh Mesh => _mesh;
        public int FractionIndex => _fractionIndex;
        public int TextureIndex => _fractionIndex == 0 ? 1 : 0;
    }
}