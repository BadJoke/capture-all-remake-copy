﻿using UnityEngine;

public class BannerActivator : MonoBehaviour
{
    private void Start()
    {
        Invoke(nameof(Show), 0.5f);
    }

    private void Show()
    {
        HoopslyIntegration.Instance.ShowBanner();
    }
}