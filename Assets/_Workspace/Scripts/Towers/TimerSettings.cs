﻿using Improvements;
using Savings;
using UnityEngine;

namespace Towers
{
    [CreateAssetMenu(fileName = "TimerSettings", menuName = "Building/Timer Settings")]
    public class TimerSettings : ScriptableObject
    {
        [SerializeField] private float _minPlayerDelay;
        [SerializeField] private float _maxPlayerDelay;
        [SerializeField] private float _enemyTickRate;
        [SerializeField] private float _minEnemyDelay;
        [SerializeField] private float _maxEnemyDelay;
        [SerializeField] private float _shootingDelay = 1f;
        [SerializeField] private float _targetingMinDelay;
        [SerializeField] private float _targetingMaxDelay;

        private SaveMaster _saveMaster;

        private const float RateModifier = 0.005f;

        public float EnemyTargetingDelay => Random.Range(_targetingMinDelay, _targetingMaxDelay);
        private float PlayerTickRate => 1f / (1f + _saveMaster.SaveFile.ImprovedLevels[0] * RateModifier);

        public float GetDelay(Fraction fraction,int power)
        {
            float delay = Einstein.CalculateUnitSpawnPeriod(Improver.Instance.GetImprovementLevel(ImprovementType.SpawnRate), power, fraction);

            return fraction == Fraction.Player ? CheckPlayerDelay(delay) : CheckEnemyDelay(delay);
        }

        public void LoadPlayerRate()
        {
            _saveMaster = SaveMaster.Instance;
        }

        private float CheckPlayerDelay(float delay)
        {
            return Mathf.Clamp(delay, _minPlayerDelay, _maxPlayerDelay);
        }

        private float CheckEnemyDelay(float delay)
        {
            return Mathf.Clamp(delay, _minEnemyDelay, _maxEnemyDelay);
        }
    }
}