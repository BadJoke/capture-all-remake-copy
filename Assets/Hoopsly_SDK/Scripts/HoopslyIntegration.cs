﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Firebase;
using Firebase.Analytics;
using Facebook.Unity;
using Hoopsly.Events;

public class HoopslyIntegration : MonoBehaviour
{
    private static HoopslyIntegration instance;
    public static HoopslyIntegration Instance
    {
        get { return instance; }
    }
    private AppsFlyerObjectScript m_appsFlyerIntegrationManager;
    private string m_uuid;

    private int interstitialRetryAttempt;
    private int rewardedRetryAttempt;
    private bool isBannerShowing;
    private bool isMrecShowing;

    private bool m_isFirebaseInitilized = false;
    //private Queue<FirebaseEvent> m_firebaseEventQueue = new Queue<FirebaseEvent>();
    private Queue<HoopslyAnalicsEvent> m_firebaseEventQueue = new Queue<HoopslyAnalicsEvent>();


    private AdRewardType m_currentRewardType = AdRewardType.other;

    public event Action<string> m_OnInterstitialLoadedEvent = delegate { };
    public event Action<string, MaxSdkBase.ErrorInfo> m_OnInterstitialFailedEvent = delegate { };
    public event Action<string, MaxSdkBase.ErrorInfo, MaxSdkBase.AdInfo> m_OnInterstitialFailedToDisplayEvent = delegate { };
    public event Action<string, MaxSdkBase.AdInfo> m_OnInterstitialDisplayedEvent = delegate { };
    public event Action<string, MaxSdkBase.AdInfo> m_OnInterstitialClickedEvent = delegate { };
    public event Action<string> m_OnInterstitialDismissedEvent = delegate { };

    public event Action<string, MaxSdkBase.AdInfo> m_OnRewardedAdLoadedEvent = delegate { };
    public event Action<string, MaxSdkBase.ErrorInfo> m_OnRewardedAdLoadFailedEvent = delegate { };
    public event Action<string, MaxSdkBase.ErrorInfo, MaxSdkBase.AdInfo> m_OnRewardedAdFailedToDisplayEvent = delegate { };
    public event Action<string, MaxSdkBase.AdInfo> m_OnRewardedAdDisplayedEvent = delegate { };
    public event Action<string, MaxSdkBase.AdInfo> m_OnRewardedAdClickedEvent = delegate { };
    public event Action<string, MaxSdkBase.AdInfo> m_OnRewardedAdClosedEvent = delegate { };
    public event Action<string, MaxSdk.Reward, MaxSdkBase.AdInfo, AdRewardType> m_OnRewardedAdReceivedRewardEvent = delegate { };

    private FPS_MeasureTool measureTool;

    private UnityAction<string> OnUpdateLevelIdUserProperty;


    #region Unity methods
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        AddFpsMeasureTool();
        OnUpdateLevelIdUserProperty += RaiseLevelIdUserProperety;
    }

    private void AddFpsMeasureTool()
    {
        measureTool = gameObject.AddComponent<FPS_MeasureTool>();
        measureTool.MeasureInterval = HoopslySettings.Instance.FPSmeasueIntervals;
    }

    private void Start()
    {
        Debug.Log($"==========[Hoopsly_sdk_ver: {HoopslySettings.Instance.SdkVersion}]==========");
        m_uuid = GetOrGenerateUUID();
        InitSequence();
    }

    private void InitSequence()
    {
        Debug.Log(Application.platform);
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            if(HoopslySettings.Instance.UseApplovin)
                InitApplovin(m_uuid);
        }
        else if(Application.platform == RuntimePlatform.Android)
        {
            if (HoopslySettings.Instance.UseApplovin)
                InitApplovin(m_uuid);

            InitFirebase(m_uuid);

            if (HoopslySettings.Instance.UseAppsflyer)
                InitAppsFlyer(m_uuid);

            if (HoopslySettings.Instance.UseFacebook)
                InitFacebookSDK();
        }
    }


    private void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            if(HoopslySettings.Instance.UseFacebook)
            {
                if (FB.IsInitialized)
                    FB.ActivateApp();
                else
                {
                    FB.Init(() =>
                    {
                        FB.ActivateApp();
                    });
                }
            }
        }
    }
    #endregion

    #region UUID get or generate
    private string GetOrGenerateUUID()
    {
        string uuid = "";
        if (!PlayerPrefs.HasKey("UUID"))
        {
            uuid = Guid.NewGuid().ToString();
            PlayerPrefs.SetString("UUID", uuid);
        }
        else
        {
            uuid = PlayerPrefs.GetString("UUID");
        }
        return uuid;
    }
    #endregion

    #region Firebase
    private void InitFirebase(string uuid)
    {
        Debug.Log("==========[FIREBASE_INIT]==========");
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                FirebaseAnalytics.SetUserId(uuid);
                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                m_isFirebaseInitilized = true;
                Debug.Log("==========[FIREBASE_INIT_COMPLETE]==========");
                RaiseInitEvent();
                //SendAllDelayedEvents();
                DispatchEventsInQuque();
            }
            else
            {
                Debug.LogError(System.String.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
            }
        });
    }


    private void DispatchEventsInQuque()
    {
        Debug.Log($"===[In queue detected: {m_firebaseEventQueue.Count.ToString()} events]===");
        if (!m_isFirebaseInitilized) { Debug.Log("===[Dispatch delayed! Firebase initialization incomplite.]==="); return; }
        while (m_firebaseEventQueue.Count > 0)
        {
            HoopslyAnalicsEvent hoopslyEvent = m_firebaseEventQueue.Dequeue();
            if (hoopslyEvent == null)
            {
                Debug.Log("======[EVENT IS NULL! SKIPPING TO NEXT!]======");
            }
            else
            {
                Debug.Log($"======[DISPATCHING EVENT: {hoopslyEvent.EventName}]======");
                hoopslyEvent.Dispatch();
            }
        }
    }

    private void RaiseInitEvent()
    {
        if (PlayerPrefs.HasKey("InitSended")) { return; }
        PlayerPrefs.SetInt("InitSended", 1);
        FirebaseAnalytics.LogEvent("Init", GenerateInitParameterArray());
        Debug.Log("===[Firebase: Init was logged]===");
    }


    private Parameter[] GenerateInitParameterArray()
    {
        List<Parameter> parameters = new List<Parameter>();
        parameters.Add(new Parameter("GPU", SystemInfo.graphicsDeviceName.ToString()));
        parameters.Add(new Parameter("CPU", SystemInfo.processorType.ToString()));
        parameters.Add(new Parameter("RAM", SystemInfo.systemMemorySize));
        parameters.Add(new Parameter("screen_res_x", Screen.width));
        parameters.Add(new Parameter("screen_res_y", Screen.height));
        parameters.Add(new Parameter("hoopsly_sdk_ver", HoopslySettings.Instance.SdkVersion));
#if UNITY_IOS
        parameters.Add(new Parameter("idfv", UnityEngine.iOS.Device.vendorIdentifier));
#endif
        return parameters.ToArray();
    }

    private void RaiseLevelIdUserProperety(string levelId)
    {
        Debug.Log("=========[LOG_USER_PROPERTY_UPDATE]=========");
        FirebaseAnalytics.SetUserProperty("level_id", levelId);
    }


    private void RaiseAdAttemptEvent(AdRewardType adRewardType)
    {
        m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("ad_attempt", new HoopslyEventParameter("reward_type", adRewardType.ToString())));
        DispatchEventsInQuque();
        //FirebaseAnalytics.LogEvent("ad_attempt", new Parameter("reward_type", adRewardType.ToString()));
    }

    private void RaiseAdWatchedEvent(MaxSdkBase.AdInfo adInfo, AdRewardType rewardType)
    {
        m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("ad_watched", new HoopslyEventParameter[] 
        {
            new HoopslyEventParameter("ad_network", adInfo.NetworkName),
            new HoopslyEventParameter("reward_type", rewardType.ToString())
        }));
        DispatchEventsInQuque();
        //FirebaseAnalytics.LogEvent("ad_watched", new Parameter("ad_network", adInfo.NetworkName), new Parameter("reward_type", rewardType.ToString()));
    }

    public void RaiseLevelStartEvent(string level_id, bool measureFPS = true, string skin = "", string gameType = "")
    {
        List<HoopslyEventParameter> hoopslyEventParameters = new List<HoopslyEventParameter>();
        hoopslyEventParameters.Add(new HoopslyEventParameter("level_id", level_id));

        if (skin != "")
            hoopslyEventParameters.Add(new HoopslyEventParameter("skin", skin));
        if (gameType != "")
            hoopslyEventParameters.Add(new HoopslyEventParameter("type", gameType));

        m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("level_start", hoopslyEventParameters.ToArray(), new HoopslyEvent_String("level_id", OnUpdateLevelIdUserProperty)));
        DispatchEventsInQuque();
        if (measureTool.MeasurmentInProcess)
        {
            measureTool.StopMeasurement();
            Debug.LogWarning("===[FPS measurment was not over properly! Force stop previos measurment.]===");
        }
        if (measureFPS == true)
        {
            measureTool.StartMeasurement();
        }
    }

    public void RaiseAdOfferEvent(AdRewardType rewardType)
    {
        m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("ad_offer", new HoopslyEventParameter("reward_type", rewardType.ToString())));
        DispatchEventsInQuque();
    }

    public void RaiseConsumableEvent(string consumableId)
    {
        m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("consumable", new HoopslyEventParameter("consumable_id", consumableId)));
        DispatchEventsInQuque();
    }

    public void RaiseUpgradeEvent(string content_id, int level, ChangeCondition upgradeCondition)
    {
        m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("upgrade", new HoopslyEventParameter[] 
        {
            new HoopslyEventParameter("content_id", content_id),
            new HoopslyEventParameter("level", level),
            new HoopslyEventParameter("condition", upgradeCondition.ToString())
        }));
        DispatchEventsInQuque();
    }

    public void RaiseUnlockEvent(string content_id, ChangeCondition unlockCondition)
    {
        m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("unlock", new HoopslyEventParameter[]
        {
            new HoopslyEventParameter("content_id", content_id),
            new HoopslyEventParameter("condition", unlockCondition.ToString())
        }));
        DispatchEventsInQuque();
    }

    public void RaiseLevelFinishedEvent(string level_id, LevelFinishedResult result, int playTime, string reason = "", string enemy = "", string gameType = "")
    {
        List<HoopslyEventParameter> eventParams = new List<HoopslyEventParameter>()
        {
            new HoopslyEventParameter("level_id", level_id.ToString()),
            new HoopslyEventParameter("result", result.ToString()),
            new HoopslyEventParameter("time", playTime)
        };

        if (reason != "")
            eventParams.Add(new HoopslyEventParameter("reason", reason));

        if (enemy != "")
            eventParams.Add(new HoopslyEventParameter("enemy", enemy));

        if (gameType != "")
            eventParams.Add(new HoopslyEventParameter("type", gameType));

        if (measureTool.MeasurmentInProcess)
        {
            int[] measureResult = measureTool.StopMeasurement();
            eventParams.Add(new HoopslyEventParameter("fps_avg", measureResult[0]));
            eventParams.Add(new HoopslyEventParameter("fps_perc_1", measureResult[1]));
            eventParams.Add(new HoopslyEventParameter("fps_perc_5", measureResult[2]));
        }

        m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("level_end", eventParams.ToArray()));

        DispatchEventsInQuque();

        LevelCountEvent();

    }

    public void RaiseRateUsEvent(int rate)
    {
        m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("rate_us",  new HoopslyEventParameter("stars", rate)));
        DispatchEventsInQuque();
    }

    private void InterWatchCountEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        int currentIntCount = PlayerPrefs.GetInt("interCount", 0);
        currentIntCount++;

        if(currentIntCount==10)
        {
            m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("opt_int_10"));
            DispatchEventsInQuque();
        }
        else if(currentIntCount==20)
        {
            m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("opt_int_20"));
            DispatchEventsInQuque();
        }
        else if(currentIntCount==40)
        {
            m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("opt_int_40"));
            DispatchEventsInQuque();
        }
        PlayerPrefs.SetInt("interCount", currentIntCount);
    }

    private void RewardWatchCountEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        int currentRewardCount = PlayerPrefs.GetInt("rewardCount", 0);
        currentRewardCount++;
        if(currentRewardCount == 2)
        {
            m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("opt_rew_2"));
            DispatchEventsInQuque();
        }
        else if(currentRewardCount == 5)
        {
            m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("opt_rew_5"));
            DispatchEventsInQuque();
        }
        else if(currentRewardCount == 10)
        {
            m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("opt_rew_10"));
            DispatchEventsInQuque();
        }
        PlayerPrefs.SetInt("rewardCount", currentRewardCount);
    }

    private void LevelCountEvent()
    {
        int currentLevelCount = PlayerPrefs.GetInt("levelCount", 0);
        currentLevelCount++;
        if (currentLevelCount == 5)
        {
            m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("opt_lvl_5"));
            DispatchEventsInQuque();
        }
        else if(currentLevelCount == 10)
        {
            m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("opt_lvl_10"));
            DispatchEventsInQuque();
        }
        else if(currentLevelCount == 20)
        {
            m_firebaseEventQueue.Enqueue(new HoopslyAnalicsEvent("opt_lvl_20"));
            DispatchEventsInQuque();
        }
        PlayerPrefs.SetInt("levelCount", currentLevelCount);
    }

    #endregion

    #region AppsFlyer Initailization
    private void InitAppsFlyer(string uuid)
    {
        Debug.Log("==========[APPSFLYER_INIT]==========");
        if (m_appsFlyerIntegrationManager != null)
        {
            m_appsFlyerIntegrationManager.InitAppsflyerSDK(uuid, HoopslySettings.Instance.AppsFlyerSdkKey, HoopslySettings.Instance.AppsflyerIsDebug, HoopslySettings.Instance.AppsFlyerAppID);
        }
        else
        {
            m_appsFlyerIntegrationManager = GetComponentInChildren<AppsFlyerObjectScript>();
            if (m_appsFlyerIntegrationManager != null)
                m_appsFlyerIntegrationManager.InitAppsflyerSDK(uuid, HoopslySettings.Instance.AppsFlyerSdkKey, HoopslySettings.Instance.AppsflyerIsDebug, HoopslySettings.Instance.AppsFlyerAppID);
            else
                Debug.LogError("Unable to find AppsFlyer to init!");
        }
    }
    #endregion

    #region Applovin
    private void InitApplovin(string uuid)
    {
        Debug.Log("==========[APPLOVIN_INIT]==========");
        if (HoopslySettings.Instance.MaxSdkKey == "")
        {
            Debug.LogWarning("Applovin MAX sdk key was not set! Initialization skipped!");
            return;
        }

        MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
        {
            if(Application.platform == RuntimePlatform.IPhonePlayer)
            {
                
                InitFirebase(m_uuid);

                if(HoopslySettings.Instance.UseAppsflyer)
                    InitAppsFlyer(m_uuid);

                if(HoopslySettings.Instance.UseFacebook)
                    InitFacebookSDK();
            }

            if (HoopslySettings.Instance.UseInterstitialAd)
                InitializeInterstitialAds();
            if (HoopslySettings.Instance.UseRewardedAd)
                InitializeRewardedAds();
            if (HoopslySettings.Instance.UseBannerAd)
                InitializeBannerAds();
            if (HoopslySettings.Instance.UseMRECAd)
                InitializeMrecAds();
            if (HoopslySettings.Instance.ShowMediationDebuggerOnLoad)
                MaxSdk.ShowMediationDebugger();
        };
        MaxSdk.SetSdkKey(HoopslySettings.Instance.MaxSdkKey);
        MaxSdk.SetUserId(uuid);
        MaxSdk.InitializeSdk();
    }
    #region Interstitial Ad Methods
    private void InitializeInterstitialAds()
    {
        MaxSdkCallbacks.Interstitial.OnAdLoadedEvent += OnInterstitialLoadedEvent;
        MaxSdkCallbacks.Interstitial.OnAdLoadFailedEvent += OnInterstitialLoadFailedEvent;
        MaxSdkCallbacks.Interstitial.OnAdDisplayFailedEvent += OnInterstitialAdFailedToDisplayEvent;
        MaxSdkCallbacks.Interstitial.OnAdDisplayedEvent += OnInterstitialDisplayedEvent;
        MaxSdkCallbacks.Interstitial.OnAdClickedEvent += OnInterstitialClickedEvent;
        MaxSdkCallbacks.Interstitial.OnAdHiddenEvent += OnInterstitialDismissedEvent;

        MaxSdkCallbacks.Interstitial.OnAdHiddenEvent += InterWatchCountEvent;

        LoadInterstitial();
    }

    void LoadInterstitial()
    {
        MaxSdk.LoadInterstitial(HoopslySettings.Instance.InterstitialAdUnitID);
    }

    public bool IsInterstitialReady()
    {
        return MaxSdk.IsInterstitialReady(HoopslySettings.Instance.InterstitialAdUnitID);
    }

    public void ShowInterstitial()
    {
        if (MaxSdk.IsInterstitialReady(HoopslySettings.Instance.InterstitialAdUnitID))
        {
            MaxSdk.ShowInterstitial(HoopslySettings.Instance.InterstitialAdUnitID);
        }
        else
        {
            Debug.LogWarning("Ad was not redy!");
        }
    }

    private void OnInterstitialLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("Interstitial loaded");
        m_OnInterstitialLoadedEvent(adUnitId);
        interstitialRetryAttempt = 0;
    }

    private void OnInterstitialLoadFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
    {
        interstitialRetryAttempt++;
        double retryDelay = Math.Pow(2, Math.Min(6, interstitialRetryAttempt));
        Debug.Log("Interstitial failed to load with error code: " + errorInfo);
        m_OnInterstitialFailedEvent(adUnitId, errorInfo);
        Invoke("LoadInterstitial", (float)retryDelay);
    }

    private void OnInterstitialAdFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("Interstitial failed to display with error code: " + errorInfo);
        m_OnInterstitialFailedToDisplayEvent(adUnitId, errorInfo, adInfo);
        LoadInterstitial();
    }
    private void OnInterstitialDisplayedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo) 
    {
        Debug.Log("Interstitial ad was displayed");
        m_OnInterstitialDisplayedEvent(adUnitId, adInfo);
    }

    private void OnInterstitialClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo) 
    {
        Debug.Log("Interstitial ad was clicked");
        m_OnInterstitialClickedEvent(adUnitId, adInfo);
    }

    private void OnInterstitialDismissedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("Interstitial dismissed");
        m_OnInterstitialDismissedEvent(adUnitId);
        LoadInterstitial();
    }
    #endregion

    #region Rewarded Ad Methods
    private void InitializeRewardedAds()
    {
        MaxSdkCallbacks.Rewarded.OnAdLoadedEvent += OnRewardedAdLoadedEvent;
        MaxSdkCallbacks.Rewarded.OnAdLoadFailedEvent += OnRewardedAdLoadFailedEvent;
        MaxSdkCallbacks.Rewarded.OnAdDisplayFailedEvent += OnRewardedAdFailedToDisplayEvent;
        MaxSdkCallbacks.Rewarded.OnAdDisplayedEvent += OnRewardedAdDisplayedEvent;
        MaxSdkCallbacks.Rewarded.OnAdClickedEvent += OnRewardedAdClickedEvent;
        //MaxSdkCallbacks.Rewarded.OnAdRevenuePaidEvent += OnRewardedAdRevenuePaidEvent;
        MaxSdkCallbacks.Rewarded.OnAdHiddenEvent += OnRewardedAdClosedEvent;
        MaxSdkCallbacks.Rewarded.OnAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;

        MaxSdkCallbacks.Rewarded.OnAdHiddenEvent += RewardWatchCountEvent;

        LoadRewardedAd();
    }

    private void LoadRewardedAd()
    {
        MaxSdk.LoadRewardedAd(HoopslySettings.Instance.RewardedAdUnitID);
    }

    public bool IsRewardedReady()
    {
        return MaxSdk.IsRewardedAdReady(HoopslySettings.Instance.RewardedAdUnitID);
    }

    public void ShowRewarded(AdRewardType rewardType)
    {
        if (MaxSdk.IsRewardedAdReady(HoopslySettings.Instance.RewardedAdUnitID))
        {
            MaxSdk.ShowRewardedAd(HoopslySettings.Instance.RewardedAdUnitID, rewardType.ToString());
            m_currentRewardType = rewardType;
            RaiseAdAttemptEvent(rewardType);
        }
        else
        {
            Debug.LogWarning("AD Not ready!");
            LoadRewardedAd();
        }
    }

    private void OnRewardedAdLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("Rewarded ad loaded");
        m_OnRewardedAdLoadedEvent(adUnitId, adInfo);
        rewardedRetryAttempt = 0;
    }

    private void OnRewardedAdLoadFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
    {
        rewardedRetryAttempt++;
        double retryDelay = Math.Pow(2, Math.Min(6, rewardedRetryAttempt));
        Debug.LogWarning("Rewarded ad failed to load with error code: " + errorInfo);
        m_OnRewardedAdLoadFailedEvent(adUnitId, errorInfo);
        Invoke("LoadRewardedAd", (float)retryDelay);
    }

    private void OnRewardedAdFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo, MaxSdkBase.AdInfo adInfo)
    {
        Debug.LogWarning("Rewarded ad failed to display with error code: " + errorInfo);
        m_OnRewardedAdFailedToDisplayEvent(adUnitId, errorInfo, adInfo);
        LoadRewardedAd();
    }

    private void OnRewardedAdDisplayedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("Rewarded ad displayed");
        m_OnRewardedAdDisplayedEvent(adUnitId, adInfo);
    }

    private void OnRewardedAdClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("Rewarded ad clicked");
        m_OnRewardedAdClickedEvent(adUnitId, adInfo);
    }

    private void OnRewardedAdClosedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("Rewarded ad was closed. Load next reward ad");
        m_OnRewardedAdClosedEvent(adUnitId, adInfo);
        LoadRewardedAd();
    }

    private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("Rewarded ad received reward");
        m_OnRewardedAdReceivedRewardEvent(adUnitId, reward, adInfo, m_currentRewardType);
        RaiseAdWatchedEvent(adInfo, m_currentRewardType);
    }

    #endregion

    #region Banner Ad Methods

    private void InitializeBannerAds()
    {
        MaxSdk.CreateBanner(HoopslySettings.Instance.BannerAdUnitID, HoopslySettings.Instance.BannerPosition);
        MaxSdk.SetBannerExtraParameter(HoopslySettings.Instance.BannerAdUnitID, "adaptive_banner", HoopslySettings.Instance.UseAdaptiveBanner ? "true" : "false");
        MaxSdk.SetBannerBackgroundColor(HoopslySettings.Instance.BannerAdUnitID, HoopslySettings.Instance.BannerBackgroundColor);
    }

    public void ShowBanner()
    {
        MaxSdk.ShowBanner(HoopslySettings.Instance.BannerAdUnitID);
    }

    public void HideBanner()
    {
        MaxSdk.HideBanner(HoopslySettings.Instance.BannerAdUnitID);
    }

    private void ToggleBannerVisibility()
    {
        if (!isBannerShowing)
        {
            MaxSdk.ShowBanner(HoopslySettings.Instance.BannerAdUnitID);
        }
        else
        {
            MaxSdk.HideBanner(HoopslySettings.Instance.BannerAdUnitID);
        }
        isBannerShowing = !isBannerShowing;
    }

    #endregion

    #region MREC Ad Methods
    private void InitializeMrecAds()
    {
        MaxSdk.CreateMRec(HoopslySettings.Instance.MRECAdUnitID, HoopslySettings.Instance.MrecPosition);
    }

    public void ShowMREC()
    {
        MaxSdk.ShowMRec(HoopslySettings.Instance.MRECAdUnitID);
    }

    public void HideMREC()
    {
        MaxSdk.HideMRec(HoopslySettings.Instance.MRECAdUnitID);
    }

    public void ToggleMrecVisibility()
    {
        if(isMrecShowing)
        {
            MaxSdk.ShowMRec(HoopslySettings.Instance.MRECAdUnitID);
        }
        else
        {
            MaxSdk.HideMRec(HoopslySettings.Instance.MRECAdUnitID);
        }
        isMrecShowing = !isMrecShowing;
    }
    #endregion

    #endregion

    #region Facebook & Audiebce Initialization
    private void InitFacebookSDK()
    {
        Debug.Log("==========FACEBOOK!==========");
        FB.Init();
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            FB.Init(() =>
            {
                FB.ActivateApp();
            });
        }
    }
    #endregion

}

//class FirebaseEvent
//{
//    public string m_name;
//    public string m_levelId;
//    public Parameter[] m_parameters;
//}

public enum LevelFinishedResult { win, lose, manual_restart };
public enum AdRewardType { revive, multiply, skin, skin_1, skin_2, skin_3, coins_1, coins_2, coins_3, bonus_level, upgrade_attack, upgrade_def, upgrade_speed, upgrade_spawn, upgrade_hp, upgrade_income, other, boost_freeze, boost_speed_up, boost_tower_count };
public enum ChangeCondition { milestone, buy, ad_watch, boost };