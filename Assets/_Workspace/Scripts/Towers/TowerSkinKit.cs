﻿using System;
using UnityEngine;

namespace Towers
{
    [Serializable]
    [CreateAssetMenu(menuName = "Tower Kit", fileName = "Tower Kit")]
    public class TowerSkinKit : ScriptableObject
    {
        [SerializeField] private TowerSkinMesh _base;
        [SerializeField] private TowerSkinMesh[] _skins = new TowerSkinMesh[4];

        public TowerSkinMesh Base => _base;

        public TowerSkinMesh GetSkin(TowerType type)
        {
            return _skins[(int)type];
        }
    }
}