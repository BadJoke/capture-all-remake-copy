﻿using System.Collections.Generic;
using Towers;
using UnityEngine;

namespace UI.Boosts
{
    public class PointsBoost : Boost
    {
        [SerializeField] private int _extraPoints = 10;
        
        public override void Activate()
        {
            IEnumerable<Tower> towers = TowersController.GetTowersByFraction(Fraction.Player);

            foreach (Tower tower in towers)
            {
                tower.AddPoints(_extraPoints);
            }
        }
    }
}