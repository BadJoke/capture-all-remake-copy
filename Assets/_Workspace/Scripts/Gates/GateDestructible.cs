using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gates;
using Units;
using UnityEngine.UI;

public class GateDestructible : Gate
{
    [Header("����� ���������:")]
    [Range(1, 20)]
    [SerializeField] private int _endurance = 3;
    [Header("--------------------")]
    [SerializeField] private Text _textComponent;
    [SerializeField] private GameObject _objectToDeactivate;
    [SerializeField] private GameObject ps;

    private bool _destroyed;

    private void OnValidate()
    {
        _textComponent.text = _endurance.ToString();
    }

    public override void UnitEntered(Unit unit)
    {
        if (_destroyed)
        {
            return;
        }
        
        _endurance--;
        _textComponent.text = _endurance.ToString();
        unit.Die(true);
        
        if (_endurance == 0)
        {
            Destroy();
        }
    }

    private void Destroy()
    {
        ps.transform.SetParent(null);
        ps.SetActive(true);
        _objectToDeactivate.SetActive(false);
        _destroyed = true;
    }

}
