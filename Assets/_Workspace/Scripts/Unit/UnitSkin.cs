﻿using System.Collections;
using UnityEngine;
using General;
using Towers;

namespace Units
{
    public class UnitSkin : MonoBehaviour
    {
        [SerializeField] private Transform _model;
        [SerializeField] private GameObject[] _skins;
        [SerializeField] private AnimationCurve _scalingCurve;
        [SerializeField] private Renderer _colorDependentRenderer;

        private float _relativePositionX;
        private Vector3 _modelNormalScale;
        private Settings _settings;
        private ColorManager _colorManager;

        public GameObject Model => _model.gameObject;

        private void Awake()
        {
            _modelNormalScale = _model.localScale;
            _colorManager = ColorManager.Instance;
            _settings = Settings.Instance;
        }

        public void RefreshSkin(Unit unit)
        {
            foreach (GameObject skin in _skins)
            {
                skin.SetActive(false);
            }

            if (unit as Stickman || unit as StickmanGroup)
                _skins[(int)GetCurrentStickmanSkin(unit)].SetActive(true);
            else if (unit as Tank)
                _skins[(int)GetCurrentTankSkin(unit)].SetActive(true);
        }

        public void ForceSetStickmanSkin(SkinStickman ss)
        {
            foreach (GameObject skin in _skins)
            {
                skin.SetActive(false);
            }

            _skins[(int)ss].SetActive(true);
        }

        private SkinStickman GetCurrentStickmanSkin(Unit unit)
        {
            if (_settings == null)
            {
                _settings = Settings.Instance;
            }

            switch (unit.Fraction)
            {
                case Fraction.Player:
                    return _settings.PlayerStickmanSkin;

                case Fraction.Enemy1:
                    return _settings.Enemy1StickmanSkin;

                case Fraction.Enemy2:
                    return _settings.Enemy2StickmanSkin;

                default:
                    return _settings.PlayerStickmanSkin;
            }
        }

        private SkinTank GetCurrentTankSkin(Unit unit)
        {
            switch (unit.Fraction)
            {
                case Fraction.Player:
                    return _settings.SelectedPlayerTankSkin;

                case Fraction.Enemy1:
                    return _settings.SelectedEnemy1TankSkin;

                case Fraction.Enemy2:
                    return _settings.SelectedEnemy2TankSkin;

                default:
                    return _settings.SelectedPlayerTankSkin;
            }
        }

        public void SetScaleAnimated(float newScale, float period)
        {
            StopCoroutine(nameof(AnimateScaling));
            StartCoroutine(AnimateScaling(newScale, period));
        }

        public void SetScale(float newScale)
        {
            StopCoroutine(nameof(AnimateScaling));
            _model.localScale = Vector3.one * newScale;
        }

        public void ClearScaling()
        {
            StopCoroutine(nameof(AnimateScaling));
            _model.localScale = _modelNormalScale;
        }

        private IEnumerator AnimateScaling(float targetScale, float period)
        {
            Vector3 startScale = _model.localScale;

            for(float t = 0; t <= period; t += period/20)
            {
                _model.localScale = Vector3.LerpUnclamped(startScale, targetScale * Vector3.one, _scalingCurve.Evaluate(t/period));
                yield return new WaitForSeconds(period/20);
            }
            _model.localScale = targetScale*Vector3.one;
        }


        // Moving --------------------------------------------------

        public void ForceClearRelativePosition()
        {
            _relativePositionX = 0;
            _model.localPosition = new Vector3(0, _model.localPosition.y, _model.localPosition.z);
        }

        public void SetRelativePosition(float newPos)
        {
            _relativePositionX = newPos;
            StartCoroutine(AnimateDisplacement());
        }

        private IEnumerator AnimateDisplacement()
        {
            while (_model.localPosition.x != _relativePositionX)
            {
                Vector3 newPosition = _model.localPosition;
                newPosition.x = Mathf.MoveTowards(newPosition.x, _relativePositionX, Time.deltaTime * 2);

                _model.localPosition = newPosition;

                yield return new WaitForEndOfFrame();
            }
        }


        // Color Management -----------------------------------------------

        public void UpdateColor(Fraction fraction)
        {
            _colorDependentRenderer.material = _colorManager.GetUnitMaterial(fraction);
        }
    }
}