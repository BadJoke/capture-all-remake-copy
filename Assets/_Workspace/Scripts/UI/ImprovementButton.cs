using System;
using UnityEngine;
using UnityEngine.UI;

namespace Improvements
{
    public class ImprovementButton : MonoBehaviour
    {
        [SerializeField] private ImprovementType _type;
        [SerializeField] private CustomButton _customButton;
        [SerializeField] private Text _costText;
        [SerializeField] private Text _improvementLevelText;
        [SerializeField] private GameObject _watchAdButton;
        [SerializeField] private GameObject _buyButton;
        [SerializeField] private Advertizer _advertizer; 

        public event Action<ImprovementButton> Clicked; 

        public bool CanBuy { get; private set; }
        public bool IsAdOfferSent { get; private set; }
        public int Price { get; private set; }
        public ImprovementType Type => _type;

        public void Click()
        {
            if (IsAdOfferSent)
            {
                _customButton.Deactivate();
            }

            Clicked?.Invoke(this);
        }

        public void Refresh(int playerMoney, int level)
        {
            _improvementLevelText.text = "LV. " + level.ToString();

            Price = Einstein.CalculateCostOfImprovement(level);
            _costText.text = Price.ToString();

            CanBuy = Price <= playerMoney;

            _watchAdButton.SetActive(!CanBuy);
            _buyButton.SetActive(CanBuy);

            if (CanBuy == false && IsAdOfferSent == false)
            {
                Invoke(nameof(SendAdOffer), 1f);

                IsAdOfferSent = true;
            }
        }

        private void SendAdOffer()
        {
            _advertizer.ImprovementAdOffer(_type);
        }
    }
}