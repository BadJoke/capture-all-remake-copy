﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI.Rating
{
    [RequireComponent(typeof(Image))]
    public class Star : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private int _rate;
        [SerializeField] private Sprite _active;
        [SerializeField] private Sprite _deactive;

        private Image _image;

        public event Action<int> Clicked;

        private void Start()
        {
            _image = GetComponent<Image>();
        }

        public void SetActive(bool isActive)
        {
            _image.sprite = isActive ? _active : _deactive;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Clicked?.Invoke(_rate);
        }
    }
}