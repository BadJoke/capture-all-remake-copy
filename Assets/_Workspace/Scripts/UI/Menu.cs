﻿using UnityEngine;

namespace UI
{
    public class Menu : MonoBehaviour
    {
        [SerializeField] private Showman _showman;
        [SerializeField] private Animation _mainMenuAnim;
        
        public void StartLevel()
        {
            _showman.PlayStartLevelAnimation();
            _mainMenuAnim.Play();
        }
    }
}