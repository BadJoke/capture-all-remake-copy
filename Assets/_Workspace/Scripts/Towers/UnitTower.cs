using UnityEngine;
using Units;
using System.Collections.Generic;
using System.Linq;
using Towers.Attacking;
using Towers.Connections;

namespace Towers
{
    [RequireComponent(typeof(UnitAttacker))]
    public class UnitTower : Tower
    {
        [SerializeField] private OutcomingConnectionController _outcoming�onnections;

        private UnitAttacker _attacker;

        public bool IsMaxConnections => _outcoming�onnections.IsMaxConnection;
        public IEnumerable<Unit> Units => _attacker.ActiveUnits;

        protected override void Awake()
        {
            base.Awake();

            _attacker = GetComponent<UnitAttacker>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            
            Frozen += OnFrozen;
            Unfrozen += OnUnfrozen;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            Frozen -= OnFrozen;
            Unfrozen -= OnUnfrozen;
        }

        public override void Begin()
        {
            if (_fraction == Fraction.Empty)
            {
                return;
            }

            Timer.Begin();
        }

        public override void Stop()
        {
            Timer.Stop();
        }

        public void ConnectTo(Tower target)
        {
            _outcoming�onnections.ConnectTo(target);
        }

        public override void SetModifier(float modifier)
        {
            base.SetModifier(modifier);
            
            _attacker.SetUnitsModifier(modifier);
        }

        public override void ResetModifier()
        {
            base.ResetModifier();
            
            _attacker.ResetUnitsModifier();
        }

        protected override void OnFractionChanged(Fraction fraction)
        {
            if (_fraction == Fraction.Empty)
            {
                Timer.Begin();
            }

            IEnumerable<Tower> towers = TowersController.GetTowers(this).Where(tower => tower.CurrentFraction == CurrentFraction);

            if (towers.Count() == 0)
            {
                _attacker.DestroyUnits();
            }

            _outcoming�onnections.RemoveAll();

            base.OnFractionChanged(fraction);
        }

        private void OnFrozen()
        {
            foreach (Unit unit in _attacker.ActiveUnits)
            {
                unit.Freeze();
            }
        }

        private void OnUnfrozen()
        {
            foreach (Unit unit in _attacker.ActiveUnits)
            {
                unit.Unfreeze();
            }
        }
    }
}