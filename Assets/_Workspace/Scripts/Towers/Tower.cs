﻿using System;
using Towers.Connections;
using Units;
using UnityEngine;

namespace Towers
{
    [RequireComponent(typeof(Timer))]
    public abstract class Tower : MonoBehaviour, IDamagable
    {
        [SerializeField] protected TowerAppearance Appearance;
        [SerializeField] protected TowerPower TowerPower;
        [SerializeField] protected Fraction _fraction;
        [SerializeField] private TowerType _type;
        [SerializeField] private Door _door;
        [SerializeField] private GameObject _keyIcon;
        [SerializeField] private IncomingConnectionController _incomingConnections;

        protected Timer Timer;

        public event Action<int> PowerChanged
        {
            add => TowerPower.Changed += value;
            remove => TowerPower.Changed -= value;
        }

        public event Action<Fraction, Tower> FractionChanged;

        protected event Action Frozen;
        protected event Action Unfrozen;

        public TowerType Type => _type;
        public Fraction CurrentFraction => _fraction;
        public int Power => TowerPower.Amount;
        public bool HasDoor => _door;
        public float Radius => 1.5f;
        public Transform Transform => transform;

        protected virtual void Awake()
        {
            Timer = GetComponent<Timer>();

            if (_door)
            {
                _keyIcon.SetActive(true);
                Appearance.SetKey();
                _fraction = Fraction.Empty;
            }
        }

        protected virtual void OnEnable()
        {
            TowerPower.FractionChanged += OnFractionChanged;
            TowerPower.MaxValueReached += OnMaxValueReached;
        }

        protected virtual void OnDisable()
        {
            TowerPower.FractionChanged -= OnFractionChanged;
            TowerPower.MaxValueReached -= OnMaxValueReached;
        }

        private void Start()
        {
            Appearance.SetAppearance(_fraction, _type);
        }

        public void UpdateAppearance()
        {
            Appearance.SetAppearance(_fraction, _type);
        }

        public abstract void Begin();

        public abstract void Stop();

        public void TakeConnection(Connection connection)
        {
            _incomingConnections.TakeConnection(connection);
        }

        public void TakeDamage(IDamager damager)
        {
            TowerPower.ApplyUnit(damager, CurrentFraction);
        }

        public void AddPoints(int amount)
        {
            TowerPower.AddPower(amount);
            Appearance.ExtraPointsEffectPlay();
        }
        
        public void Freeze()
        {
            Timer.Freeze();
            Frozen?.Invoke();
        }
        
        public void Unfreeze()
        {
            Timer.Unfreeze();
            Unfrozen?.Invoke();
        }

        public virtual void SetModifier(float modifier)
        {
            Timer.SetModifier(1f / modifier);
            Appearance.ActiveRage();
        }

        public virtual void ResetModifier()
        {
            Timer.ResetModifier();
            Appearance.DeactiveRage();
        }

        private void CheckDoor()
        {
            if (_door)
            {
                _keyIcon.SetActive(false);
                _door.Open();
                _door = null;

                Appearance.UnshiftIcons();
            }
        }

        protected virtual void OnFractionChanged(Fraction fraction)
        {
            CheckDoor();

            _fraction = fraction;
            Appearance.SetAppearance(fraction, _type);
            _incomingConnections.CheckConnections();
            FractionChanged?.Invoke(fraction, this);
        }

        private void OnMaxValueReached()
        {
            Appearance.MaxValueEffectPlay();
        }
    }
}