using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphUI : MonoBehaviour
{
    [Header("Parameters:")]
    [Tooltip("You can't change this in PlayMode!")]
    [Min(2)]
    [SerializeField] private int _maxSamples;

    [Header("��������� ��� � ��� �������:")]
    [SerializeField] bool _customBordersEnabled = false;

    [Header("x = min, y = max")]
    [SerializeField] private Vector2 _customBorders = new Vector2(0, 80);


    [Header("Graph links")]
    [SerializeField] private GameObject _pointPattern;

    [SerializeField] private GameObject _linePattern;

    [SerializeField] private RectTransform _graphicsContainer;

    [SerializeField] private Text _bottomText;

    [SerializeField] private Text _peacText;

    


    private RectTransform[] _points;
    private RectTransform[] _lines;
    private float[] Values;




    public int MaxSamples => _maxSamples;





    public void SetValues(float[] value)
    {
        // Set with clamping in _maxSamples;
        if(value.Length > _maxSamples)
        {
            Values = new float[_maxSamples];
            for (int i = 0; i < _maxSamples; i++)
            {
                Values[i] = value[i];
            }
        }
        else
        {
            Values = value;
        }

        // Find peac and bottom of graph
        float peac = 0;
        float bottom = 0;
        if (_customBordersEnabled)
        {
            peac = _customBorders.y;
            bottom = _customBorders.x;
        }
        else
        {
            bool peacFound = false;
            bool bottomFound = false;

            for (int i = 0; i < Values.Length; i++)
            {
                if (!peacFound || Values[i] > peac)
                {
                    peacFound = true;
                    peac = Values[i];
                }
                else if (!bottomFound || Values[i] < bottom)
                {
                    bottomFound = true;
                    bottom = Values[i];
                }
            }
        }
        
        

        //Draw a graph!
        SetPoints(bottom, peac);
    }

    private void Start()
    {
        _points = new RectTransform[_maxSamples];
        _lines = new RectTransform[_maxSamples - 1];
        for (int i = 0; i < _maxSamples; i++)
        {
            _points[i] = Instantiate(_pointPattern, _graphicsContainer).GetComponent<RectTransform>();
            _points[i].gameObject.SetActive(false);
            // We want to have count of lines one less than of points
            if (i < _maxSamples - 1)
            {
                _lines[i] = Instantiate(_linePattern, _graphicsContainer).GetComponent<RectTransform>();
                _lines[i].gameObject.SetActive(false);
            }
        }
        _pointPattern.SetActive(false);
        _linePattern.SetActive(false);
    }

    void SetPoints(float bottom, float peac)
    {
        _bottomText.text = bottom.ToString();
        _peacText.text = peac.ToString();

        for (int i = 0; i < _maxSamples; i++)
        {
            if(i < Values.Length)
            {
                float val = Values[i];
                float xPos = _graphicsContainer.sizeDelta.x * (((_maxSamples-1-i)*1f) / (_maxSamples-1));
                float yPos = _graphicsContainer.sizeDelta.y * Mathf.InverseLerp(bottom, peac, val);

                _points[i].gameObject.SetActive(true);
                _points[i].anchoredPosition = Vector3.right * xPos + Vector3.up * yPos;

                if(i > 0 && i < _maxSamples)
                {
                    _lines[i - 1].gameObject.SetActive(true);
                    _lines[i - 1].position = _points[i].position;
                    _lines[i - 1].sizeDelta = new Vector2(Vector3.Distance(_points[i].anchoredPosition, _points[i - 1].anchoredPosition), 3);
                    _lines[i - 1].localEulerAngles = Vector3.forward *
                        -(90-Vector3.Angle(_points[i].anchoredPosition - _points[i - 1].anchoredPosition, Vector3.up));
                }
            }
            else
            {
                _points[i].gameObject.SetActive(false);
                if (i > 0 && i < _maxSamples)
                {
                    _lines[i - 1].gameObject.SetActive(false);
                }
            }
        }
    }

}
