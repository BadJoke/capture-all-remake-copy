﻿namespace Towers
{
    public enum TowerType
    {
        Default,
        Shooting,
        Attacking,
        Defending
    }
}