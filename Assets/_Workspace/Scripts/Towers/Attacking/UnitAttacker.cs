﻿using FUGames.Pooling;
using General;
using System.Collections.Generic;
using Towers.Connections;
using Units;
using UnityEngine;

namespace Towers.Attacking
{
    public class UnitAttacker : MonoBehaviour
    {
        [SerializeField] private Timer _timer; 
        [SerializeField] private OutcomingConnectionController _connectionController;

        private int _connectionIndex;
        private float _unitSpeedModifier = 1f;
        private List<Unit> _activeUnits;
        private PoolManager _poolManager;
        private Settings _settings;

        public IEnumerable<Unit> ActiveUnits => new List<Unit>(_activeUnits);

        private void Awake()
        {
            _timer.Ticking += OnTicking;
            _timer.Stopped += OnStopped;
        }

        private void Start()
        {
            _connectionIndex = 0;
            _activeUnits = new List<Unit>();
            _poolManager = PoolManager.Instance;
            _settings = Settings.Instance;
        }

        public void DestroyUnits()
        {
            IEnumerable<Unit> units = new List<Unit>(_activeUnits);

            foreach (Unit unit in units)
            {
                unit.Die(true);
            }
        }

        public void SetUnitsModifier(float modifier)
        {
            _unitSpeedModifier = modifier;

            foreach (Unit unit in _activeUnits)
            {
                unit.SetModifier(modifier);
            }
        }

        public void ResetUnitsModifier()
        {
            _unitSpeedModifier = 1f;

            foreach (Unit unit in _activeUnits)
            {
                unit.ResetModifier();
            }
        }

        private void OnTicking()
        {
            if (_connectionController.Connections.Count == 0)
            {
                return;
            }

            if (_connectionIndex >= _connectionController.Connections.Count)
            {
                _connectionIndex = 0;
            }

            Connection connection = _connectionController.Connections[_connectionIndex];
            Unit unit;

            if (_settings.IsDoubleStickman && connection.Owner.CurrentFraction == Fraction.Player)
            {
                unit = _poolManager.Take<StickmanGroup>();
            }
            else
            {
                unit = _poolManager.Take<Stickman>();
            }

            unit.Initialize(connection.Owner, connection.Target, connection.Gates);
            unit.SetModifier(_unitSpeedModifier);
            unit.Died += OnUnitDied;
            unit.Cloned += OnUnitCloned;

            _activeUnits.Add(unit);
            _connectionIndex++;
        }

        private void OnStopped()
        {
            _timer.Ticking -= OnTicking;
            _timer.Stopped -= OnStopped;
        }

        private void OnUnitCloned(Unit unit)
        {
            unit.Died += OnUnitDied;
            _activeUnits.Add(unit);
        }

        private void OnUnitDied(Unit unit)
        {
            unit.Died -= OnUnitDied;
            unit.Cloned -= OnUnitCloned;
            _activeUnits.Remove(unit);
        }
    }
}