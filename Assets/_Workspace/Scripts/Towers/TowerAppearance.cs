﻿using DG.Tweening;
using General;
using UnityEngine;

namespace Towers
{
    [RequireComponent(typeof(TowerSkinKitController))]
    public class TowerAppearance : MonoBehaviour
    {
        [SerializeField] private MeshRenderer _base;
        [SerializeField] private MeshFilter _baseFilter;
        [SerializeField] private MeshRenderer[] _levels;
        [SerializeField] private MeshFilter[] _levelsFilters;
        [SerializeField] private Material _textureMaterial;
        [SerializeField] private TowerIcon _sword;
        [SerializeField] private TowerIcon _shield;
        [SerializeField] private ParticleSystem _levelDestroyEffect;
        [SerializeField] private ParticleSystem _fractionWawe;
        [SerializeField] private ParticleSystem _maxValueEffect;
        [SerializeField] private Transform _tower;
        [SerializeField] private Vector3 _rageScale;
        [SerializeField] private ParticleSystem _rageEffect;
        [SerializeField] private ParticleSystem _extraPointsEffect;

        private bool _hasKey = false;
        private bool _hasSword;
        private bool _hasShield;
        private Vector3 _startScale;
        private Material _effectsMaterial;
        private ColorManager _colorManager;
        private TowerSkinKitController _skinController;

        private void Start()
        {
            _startScale = transform.localScale;
            _colorManager = ColorManager.Instance;
            _skinController = GetComponent<TowerSkinKitController>();
        }

        public void SetKey()
        {
            _hasKey = true;
        }

        public void SetAppearance(Fraction fraction, TowerType type)
        {
            TowerSkinKit kit = _skinController.GetKit(fraction, type);
            TowerSkinMesh baseMesh = kit.Base; 
            TowerSkinMesh levelMesh = kit.GetSkin(type); 
            Material towerMaterial = _colorManager.GetTowerMaterial(fraction);

            var destroyEffect = _levelDestroyEffect.main;
            destroyEffect.startColor = towerMaterial.color;

            _baseFilter.sharedMesh = baseMesh.Mesh;

            Material[] baseMaterials = new Material[2];
            baseMaterials[baseMesh.TextureIndex] = _textureMaterial;
            baseMaterials[baseMesh.FractionIndex] = towerMaterial;
            _base.materials = baseMaterials;

            Material[] levelMaterials = new Material[2];
            levelMaterials[levelMesh.TextureIndex] = _textureMaterial;
            levelMaterials[levelMesh.FractionIndex] = towerMaterial;

            for (int i = 0; i < _levels.Length; i++)
            {
                _levelsFilters[i].sharedMesh = levelMesh.Mesh;
                _levels[i].materials = levelMaterials;
            }

            if (fraction != Fraction.Empty)
            {
                _effectsMaterial = _colorManager.GetLineMaterial(fraction);
                SetEffects();
            }
        }

        public void ChangeIcons(bool hasSword, bool hasShield)
        {
            if (hasSword != _hasSword)
            {
                if (hasSword)
                {
                    _sword.Show(_hasKey);
                }
                else
                {
                    _sword.Hide();
                }
            }

            if (hasShield != _hasShield)
            {
                if (hasShield)
                {
                    _shield.Show(_hasKey);
                }
                else
                {
                    _shield.Hide();
                }
            }

            if (hasSword && hasShield)
            {
                _sword.Shift();
                _shield.Shift();
            }
            else
            {
                _sword.Unshift();
                _shield.Unshift();
            }

            _hasSword = hasSword;
            _hasShield = hasShield;
        }

        public void UnshiftIcons()
        {
            _hasKey = false;
            _sword.Down();
            _shield.Down();
        }

        public void MaxValueEffectPlay()
        {
            _maxValueEffect.Play();
        }

        public void ActiveRage()
        {
            _tower.DOScale(_rageScale, 0.25f);
            _rageEffect.Play();
        }
        
        public void DeactiveRage()
        {
            _tower.DOScale(_startScale, 0.25f);
            _rageEffect.Stop();
        }
        
        public void ExtraPointsEffectPlay()
        {
            _extraPointsEffect.Play();
        }
        
        private void SetEffects()
        {
            SetEffectColor(_maxValueEffect);
            SetEffectColor(_fractionWawe);

            _fractionWawe.Play();
        }

        private void SetEffectColor(ParticleSystem effect)
        {
            ParticleSystem.MainModule main = effect.main;
            main.startColor = _effectsMaterial.color;
        }
    }
}