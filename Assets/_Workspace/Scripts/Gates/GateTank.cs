using UnityEngine;
using Gates;
using Units;
using FUGames.Pooling;

public class GateTank : Gate
{
    [SerializeField] private bool _morphWithEffects;
    
    public override void UnitEntered(Unit unit)
    {
        Tank newTank = PoolManager.Instance.Take<Tank>();
        newTank.InitializeClone(unit, unit.transform.position, false);

        unit.Die(_morphWithEffects);
    }
}
