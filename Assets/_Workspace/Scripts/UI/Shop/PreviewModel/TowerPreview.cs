﻿using Towers;

namespace UI.Shop
{
    public class TowerPreview : PreviewModel
    {
        private TowerModelBase _currentSkin;
        private TowerModelBase _newSkin;

        public override void Init()
        {
            base.Init();

            _currentSkin = (Buttons[0] as TowerButton)?.Skin;
        }

        public override bool IsCurrent()
        {
            if (CurrentButton == null)
            {
                return false;
            }

            return IsCurrent(CurrentButton);
        }

        public override bool IsOpen()
        {
            if (CurrentButton == null)
            {
                return false;
            }

            TowerButton button = CurrentButton as TowerButton;
            return button.IsOpen();
        }

        public override void SetValueToSettings()
        {
            TowerButton button = CurrentButton as TowerButton;

            switch (button.TowerType)
            {
                case TowerType.Default:
                    Settings.PlayerDefaultTower = button.TowerSkin;
                    break;
                case TowerType.Shooting:
                    Settings.PlayerShootingTower = button.TowerSkin;
                    break;
                case TowerType.Attacking:
                    Settings.PlayerAttackingTower = button.TowerSkin;
                    break;
                case TowerType.Defending:
                    Settings.PlayerDefendingTower = button.TowerSkin;
                    break;
                default:
                    throw new System.ArgumentOutOfRangeException();
            }

            TowersController.UpdatePlayerAppearance(button.TowerType);
        }

        public override void AddToOpened()
        {
            TowerButton button = CurrentButton as TowerButton;

            switch (button.TowerType)
            {
                case TowerType.Default:
                    SaveMaster.SaveFile.OpenedDefaultTowerSkins[(int)button.TowerSkin] = true;
                    break;
                case TowerType.Shooting:
                    SaveMaster.SaveFile.OpenedShootingTowerSkins[(int)button.TowerSkin] = true;
                    break;
                case TowerType.Attacking:
                    SaveMaster.SaveFile.OpenedAttackingTowerSkins[(int)button.TowerSkin] = true;
                    break;
                case TowerType.Defending:
                    SaveMaster.SaveFile.OpenedDefendingTowerSkins[(int)button.TowerSkin] = true;
                    break;
                default:
                    throw new System.ArgumentOutOfRangeException();
            }

            SaveMaster.SaveData();
        }

        protected override bool IsCurrent(PreviewButton button)
        {
            TowerButton towerButton = button as TowerButton;

            return towerButton.TowerType switch
            {
                TowerType.Default => Settings.PlayerDefaultTower == towerButton.TowerSkin,
                TowerType.Shooting => Settings.PlayerShootingTower == towerButton.TowerSkin,
                TowerType.Attacking => Settings.PlayerAttackingTower == towerButton.TowerSkin,
                TowerType.Defending => Settings.PlayerDefendingTower == towerButton.TowerSkin,
                _ => throw new System.ArgumentOutOfRangeException()
            };
        }

        protected override void EnableNew()
        {
            _newSkin.Show(((TowerButton) CurrentButton).TowerType);
            _currentSkin = _newSkin;
        }

        protected override void DisableCurrent()
        {
            _currentSkin.Hide();
        }

        protected override void OnButtonClick(PreviewButton button)
        {
            TowerButton towerButton = button as TowerButton;

            CurrentButton?.Deselect();
            towerButton?.Select();
            CurrentButton = towerButton;
            _newSkin = towerButton.Skin;

            base.OnButtonClick(button);
        }
    }
}