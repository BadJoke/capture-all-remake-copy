using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using Firebase;
using Firebase.Analytics;

namespace Hoopsly.Events
{
    public class HoopslyAnalicsEvent
    {
        private string m_eventName;
        public string EventName
        {
            get { return m_eventName; }
        }


        private HoopslyEventParameter[] m_eventParameters;
        public HoopslyEventParameter[] EventParameters
        {
            get { return m_eventParameters; }
        }

        public HoopslyAnalicsEvent(string eventName)
        {
            m_eventName = eventName;
        }

        public HoopslyAnalicsEvent(string eventName, HoopslyEventParameter eventParameter)
        {
            m_eventName = eventName;
            m_eventParameters = new HoopslyEventParameter[] { eventParameter };
        }

        public HoopslyAnalicsEvent(string eventName, HoopslyEventParameter[] eventParameters)
        {
            m_eventName = eventName;
            m_eventParameters = eventParameters;
        }

        public HoopslyAnalicsEvent(string eventName, HoopslyEventParameter[] eventParameters, HoopslyEvent_String newEvent)
        {
            m_eventName = eventName;
            m_eventParameters = eventParameters;

            OnDispatchEventString = newEvent;
        }

        public HoopslyAnalicsEvent(string eventName, HoopslyEventParameter[] eventParameters, HoopslyEvent_Double newEvent)
        {
            m_eventName = eventName;
            m_eventParameters = eventParameters;

            OnDispatchEventDouble = newEvent;
        }

        public HoopslyAnalicsEvent(string eventName, HoopslyEventParameter[] eventParameters, HoopslyEvent_Long newEvent)
        {
            m_eventName = eventName;
            m_eventParameters = eventParameters;

            OnDispatchEventLong = newEvent;
        }

        private HoopslyEvent_String OnDispatchEventString;
        private HoopslyEvent_Double OnDispatchEventDouble;
        private HoopslyEvent_Long OnDispatchEventLong;

        public void Dispatch()
        {
            if(m_eventParameters!=null)
            {
                List<Parameter> eventParameters = new List<Parameter>();
                for (int i = 0; i < m_eventParameters.Length; i++)
                {
                    //if (m_eventParameters[i] != null)
                    //{
                    //    Debug.Log($"===[Parameter with id: {i} IS exist. Parameter name is: {m_eventParameters[i].ParamName}]===");
                    //}
                    //else
                    //{
                    //    Debug.Log($"===[Parameter with id: {i} NOT exist]===");
                    //}
                    //if (eventParameters == null)
                    //{
                    //    Debug.Log("===[Parameters is NULL]===");
                    //}
                    Parameter fbParam = m_eventParameters[i].ConvertToFirebaseParameter();
                    if (fbParam != null)
                    {
                        eventParameters.Add(fbParam);
                    }
                }
                //Debug.Log($"===[Firebase: {m_eventName} was logged]===");
                FirebaseAnalytics.LogEvent(m_eventName, eventParameters.ToArray());
            }
            else
            {
                FirebaseAnalytics.LogEvent(m_eventName);
            }


            if(OnDispatchEventString!=null)
            {
                foreach (HoopslyEventParameter eventParam in m_eventParameters)
                {
                    if(eventParam.ParamName == OnDispatchEventString.RelativeParameterName)
                    {
                        OnDispatchEventString.Invoke((string)eventParam.ParamValue);
                        break;
                    }
                }
            }
            //else
            //{
            //    Debug.Log("===[STRING DISPATCH EVENT IS NULL. SKIPPING]===");
            //}


            if (OnDispatchEventLong!=null)
            {
                foreach (HoopslyEventParameter eventParam in m_eventParameters)
                {
                    if(eventParam.ParamName == OnDispatchEventLong.RelativeParameterName)
                    {
                        OnDispatchEventLong.Invoke((long)eventParam.ParamValue);
                        break;
                    }
                }
            }
            //else
            //{
            //    Debug.Log("===[LONG DISPATCH EVENT IS NULL. SKIPPING]===");
            //}


            if (OnDispatchEventDouble!=null)
            {
                foreach (HoopslyEventParameter eventParam in m_eventParameters)
                {
                    if(eventParam.ParamName == OnDispatchEventDouble.RelativeParameterName)
                    {
                        OnDispatchEventDouble.Invoke((double)eventParam.ParamValue);
                        break;
                    }
                }
            }
            //else
            //{
            //    Debug.Log("===[DOUBLE DISPATCH EVENT IS NULL. SKIPPING]===");
            //}

        }
    }

    public class HoopslyEventParameter
    {
        private enum ValueType { str, lng, doubl, none };
        private ValueType valueType = ValueType.none;

        private string m_paramName;
        public string ParamName
        {
            get { return m_paramName; }
        }

        private object m_paramValue;
        public object ParamValue
        {
            get
            {
                return m_paramValue;
            }
        }


        public HoopslyEventParameter(string paramName, string paramValue)
        {
            m_paramName = paramName;
            m_paramValue = paramValue;
            valueType = ValueType.str;
        }

        public HoopslyEventParameter(string paramName, long paramValue)
        {
            m_paramName = paramName;
            m_paramValue = paramValue;
            valueType = ValueType.lng;
        }

        public HoopslyEventParameter(string paramName, double paramValue)
        {
            m_paramName = paramName;
            m_paramValue = paramValue;
            valueType = ValueType.doubl;
        }


        public Parameter ConvertToFirebaseParameter()
        {
            if(valueType==ValueType.str)
            {
                return new Parameter(ParamName, (string)ParamValue);
            }
            else if(valueType==ValueType.doubl)
            {
                return new Parameter(ParamName, (double)ParamValue);
            }    
            else if(valueType==ValueType.lng)
            {
                return new Parameter(ParamName, (long)ParamValue);
            }
            else
            {
                return null;
            }

        }
    }

    [System.Serializable]
    public class HoopslyEvent_Long : UnityEvent<long>
    {
        private string m_relativeParameterName;
        public string RelativeParameterName
        {
            get { return m_relativeParameterName; }
        }
        public HoopslyEvent_Long(string relativeParameterName, UnityAction<long> relativeAction)
        {
            m_relativeParameterName = relativeParameterName;
            this.AddListener(relativeAction);
        }
    }

    [System.Serializable]
    public class HoopslyEvent_String : UnityEvent<string>
    {
        private string m_relativeParameterName;
        public string RelativeParameterName
        {
            get { return m_relativeParameterName; }
        }
        public HoopslyEvent_String(string relativeParameterName, UnityAction<string> relativeAction)
        {
            m_relativeParameterName = relativeParameterName;
            this.AddListener(relativeAction);
        }
    }

    [System.Serializable]
    public class HoopslyEvent_Double : UnityEvent<double>
    {
        private string m_relativeParameterName;
        public string RelativeParameterName
        {
            get { return m_relativeParameterName; }
        }
        public HoopslyEvent_Double(string relativeParameterName, UnityAction<double> relativeAction)
        {
            m_relativeParameterName = relativeParameterName;
            this.AddListener(relativeAction);
        }
    }

}
