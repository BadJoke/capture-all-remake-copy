﻿using System;
using Towers;
using UnityEngine;

namespace General
{
    //Методы слишком сильно дублируют друг друга, подумать чем заменить
    public class ColorManager : MonoBehaviour
    {
        [SerializeField] private ColorKit _emptyKit;
        [SerializeField] private ColorKit _playerKit;
        [SerializeField] private ColorKit _enemy1Kit;
        [SerializeField] private ColorKit _enemy2Kit;

        private ArgumentException _exception;

        public static ColorManager Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
            _exception = new ArgumentException("Manager doesn`t contains this fraction");
        }

        public Material GetTowerMaterial(Fraction fraction)
        {
            switch (fraction)
            {
                case Fraction.Empty:
                    return _emptyKit.Tower;
                case Fraction.Player:
                    return _playerKit.Tower;
                case Fraction.Enemy1:
                    return _enemy1Kit.Tower;
                case Fraction.Enemy2:
                    return _enemy2Kit.Tower;
                default:
                    throw _exception;
            }
        }

        public Material GetUnitMaterial(Fraction fraction)
        {
            switch (fraction)
            {
                case Fraction.Empty:
                    return _emptyKit.Unit;
                case Fraction.Player:
                    return _playerKit.Unit;
                case Fraction.Enemy1:
                    return _enemy1Kit.Unit;
                case Fraction.Enemy2:
                    return _enemy2Kit.Unit;
                default:
                    throw _exception;
            }
        }

        public Material GetLineMaterial(Fraction fraction)
        {
            switch (fraction)
            {
                case Fraction.Empty:
                    return _emptyKit.Line;
                case Fraction.Player:
                    return _playerKit.Line;
                case Fraction.Enemy1:
                    return _enemy1Kit.Line;
                case Fraction.Enemy2:
                    return _enemy2Kit.Line;
                default:
                    throw _exception;
            }
        }

        public ColorKit GetColorKit(Fraction fraction)
        {
            switch (fraction)
            {
                case Fraction.Empty:
                    return _emptyKit;
                case Fraction.Player:
                    return _playerKit;
                case Fraction.Enemy1:
                    return _enemy1Kit;
                case Fraction.Enemy2:
                    return _enemy2Kit;
                default:
                    throw _exception;
            }
        }
    }
}
