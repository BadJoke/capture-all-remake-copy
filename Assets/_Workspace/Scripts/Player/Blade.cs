﻿using UnityEngine;

namespace Player
{
    public class Blade : MonoBehaviour
    {
        [SerializeField] private TrailRenderer _trailPrefab;

        private Camera _camera;
        private TrailRenderer _trail;

        private void Start()
        {
            _camera = Camera.main;
        }

        private void Update()
        {
            transform.position = GetPosition();
        }

        public void Activate()
        {
            _trail = Instantiate(_trailPrefab, transform);
            _trail.Clear();
        }

        public void Deactivate()
        {
            if (_trail)
            {
                _trail.transform.SetParent(transform.parent);
                _trail.autodestruct = true;
            }
        }

        private Vector3 GetPosition()
        {
            Vector3 position = Input.mousePosition;
            position.z = 8f;
            return _camera.ScreenToWorldPoint(position);
        }
    }
}