using UnityEngine;
using Savings;
using UI;

namespace Improvements
{
    public class Improver : MonoBehaviour
    {
        [SerializeField] private Advertizer _advertizer;
        [SerializeField] private Wallet _wallet;
        [SerializeField] private ImprovementButton[] _buttons; 

        private int[] _improvementLevels;
        private SaveMaster _saveMaster;

        public static Improver Instance { get; private set; }

        private void OnEnable()
        {
            foreach (ImprovementButton button in _buttons)
            {
                button.Clicked += OnButtonClicked;
            }

            _wallet.MoneyChanged += OnMoneyChanged;
            _advertizer.AdWatched += OnAdWatched;
        }

        private void OnDisable()
        {
            foreach (ImprovementButton button in _buttons)
            {
                button.Clicked -= OnButtonClicked;
            }

            _wallet.MoneyChanged -= OnMoneyChanged;
            _advertizer.AdWatched -= OnAdWatched;
        }

        private void Awake()
        {
            Instance = this;
            _saveMaster = SaveMaster.Instance;

            LoadImprovements();
        }

        public int GetImprovementLevel(ImprovementType type)
        {
            return _improvementLevels[(int)type];
        }

        private void LoadImprovements()
        {
            Save saveFile = _saveMaster.SaveFile;

            _improvementLevels = saveFile.ImprovedLevels;

            if (_saveMaster.SaveFile.CurrentLevel != 0)
            {
                RefreshButtons(saveFile.Money);
            }
        }

        private void ForceLevelUpImprovement(ImprovementType type)
        {
            _improvementLevels[(int)type]++;

            _saveMaster.SaveFile.ImprovedLevels = _improvementLevels;
            _saveMaster.SaveData();

            RefreshButtons(_saveMaster.SaveFile.Money);
        }

        private void RefreshButtons(int money)
        {
            foreach (ImprovementButton button in _buttons)
            {
                button.Refresh(money, _improvementLevels[(int)button.Type]);
            }
        }

        private void OnMoneyChanged(int money)
        {
            RefreshButtons(money);
        }

        private void OnButtonClicked(ImprovementButton button)
        {
            if (button.CanBuy)
            {
                _wallet.Buy(button.Price, true);

                ForceLevelUpImprovement(button.Type);
            }
            else
            {
                _advertizer.ShowLevelUpImprovement(button.Type);
            }
        }

        private void OnAdWatched(ImprovementType type)
        {
            ForceLevelUpImprovement(type);
        }
    }
}