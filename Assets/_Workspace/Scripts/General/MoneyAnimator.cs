using System.Collections;
using UnityEngine;
using FUGames.Pooling;
using UnityEngine.UI;
using DG.Tweening.Core;
using DG.Tweening;

namespace UI
{
    public class MoneyAnimator : MonoBehaviour
    {
        [SerializeField] private float _changingTime = 0.2f;
        [SerializeField] private Text coinText;
        [SerializeField] private Transform canvas;
        [SerializeField] private Transform coinsField;

        public void SetMoneyWithoutAnim(int newValue)
        {
            coinText.text = newValue.ToString();
        }

        public void AnimateCoinText(int oldAmount, int newAmount)
        {
            DOGetter<int> getter = () => oldAmount;
            DOSetter<int> setter = (value) =>
            {
                coinText.text = value.ToString();
            };

            DOTween.To(getter, setter, newAmount, _changingTime);
        }

        public void AnimateGain()
        {
            StartCoroutine(AnimateCoining(Input.mousePosition, coinsField.position));
        }

        public void AnimatePay()
        {
            StartCoroutine(AnimateCoining(coinsField.position, Input.mousePosition));
        }

        IEnumerator AnimateCoining(Vector3 start, Vector3 finish)
        {
            for (int i = 0; i < 10; i++)
            {
                PoolManager.Instance.Take<FlyingCoin>().Initialize(start, finish, 0.5f, canvas);

                yield return new WaitForSeconds(0.05f);
            }
        }
    }
}
