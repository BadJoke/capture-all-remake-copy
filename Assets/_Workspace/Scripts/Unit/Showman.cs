using UnityEngine;
using Units;
using General;

public class Showman : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [Header("Links and namings")]
    [SerializeField] private string _levelUpAnimatorName;
    [SerializeField] private string _levelStartedAnimatorName;

    public void PlayLevelUpAnimation()
    {
        _animator.SetBool(_levelUpAnimatorName, true);
    }

    public void PlayStartLevelAnimation()
    {
        _animator.SetBool(_levelStartedAnimatorName, true);
    }

    void ResetLevelUpAnimation()
    {
        _animator.SetBool(_levelUpAnimatorName, false);
    }
}
