﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class PrivacyPolicy : MonoBehaviour
    {
        [SerializeField] private GameObject _block;
        [SerializeField] private Toggle _readToggle;
        [SerializeField] private Toggle _agreeToggle;
        [SerializeField] private GameObject _notFound;

        private const string PrivacyURL = "";

        public void OnToggleChanged()
        {
            if (_readToggle.isOn && _agreeToggle.isOn)
            {
                _block.SetActive(false);
            }
            else
            {
                _block.SetActive(true);
            }
        }

        public void OnKnowMoreClick()
        {
            //Application.OpenURL(PrivacyURL);
            _notFound.SetActive(true);
        }
    }
}