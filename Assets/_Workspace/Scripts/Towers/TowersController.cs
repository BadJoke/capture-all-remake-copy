﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Towers
{
    public class TowersController : MonoBehaviour
    {
        private static Tower[] _towers;

        private void Awake()
        {
            _towers = FindObjectsOfType<Tower>();
        }

        public static Tower[] GetTowers(Tower excepted)
        {
            return _towers.Where(tower => tower != excepted).ToArray();
        }
        
        public static IEnumerable<Tower> GetTowersByFraction(Fraction fraction)
        {
            return _towers.Where(tower => tower.CurrentFraction == fraction).ToArray();
        }
        
        public static IEnumerable<Tower> GetTowersExceptFraction(Fraction fraction)
        {
            return _towers.Where(tower => tower.CurrentFraction != fraction).ToArray();
        }
        
        public static IEnumerable<Tower> GetEnemiesTowers()
        {
            return _towers.Where(tower => tower.CurrentFraction != Fraction.Empty && tower.CurrentFraction != Fraction.Player).ToArray();
        }

        public static void UpdatePlayerAppearance(TowerType type)
        {
            IEnumerable<Tower> playerTowers = _towers.Where(tower =>
                tower.CurrentFraction == Fraction.Player
                && tower.Type == type);

            foreach (Tower tower in playerTowers)
            {
                tower.UpdateAppearance();
            }
        }

        public static void StartLevel()
        {
            foreach (Tower tower in _towers)
            {
                tower.Begin();
            }
        }

        public static void StopLevel()
        {
            foreach (Tower tower in _towers)
            {
                tower.Stop();
            }
        }
    }
}
