using FUGames.Pooling;
using Gates;
using Improvements;
using System;
using Towers;
using UnityEngine;

namespace Units
{
    [RequireComponent(typeof(UnitCollision))]
    public abstract class Unit : PoolObject, IDamagable, IDamager
    {
        [SerializeField] protected int Lives = 1;
        [Header("(Damage ��� ������ ���������: ���� ������ ����� = Damage/2)")]
        [SerializeField] private int _damage = 1;
        [SerializeField] private Animator[] _animators;
        [SerializeField] private ParticleSystem[] _rageEffects;

        private float _speed = 2f;
        private int _normalLives;
        private bool _isFrozen = false;
        private UnitCollision _collision;

        public event Action<Unit> Cloned;
        public event Action<Unit> Died;

        public bool IsDied { get; protected set; }
        public int Damage => _damage;
        public bool IsMultiplied { get; private set; }
        public float RemainingDistance { private set; get; }
        public float CoveredDistance { private set; get; }
        public float SpeedModifier { get; private set; } = 1f;
        public TowerType Type => Owner.Type;
        public Fraction Fraction => Owner.CurrentFraction;
        public UnitTower Owner { private set; get; }
        public Tower Target { private set; get; }
        public Gate[] Gates => _collision.Gates;
        public float Radius => _collision.UnitRadius;
        public Transform Transform => transform;

        protected float UnitRadius => _collision.UnitRadius;

        private void Awake()
        {
            _collision = GetComponent<UnitCollision>();
            _normalLives = Lives;
        }

        private void Update()
        {
            if (IsDied || _isFrozen)
                return;
            
            Move();

            _collision.CheckCollision();
        }

        public void Initialize(UnitTower from, Tower to, Gate[] gates)
        {
            InitShared(from, to, gates);

            transform.position = from.transform.position;
            transform.LookAt(to.transform.position);

            RemainingDistance = (to.transform.position - from.transform.position).magnitude;
            CoveredDistance = 0;
            IsMultiplied = false;

            SpecialInitialize();
        }

        public void InitializeClone(Unit origin, Vector3 pos, bool multiplied)
        {
            InitShared(origin.Owner, origin.Target, origin.Gates);

            transform.position = pos;
            transform.LookAt(Target.transform.position);

            SpeedModifier = origin.SpeedModifier;
            RemainingDistance = (Target.transform.position - transform.position).magnitude;
            CoveredDistance = (transform.position - Owner.transform.position).magnitude;

            IsMultiplied = multiplied;

            SpecialInitialize();

            Cloned?.Invoke(this);
        }

        public abstract void TakeDamage(IDamager damager);

        public abstract void Die(bool withEffects);
        
        public abstract void Multiply(int multiplier);

        public void Freeze()
        {
            _isFrozen = true;
            
            foreach (Animator animator in _animators)
            {
                animator.speed = 0f;
            }
        }
        
        public void Unfreeze()
        {
            _isFrozen = false;
            
            foreach (Animator animator in _animators)
            {
                animator.speed = 1f;
            }
        }

        public void SetModifier(float modifier)
        {
            SpeedModifier = modifier;

            if (Mathf.Approximately(modifier, SpeedModifier))
            {
                return;
            }
            
            foreach (ParticleSystem effect in _rageEffects)
            {
                effect.Play();
            }
        }

        public void ResetModifier()
        {
            SpeedModifier = 1f;
            
            foreach (ParticleSystem effect in _rageEffects)
            {
                effect.Stop();
            }
        }
        
        protected abstract void SpecialInitialize();

        protected void CallDiedEvent()
        {
            Died?.Invoke(this);
        }

        private void Move()
        {
            float offset = _speed * SpeedModifier * Time.deltaTime;
            CoveredDistance += offset;
            RemainingDistance -= offset;
            transform.Translate(Vector3.forward * offset);
        }

        private void InitShared(UnitTower from, Tower to, Gate[] gates)
        {
            Owner = from;
            Target = to;

            Lives = _normalLives;
            IsDied = false;
            _collision.SetGates(gates);

            int level = Improver.Instance.GetImprovementLevel(ImprovementType.MoveSpeed);
            _speed = Einstein.CalculateUnitSpeed(level, Fraction);
        }
    }
}
