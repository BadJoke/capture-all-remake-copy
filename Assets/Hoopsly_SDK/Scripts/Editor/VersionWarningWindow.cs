using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class VersionWarningWindow : EditorWindow
{
    private GUIStyle m_versionHeaderStyle;
    private GUIStyle VersionHeaderLableStyle
    {
        get
        {
            if (m_versionHeaderStyle == null)
            {
                m_versionHeaderStyle = new GUIStyle(EditorStyles.label)
                {
                    fontSize = 14,
                    fontStyle = FontStyle.Normal,
                    stretchHeight = true,
                    alignment = TextAnchor.MiddleCenter,
                    richText = true
                };
            }
            return m_versionHeaderStyle;
        }
    }

    private static string m_remoteVersion;
    public static void Init(string latestVersion)
    {
        VersionWarningWindow window = (VersionWarningWindow)VersionWarningWindow.GetWindow(typeof(VersionWarningWindow), true, "SDK VERSION IS OUTDATED");
        window.minSize = new Vector2(500, 200);
        window.maxSize = window.minSize;
        m_remoteVersion = latestVersion;
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.BeginVertical("box", GUILayout.Height(100));
        GUILayout.Label("YOUR SDK VERSION IS OUTDATED!", VersionHeaderLableStyle);
        GUILayout.Label($"Current version is: <b>{HoopslySettings.Instance.SdkVersion}</b>. Latest version is: <b>{m_remoteVersion}</b>", VersionHeaderLableStyle);
        GUILayout.EndVertical();
        GUILayout.BeginVertical();
        GUILayout.Space(20);
        if (GUILayout.Button("OPEN DOCUMENTAION FOR A LATEST VERSION", GUILayout.Height(30)))
        {
            Application.OpenURL("https://docs.google.com/document/d/1OUFI8LWsckBgGsriPKd_OziHdR94IftRTTxVUQqWjoE/");
        }
        GUILayout.Space(20);
        GUILayout.EndVertical();
    }
}
