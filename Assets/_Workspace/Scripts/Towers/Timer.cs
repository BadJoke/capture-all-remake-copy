﻿using UnityEngine;
using System;
using General;

namespace Towers
{
    public class Timer : MonoBehaviour
    {
        [SerializeField] private Tower _owner;
        [SerializeField] private TowerPower _power;
        [SerializeField] private TimerSettings _timerSettings;
        [SerializeField] private float _delay;

        private bool _isActive;
        private float _modifier = 1f;

        public event Action Ticking;
        public event Action Targeting;
        public event Action Stopped;

        private void OnEnable()
        {
            if (_owner.Type != TowerType.Shooting)
            {
                _power.Changed += OnPowerChanged;
            }
        }

        private void Start()
        {
            _timerSettings.LoadPlayerRate();
        }

        private void OnDisable()
        {
            if (_owner.Type != TowerType.Shooting)
            {
                _power.Changed -= OnPowerChanged;
            }
        }

        public void Begin()
        {
            if (_isActive)
            {
                return;
            }
            
            _isActive = true;

            if (_owner.Type == TowerType.Shooting)
            {
                _delay = 1f;
            }
            else
            {
                UpdateDelay();
            }

            Invoke(nameof(Tick), _delay);

            if (Settings.Instance.IsFreePlay == false)
            {
                Invoke(nameof(Target), _timerSettings.EnemyTargetingDelay);
            }
        }

        public void Stop()
        {
            if (_isActive == false)
            {
                return;
            }
            
            _isActive = false;

            Stopped?.Invoke();
        }

        public void Freeze()
        {
            CancelInvoke(nameof(Tick));
        }

        public void Unfreeze()
        {
            Invoke(nameof(Tick), _delay * _modifier);
        }

        public void SetModifier(float value)
        {
            _modifier = value;
        }

        public void ResetModifier()
        {
            _modifier = 1f;
        }

        private void Tick()
        {
            Ticking?.Invoke();
            Invoke(nameof(Tick), _delay * _modifier);
        }

        private void Target()
        {
            Targeting?.Invoke();
            Invoke(nameof(Target), _timerSettings.EnemyTargetingDelay);
        }

        private void UpdateDelay()
        {
            _delay = _timerSettings.GetDelay(_owner.CurrentFraction, _owner.Power);
        }

        private void OnPowerChanged(int amount)
        {
            UpdateDelay();
        }
    }
}