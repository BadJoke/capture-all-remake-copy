namespace UI.Boosts
{
    public enum BoostType
    {
        Freeze,
        SpeedUp,
        TowerPoints
    }
}