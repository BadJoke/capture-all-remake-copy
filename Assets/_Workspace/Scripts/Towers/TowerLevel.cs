﻿using DG.Tweening;
using System;
using UnityEngine;

namespace Towers
{
    public class TowerLevel : MonoBehaviour
    {
        public event Action Hided;

        public void Show(float yPosition, float time)
        {
            transform.DOLocalMoveY(yPosition, time);
        }

        public void Hide(float time)
        {
            Hided?.Invoke();
            transform.DOLocalMoveY(0f, time);
        }
    }
}