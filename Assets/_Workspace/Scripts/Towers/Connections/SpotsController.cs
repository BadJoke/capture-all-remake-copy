﻿using UnityEngine;

namespace Towers.Connections
{
    public class SpotsController : MonoBehaviour
    {
        [SerializeField] private Spot[] _spots;
        [SerializeField] private OutcomingConnectionController _connections;

        private void Awake()
        {
            _connections.CountChanged += OnCountChanged;
        }

        private void OnDestroy()
        {
            _connections.CountChanged -= OnCountChanged;
        }

        private void OnCountChanged(int count, int limit)
        {
            for (int i = 0; i < _spots.Length; i++)
            {
                Spot spot = _spots[i];

                spot.SetUsing(i < count);
                spot.SetActive(i < limit);
            }
        }
    }
}