﻿using DG.Tweening;
using General;
using Savings;
using System;
using UnityEngine;

namespace UI.Shop
{
    public abstract class PreviewModel : MonoBehaviour
    {
        [SerializeField] private float _size = 1f;
        [SerializeField] private PreviewButton[] _buttons;
        [SerializeField] private AdRewardType _adRewardType;
        [SerializeField] private Advertizer _advertizer;

        protected PreviewButton CurrentButton;
        protected Settings Settings;
        protected SaveMaster SaveMaster;

        public event Action<PreviewModel> Changed;

        public int Cost => CurrentButton ? CurrentButton.SkinCost : 0;
        public AdRewardType RewardType => _adRewardType;
        protected PreviewButton[] Buttons => _buttons;

        private void OnEnable()
        {
            foreach (PreviewButton button in _buttons)
            {
                button.Clicked += OnButtonClick;
            }
        }

        private void OnDisable()
        {
            foreach (var button in _buttons)
            {
                button.Clicked -= OnButtonClick;
            }
        }

        public virtual void Init()
        {
            Settings = Settings.Instance;
            SaveMaster = SaveMaster.Instance;
        }

        public void Refresh()
        {
            foreach (var button in _buttons)
            {
                button.Refresh(IsCurrent(button));
            }
        }

        public abstract bool IsCurrent();

        public abstract bool IsOpen();

        public abstract void SetValueToSettings();

        public abstract void AddToOpened();

        public void Show()
        {
            EnableNew();

            if (IsOpen() == false)
            {
                _advertizer.SkinAdOffer(_adRewardType);
            }

            transform.DOScale(_size, 0.1f);
        }

        public void Hide(Action callback)
        {
            transform.DOScale(0f, 0.1f).onComplete += () =>
            {
                DisableCurrent();

                callback?.Invoke();
            };
        }

        protected abstract bool IsCurrent(PreviewButton button);

        protected abstract void EnableNew();

        protected abstract void DisableCurrent();

        protected virtual void OnButtonClick(PreviewButton button)
        {
            Changed?.Invoke(this);
        }
    }
}