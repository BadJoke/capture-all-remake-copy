﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Shop
{
    public abstract class PreviewButton : MonoBehaviour
    {
        [SerializeField] private GameObject _lock;
        [SerializeField] private GameObject _unlock;
        [SerializeField] private GameObject _checkMark;
        [SerializeField] private Sprite _default;
        [SerializeField] private Sprite _selected;
        [SerializeField] private int _skinCost;
        [SerializeField] private Image _image;

        public event Action<PreviewButton> Clicked;

        public int SkinCost => _skinCost;

        public void Refresh(bool isCurrent)
        {
            bool isOpen = IsOpen();

            _lock.SetActive(!isOpen);
            _unlock.SetActive(isOpen);
            _checkMark.SetActive(isCurrent);
        }

        public abstract bool IsOpen();

        public void Select()
        {
            _image.sprite = _selected;
        }

        public void Deselect()
        {
            _image.sprite = _default;
        }
        
        public void OnClick()
        {
            _image.sprite = _selected;

            Clicked?.Invoke(this);
        }
    }
}