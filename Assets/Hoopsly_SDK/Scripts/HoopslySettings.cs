﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Serialization;
using System.IO;

[System.Serializable]
public class HoopslySettings : ScriptableObject
{
    [Tooltip("ENTER_APPSFLYER_SDK_KEY_HERE")]
    [SerializeField] private bool m_useAppsflyer = false;
    [SerializeField] private string m_appsflyerSdkKey = "";
    [SerializeField] private string m_appsflyerAppId = "";
    [SerializeField] private bool m_appsflyerIsDebug = false;

    [Tooltip("ENTER_MAX_SDK_KEY_HERE")]
    [SerializeField] private bool m_useApplovin = false;
    [SerializeField] private string m_maxSdkKey = "";

    [Tooltip("ENABLE_APPLOVIN_VERBOSE_LOGGING")]
    [SerializeField] private bool m_enableVerboseLogging = false;

    [SerializeField] private bool m_showMediationDebuggerOnStart = false;

    [SerializeField] private bool m_useInterstitialAd;
    [Tooltip("ENTER_INTERSTITIAL_AD_UNIT_ID_HERE")]
    private string m_interstitialAdUnitId = "";
    [SerializeField] private string m_interstitialAdUnitId_IOS = "";
    [SerializeField] private string m_interstitialAdUnitId_ANDROID = "";

    [SerializeField] private bool m_useRewardedAd;
    [Tooltip("ENTER_REWARD_AD_UNIT_ID_HERE")]
    private string m_rewardedAdUnitId = "";
    [SerializeField] private string m_rewardedAdUnitId_IOS = "";
    [SerializeField] private string m_rewardedAdUnitId_ANDROID = "";

    [SerializeField] private bool m_useMRECAd;
    [Tooltip("ENTER_MREC_AD_UNIT_ID_HERE")]
    private string m_MRECAdUnitId = "";
    [SerializeField] private string m_MRECAdUnitId_IOS = "";
    [SerializeField] private string m_MRECAdUnitId_ANDROID = "";
    [SerializeField] private MaxSdkBase.AdViewPosition m_mrecPosition = MaxSdkBase.AdViewPosition.Centered;

    [SerializeField] private bool m_useBannerAd;

    [Tooltip("ENTER_BANNER_AD_UNIT_ID_HERE")]
    private string m_bannerAdUnitId = "";
    [SerializeField] private string m_bannerAdUnitId_IOS = "";
    [SerializeField] private string m_bannerAdUnitId_ANDROID = "";
    [SerializeField] private bool m_useAdaptiveBanner = false;
    [SerializeField] private MaxSdkBase.BannerPosition m_bannerPosition = MaxSdkBase.BannerPosition.BottomCenter;
    [SerializeField] private Color m_bannerBackgroundColor = Color.black;

    [SerializeField] private bool m_useFacebook = false;
    [SerializeField] private bool m_useFirebase = false;

    [SerializeField] private string m_sdkVersion = "";
    [SerializeField] private float m_fpsMeasureIntervals = 1;
    [SerializeField] private bool m_debugFpsMeasurment = false;

    private bool initWasCalled = false;

    public float FPSmeasueIntervals
    {
        get { return Instance.m_fpsMeasureIntervals; }
        set { Instance.m_fpsMeasureIntervals = value; }
    }

    public bool UseAppsflyer
    {
        get { return Instance.m_useAppsflyer; }
        set { Instance.m_useAppsflyer = value; }
    }

    public string AppsFlyerSdkKey
    {
        get { return Instance.m_appsflyerSdkKey; }
        set { Instance.m_appsflyerSdkKey = value; }
    }

    public string AppsFlyerAppID
    {
        get { return Instance.m_appsflyerAppId; }
        set { Instance.m_appsflyerAppId = value; }
    }

    public bool AppsflyerIsDebug
    {
        get { return Instance.m_appsflyerIsDebug; }
        set { Instance.m_appsflyerIsDebug = value; }
    }

    public bool UseApplovin
    {
        get { return Instance.m_useApplovin; }
        set { Instance.m_useApplovin = value; }
    }

    public string MaxSdkKey
    {
        get { return Instance.m_maxSdkKey; }
        set { Instance.m_maxSdkKey = value; }
    }

    public bool EnableVerboseLogging
    {
        get { return Instance.m_enableVerboseLogging; }
        set { Instance.m_enableVerboseLogging = value; }
    }

    public bool ShowMediationDebuggerOnLoad
    {
        get { return Instance.m_showMediationDebuggerOnStart; }
        set { Instance.m_showMediationDebuggerOnStart = value; }
    }

    public bool UseInterstitialAd
    {
        get { return Instance.m_useInterstitialAd; }
        set { Instance.m_useInterstitialAd = value; }
    }

    public bool UseRewardedAd
    {
        get { return Instance.m_useRewardedAd; }
        set { Instance.m_useRewardedAd = value; }
    }

    public bool UseMRECAd
    {
        get { return Instance.m_useMRECAd; }
        set { Instance.m_useMRECAd = value; }
    }

    public bool UseBannerAd
    {
        get { return Instance.m_useBannerAd; }
        set { Instance.m_useBannerAd = value; }
    }

    public bool UseAdaptiveBanner
    {
        get { return Instance.m_useAdaptiveBanner; }
        set { Instance.m_useAdaptiveBanner = value; }
    }

    public string InterstitialAdUnitID
    {
        get { return Instance.m_interstitialAdUnitId; }
        set { Instance.m_interstitialAdUnitId = value; }
    }

    public string InterstitialAdUnitID_IOS
    {
        get { return Instance.m_interstitialAdUnitId_IOS; }
        set { Instance.m_interstitialAdUnitId_IOS = value; }
    }

    public string InterstitialAdUnitID_ANDROID
    {
        get { return Instance.m_interstitialAdUnitId_ANDROID; }
        set { Instance.m_interstitialAdUnitId_ANDROID = value; }
    }


    public string RewardedAdUnitID
    {
        get { return Instance.m_rewardedAdUnitId; }
        set { Instance.m_rewardedAdUnitId = value; }
    }

    public string RewardedAdUnitID_IOS
    {
        get { return Instance.m_rewardedAdUnitId_IOS; }
        set { Instance.m_rewardedAdUnitId_IOS = value; }
    }

    public string RewardedAdUnitID_ANDROID
    {
        get { return Instance.m_rewardedAdUnitId_ANDROID; }
        set { Instance.m_rewardedAdUnitId_ANDROID = value; }
    }

    public string MRECAdUnitID
    {
        get { return Instance.m_MRECAdUnitId; }
        set { Instance.m_MRECAdUnitId = value; }
    }

    public string MRECAdUnitID_IOS
    {
        get { return Instance.m_MRECAdUnitId_IOS; }
        set { Instance.m_MRECAdUnitId_IOS = value; }
    }

    public string MRECAdUnitID_ANDROID
    {
        get { return Instance.m_MRECAdUnitId_ANDROID; }
        set { Instance.m_MRECAdUnitId_ANDROID = value; }
    }

    public string BannerAdUnitID
    {
        get { return Instance.m_bannerAdUnitId; }
        set { Instance.m_bannerAdUnitId = value; }
    }

    public string BannerAdUnitID_IOS
    {
        get { return Instance.m_bannerAdUnitId_IOS; }
        set { Instance.m_bannerAdUnitId_IOS = value; }
    }

    public string BannerAdUnitID_ANDROID
    {
        get { return Instance.m_bannerAdUnitId_ANDROID; }
        set { Instance.m_bannerAdUnitId_ANDROID = value; }
    }

    public MaxSdk.BannerPosition BannerPosition
    {
        get { return Instance.m_bannerPosition; }
        set { Instance.m_bannerPosition = value; }
    }

    public MaxSdkBase.AdViewPosition MrecPosition
    {
        get { return m_mrecPosition; }
        set { Instance.m_mrecPosition = value; }
    }

    public Color BannerBackgroundColor
    {
        get { return Instance.m_bannerBackgroundColor; }
        set { Instance.m_bannerBackgroundColor = value; }
    }

    public bool UseFacebook
    {
        get { return Instance.m_useFacebook; }
        set { Instance.m_useFacebook = value; }
    }

    public bool UseFirebase
    {
        get { return Instance.m_useFirebase; }
        set { Instance.m_useFirebase = value; }
    }

    public string SdkVersion
    {
        get { return Instance.m_sdkVersion; }
    }

    public bool DebugFpsMeasurment
    {
        get { return Instance.m_debugFpsMeasurment; }
        set { Instance.m_debugFpsMeasurment = value; }
    }

    private const string settingsAssetPath = "Assets/Hoopsly_SDK/Resources/HoopslySettings.asset";
    private static HoopslySettings instance;
    public static HoopslySettings Instance
    {
        get
        {
            if(instance == null)
            {
                instance = Resources.Load("HoopslySettings") as HoopslySettings;
                if (instance != null)
                {
                    if (!instance.initWasCalled)
                    {
                        //Debug.Log($"==========[ Hoopsly settings loaded for {Application.platform} platform ]==========");
                        instance.SetPlatformDependentSettings(instance);
                        instance.initWasCalled = true;
                    }
                    return instance;
                }
#if UNITY_EDITOR
                else
                {
                    instance = CreateInstance<HoopslySettings>();
                    AssetDatabase.CreateAsset(instance, settingsAssetPath);
                }
#endif
            }
            return instance;
        }
    }

    private void SetPlatformDependentSettings(HoopslySettings settingsInstance)
    {
        #region Interstitial AD unit
        if (settingsInstance.UseInterstitialAd)
        {
            if(Application.platform == RuntimePlatform.Android)
            {
                settingsInstance.InterstitialAdUnitID = settingsInstance.InterstitialAdUnitID_ANDROID;
            }
            else if(Application.platform == RuntimePlatform.IPhonePlayer)
            {
                settingsInstance.InterstitialAdUnitID = settingsInstance.InterstitialAdUnitID_IOS;
            }
            else
            {
                settingsInstance.InterstitialAdUnitID = settingsInstance.InterstitialAdUnitID_ANDROID;
            }
        }
        #endregion

        #region Rewarded AD unit
        if (settingsInstance.UseRewardedAd)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                settingsInstance.RewardedAdUnitID = settingsInstance.RewardedAdUnitID_ANDROID;
            }
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                settingsInstance.RewardedAdUnitID = settingsInstance.RewardedAdUnitID_IOS;
            }
            else
            {
                settingsInstance.RewardedAdUnitID = settingsInstance.RewardedAdUnitID_ANDROID;
            }
        }
        #endregion

        #region MREC AD unit
        if (settingsInstance.UseMRECAd)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                settingsInstance.MRECAdUnitID = settingsInstance.MRECAdUnitID_ANDROID;
            }
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                settingsInstance.MRECAdUnitID = settingsInstance.MRECAdUnitID_IOS;
            }
            else
            {
                settingsInstance.MRECAdUnitID = settingsInstance.MRECAdUnitID_ANDROID;
            }
        }
        #endregion

        #region Banner AD unit
        if (settingsInstance.UseBannerAd)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                settingsInstance.BannerAdUnitID = settingsInstance.BannerAdUnitID_ANDROID;
            }
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                settingsInstance.BannerAdUnitID = settingsInstance.BannerAdUnitID_IOS;
            }
            else
            {
                settingsInstance.BannerAdUnitID = settingsInstance.BannerAdUnitID_ANDROID;
            }
        }
        #endregion
    }

    public void SaveSettingsAsync()
    {
#if UNITY_EDITOR
        //Debug.Log("Set dirty!");
        EditorUtility.SetDirty(instance);
        AssetDatabase.Refresh();
#endif
    }

}
