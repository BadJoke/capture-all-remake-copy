﻿using General;
using Towers;
using UnityEngine;

namespace Player
{
    public class TowerSelector : MonoBehaviour
    {

        [SerializeField] private LayerMask _groundMask;
        [SerializeField] private LayerMask _towerMask;
        [SerializeField] private SelectionLine _line;

        private bool _isNotBlocked;
        private float _raycastDistance = 100f;
        private UnitTower _selectedTower;
        private Tower _targetTower;
        private Camera _camera;
        private Settings _settings;

        private void Awake()
        {
            _camera = Camera.main;
        }

        private void Start()
        {
            _settings = Settings.Instance;
        }

        public bool TryStartSelecting()
        {
            if (TryFindPlayer())
            {
                _line.SetStart(_selectedTower);

                return true;
            }

            return false;
        }

        public void Selecting()
        {
            Vector3 position = TryFindTarget() ? _targetTower.transform.position : GetWorldPosition();

            _isNotBlocked = _line.SetPosition(position, _targetTower ? _targetTower.transform : null);
        }

        public void Stop()
        {
            if (TryFindTarget() && _selectedTower && _isNotBlocked)
            {
                _selectedTower.ConnectTo(_targetTower);
            }

            _line.SetDefault();

            _selectedTower = null;
            _targetTower = null;
        }

        private bool TryFindTarget()
        {
            return TryFindTower(out _targetTower) && _targetTower != _selectedTower;
        }

        private bool TryFindPlayer()
        {
            Tower tower;

            if (TryFindTower(out tower) && tower is UnitTower)
            {
                _selectedTower = tower as UnitTower;

                if(_settings.IsFreePlay)
                {
                    return _selectedTower.CurrentFraction != Fraction.Empty;
                }
                else
                {
                    return _selectedTower.CurrentFraction == Fraction.Player;
                }
            }

            return false;
        }

        private bool TryFindTower(out Tower tower)
        {
            var ray = _camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit, _raycastDistance, _towerMask))
            {
                tower = hit.transform.GetComponent<Tower>();
            }
            else
            {
                tower = null;
            }

            return tower;
        }

        private Vector3 GetWorldPosition()
        {
            var ray = _camera.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out RaycastHit hit, _raycastDistance, _groundMask);
            return hit.point;
        }
    }
}