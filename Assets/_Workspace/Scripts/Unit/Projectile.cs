﻿using FUGames.Pooling;
using Towers;
using UnityEngine;

namespace Units
{
    public class Projectile : PoolObject, IDamager
    {
        [SerializeField] private int _damage = 1;
        [SerializeField] private float _speed = 0.5f;
        [SerializeField] private float _reachOffest = 1.5f;

        private bool _isTargetAlive;
        private Vector3 _targetPosition;
        private Tower _owner;
        private IDamagable _target;

        public TowerType Type => _owner.Type;
        public Fraction Fraction => _owner.CurrentFraction;
        public int Damage => _damage;

        private void Update()
        {
            Move();
        }

        public void Init(Tower owner, IDamagable target, Vector3 startPosition)
        {
            _isTargetAlive = true;
            _owner = owner;
            _target = target;

            transform.position = startPosition;

            if (target is Unit unit)
            {
                unit.Died += OnUnitDied;
            }
        }

        private void Move()
        {
            Vector3 targetPosition = _isTargetAlive ? _target.Transform.position : _targetPosition;

            Vector3 direction = (targetPosition - transform.position).normalized;
            transform.Translate(direction * _speed * Time.deltaTime, Space.World);

            if (Vector3.Distance(transform.position, targetPosition) < _reachOffest)
            {
                if (_isTargetAlive)
                {
                    _target.TakeDamage(this);
                }

                BackToPool();
            }
        }

        private void OnUnitDied(Unit unit)
        {
            unit.Died -= OnUnitDied;

            _isTargetAlive = false;
            _targetPosition = unit.transform.position;
        }
    }
}
