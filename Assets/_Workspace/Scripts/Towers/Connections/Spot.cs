﻿using UnityEngine;

namespace Towers.Connections
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class Spot : MonoBehaviour
    {
        [SerializeField] private Sprite _used;
        [SerializeField] private Sprite _unused;
        
        private SpriteRenderer _sprite;

        private void Awake()
        {
            _sprite = GetComponent<SpriteRenderer>();
        }

        public void SetActive(bool isActive)
        {
            gameObject.SetActive(isActive);
        }

        public void SetUsing(bool isUsed)
        {
            _sprite.sprite = isUsed ? _used : _unused;
        }
    }
}