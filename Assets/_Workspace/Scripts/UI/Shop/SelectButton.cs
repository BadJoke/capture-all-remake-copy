﻿using UnityEngine;
using UnityEngine.UI;

namespace UI.Shop
{
    public class SelectButton : MonoBehaviour
    {
        [SerializeField] private Text _text;
        [SerializeField] private ShopModelController _modelController;
        [SerializeField] private Sprite _selectBackground;
        [SerializeField] private Sprite _selectedBackground;
 
        private bool _isSelected;
        private Image _background;
        private CustomButton _button;

        private const string SelectText = "SELECT";
        private const string SelectedText = "SELECTED";

        private void Awake()
        {
            _background = GetComponent<Image>();
            _button = GetComponent<CustomButton>();
        }

        public void SetActive(bool isActive, bool isSelected)
        {
            gameObject.SetActive(isActive);

            if (isSelected)
            {
                _button.Deactivate();
                _text.text = SelectedText;
                _background.sprite = _selectedBackground;
            }
            else
            {
                _button.Activate();
                _text.text = SelectText;
                _background.sprite = _selectBackground;
            }
            
            _isSelected = isSelected;
        }

        public void OnSelectClick()
        {
            if (_isSelected)
            {
                return;
            }

            SetActive(true, true);

            _modelController.SetSelectedModelValue();
        }
    }
}