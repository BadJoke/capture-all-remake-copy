﻿using UnityEngine;
using FUGames.Pooling;

namespace DeathEffects
{
    public class DeathEffect : PoolObject
    {
        [SerializeField] private ParticleSystem ps;
        public float delay = 1;


        public ParticleSystem PS => ps;


        public void StartCountdown()
        {
            Invoke(nameof(SetOff), delay);
        }

        void SetOff()
        {
            PoolManager.Instance.Put(this);
        }
    }
}