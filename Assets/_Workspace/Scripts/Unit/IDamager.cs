﻿using Towers;

namespace Units
{
    public interface IDamager
    {
        public TowerType Type { get; }
        public Fraction Fraction { get; }
        public int Damage { get; }
    }
}
