﻿using UnityEngine;
using UnityEngine.UI;

namespace UI.Shop
{
    public class BuyButton : MonoBehaviour
    {
        [SerializeField] private Text _cost;
        [SerializeField] private SelectButton _selectButton;
        [SerializeField] private Wallet _wallet;
        [SerializeField] private ShopModelController _modelController;
        [SerializeField] private Advertizer _advertizer;

        private int _costAmount;
        private AdRewardType _currentRewardType;

        public void SetActive(bool isActive, int cost, AdRewardType rewardType)
        {
            gameObject.SetActive(isActive);
            _cost.text = cost.ToString();
            _costAmount = cost;
            _currentRewardType = rewardType;
        }

        private void Buy(int cost)
        {
            gameObject.SetActive(false);

            _modelController.AddToOpenedCurrentModel();

            _wallet.Buy(cost, true);
            _selectButton.SetActive(true, true);
            _selectButton.OnSelectClick();
        }

        public void OnAdClick()
        {
            _advertizer.ShowSkinBuyAd(_currentRewardType);
            _advertizer.SkinAdWatched += OnAdWatched;
        }

        public void OnBuyClick()
        {
            if (_wallet.Money < _costAmount)
            {
                return;
            }

            Buy(_costAmount);
        }

        private void OnAdWatched()
        {
            _advertizer.SkinAdWatched -= OnAdWatched;

            Buy(0);
        }
    }
}