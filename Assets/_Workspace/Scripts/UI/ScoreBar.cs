﻿using DG.Tweening;
using General;
using System.Collections.Generic;
using Towers;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ScoreBar : MonoBehaviour
    {
        [SerializeField] private float _showYPosition = -75;
        [SerializeField] private float _hideYPosition = 350;
        [SerializeField] private float _fillingTime = 0.2f;
        [SerializeField] private float _showingTime = 0.25f;
        [SerializeField] private Image[] _fractions;
        [SerializeField] private ScoreCalculator _scoreCalculator;

        private RectTransform _rect;

        private void OnEnable()
        {
            _scoreCalculator.PowersChanged += OnUnitCountChanged;
        }

        private void Awake()
        {
            _rect = transform as RectTransform;
        }

        private void OnDisable()
        {
            _scoreCalculator.PowersChanged -= OnUnitCountChanged;
        }

        public void Show()
        {
            _rect.DOAnchorPosY(_showYPosition, _showingTime);
        }

        public void Hide()
        {
            _rect.DOAnchorPosY(_hideYPosition, _showingTime);
        }

        private void OnUnitCountChanged(int totalPower, Dictionary<Fraction, int> fractionPowers)
        {
            int lastPower = fractionPowers[Fraction.Player];

            _fractions[0].DOFillAmount((float)lastPower / totalPower, _fillingTime);

            for (int i = 2; i <= _fractions.Length; i++)
            {
                if (fractionPowers.TryGetValue((Fraction)i, out int power))
                {
                    lastPower += power;
                    _fractions[i - 1].DOFillAmount((float)lastPower / totalPower, _fillingTime);
                }
            }
        }
    }
}