using Improvements;
using Savings;
using System;
using UI;
using UnityEngine;
using UnityEngine.UI;

public class PassiveEarningScreen : MonoBehaviour
{
    [SerializeField] private Improver _improver;
    [SerializeField] private Text _coinsText;
    [SerializeField] private Animation _screenAnim;
    [SerializeField] private CustomButton _claimButton;
    [SerializeField] private Wallet _wallet;

    [SerializeField] private string _animOpenName;
    [SerializeField] private string _animCloseName;

    private int _earnedMoney;
    private float _deltaTimeMinutes;
    private SaveMaster _saveMaster;

    private const float MaxAbsenceMinutes = 720f;

    private void Start()
    {
        _saveMaster = SaveMaster.Instance;
        _saveMaster.GameBeginToSave += OnTutorialCompleted;

        CheckPassiveEarning();
    }

    public void Claim()
    {
        _claimButton.Deactivate();
        _saveMaster.SaveFile.LastEnterGameDate = DateTime.Now.ToString();
        _wallet.ChangeCoinsCount(_earnedMoney, true);

        _screenAnim.Play(_animCloseName);
    }

    private void OnTutorialCompleted()
    {
        if(_saveMaster.SaveFile.CurrentLevel != 0)
            _saveMaster.SaveFile.LastEnterGameDate = DateTime.Now.ToString();
    }

    private void CheckPassiveEarning()
    {
        if (_saveMaster.SaveFile.CurrentLevel == 0)
            return;

        DateTime lastEnter = DateTime.Parse(_saveMaster.SaveFile.LastEnterGameDate);
        _deltaTimeMinutes = (float)(DateTime.Now - lastEnter).TotalMinutes;
        
        int level = _improver.GetImprovementLevel(ImprovementType.PassiveEarning);
        _earnedMoney = (int)
            (Einstein.CalculatePassiveEarning(level)
            * Mathf.Clamp01(_deltaTimeMinutes / MaxAbsenceMinutes));

        if (_earnedMoney < 100)
        {
            return;
        }

        _coinsText.text = _earnedMoney.ToString();
        
        _screenAnim.gameObject.SetActive(true);
        _screenAnim.Play(_animOpenName);
    }
}
