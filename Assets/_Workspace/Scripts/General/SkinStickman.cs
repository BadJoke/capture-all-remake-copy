﻿using System;

namespace General
{
    [Serializable]
    public enum SkinStickman
    {
        Default,
        Clown,
        RedSkin,
        Barbarian,
        IndianaJhones,
        Knight,
        SpecialSquad,
        SportsMan,
        Soldier
    }
}

