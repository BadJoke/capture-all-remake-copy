using UnityEngine;
using General;
using Improvements;
using System;

public class Advertizer : MonoBehaviour
{
    private CurrentAd _currentAd;
    private ImprovementType _improvementType;
    private HoopslyIntegration _hoopslyIntegration;

    public event Action<ImprovementType> AdWatched; 
    public event Action SkinAdWatched; 
    public event Action RewardDoubled;
    public event Action<AdRewardType> BoostTaken;

    private void Awake()
    {
        _hoopslyIntegration = HoopslyIntegration.Instance;
    }

    public void AdOfferStickmansX2()
    {
        _hoopslyIntegration.RaiseAdOfferEvent(AdRewardType.multiply);
    }

    public void AdOfferCoinsX2()
    {
        _hoopslyIntegration.RaiseAdOfferEvent(AdRewardType.bonus_level);
    }

    public void SkinAdOffer(AdRewardType type)
    {
        _hoopslyIntegration.RaiseAdOfferEvent(type);
    }

    public void ImprovementAdOffer(ImprovementType type)
    {
        switch (type)
        {
            case ImprovementType.SpawnRate:
                _hoopslyIntegration.RaiseAdOfferEvent(AdRewardType.upgrade_spawn);
                break;
            case ImprovementType.MoveSpeed:
                _hoopslyIntegration.RaiseAdOfferEvent(AdRewardType.upgrade_speed);
                break;
            case ImprovementType.PassiveEarning:
                _hoopslyIntegration.RaiseAdOfferEvent(AdRewardType.upgrade_income);
                break;
            default:
                throw new ArgumentException("Invalid reward type");
        }
    }

    public void BoostAdOffer(AdRewardType boost)
    {
        _hoopslyIntegration.RaiseAdOfferEvent(boost);
    }
    
    public void ShowDoubleStickmansAd()
    {
        _currentAd = CurrentAd.StickmansX2;
        ShowRewarded(AdRewardType.multiply);
    }

    public void ShowDoubleCoinsAd()
    {
        _currentAd = CurrentAd.CoinsX2;
        ShowRewarded(AdRewardType.bonus_level);
    }

    public void ShowLevelUpImprovement(ImprovementType type)
    {
        _currentAd = CurrentAd.Upgrade;
        _improvementType = type;

        AdRewardType rewardType = AdRewardType.upgrade_spawn;
        switch (type)
        {
            case ImprovementType.SpawnRate:
                rewardType = AdRewardType.upgrade_spawn;
                break;
            case ImprovementType.MoveSpeed:
                rewardType = AdRewardType.upgrade_speed;
                break;
            case ImprovementType.PassiveEarning:
                rewardType = AdRewardType.upgrade_income;
                break;
        }

        ShowRewarded(rewardType);
    }

    public void ShowSkinBuyAd(AdRewardType rewardType)
    {
        _currentAd = CurrentAd.Skin;

        ShowRewarded(rewardType);
    }

    public void ShowBoostAd(AdRewardType boost)
    {
        _currentAd = CurrentAd.Boost;
        ShowRewarded(boost);
    }

    public void RaiseConsumableEvent(AdRewardType type)
    {
        _hoopslyIntegration.RaiseConsumableEvent(type.ToString());
    }

    private void ShowRewarded(AdRewardType rewardType)
    {
        _hoopslyIntegration.ShowRewarded(rewardType);
        _hoopslyIntegration.m_OnRewardedAdReceivedRewardEvent += OnRewardedAdShown;
    }

    public void ShowInterstitialAd()
    {
        _hoopslyIntegration.ShowInterstitial();
    }

    private void DoubleStickmansAdShown()
    {
        Settings.Instance.IsDoubleStickman = true;
    }

    private void DoubleCoinsAdShown()
    {
        RewardDoubled?.Invoke();
    }

    private void FreeImprove()
    {
        AdWatched?.Invoke(_improvementType);
    }

    private void OnRewardedAdShown(string s1, MaxSdkBase.Reward r, MaxSdkBase.AdInfo ai, AdRewardType at)
    {
        _hoopslyIntegration.m_OnRewardedAdReceivedRewardEvent -= OnRewardedAdShown;

        switch (_currentAd)
        {
            case CurrentAd.StickmansX2:
                DoubleStickmansAdShown();
                break;
            case CurrentAd.CoinsX2:
                DoubleCoinsAdShown();
                break;
            case CurrentAd.Upgrade:
                FreeImprove();
                break;
            case CurrentAd.Skin:
                SkinAdWatched?.Invoke();
                break;
            case CurrentAd.Boost:
                BoostTaken?.Invoke(at);
                break;
            default:
                throw new ArgumentException("Invalid reward type");
        }
    }
}
