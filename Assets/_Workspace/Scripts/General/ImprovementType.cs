﻿namespace Improvements
{
    public enum ImprovementType
    {
        SpawnRate,
        MoveSpeed,
        PassiveEarning
    }
}