﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Boosts
{
    public class AdditionalPower : MonoBehaviour
    {
        [SerializeField] private float _riseHeight = 350f;
        [SerializeField] private float _riseDuration = 1f;
        [SerializeField] private Vector3 _startPosition;
        [SerializeField] private Text _display;
        [SerializeField] private Outline _outline;

        public void Init(int value)
        {
            ResetValues();
        
            _display.text = "+" + value;
            
            gameObject.SetActive(true);
            
            DOTween.Sequence()
                .Append(transform.DOLocalMoveZ(transform.localPosition.z - _riseHeight, _riseDuration))
                .Join(_display.DOFade(0f, _riseDuration))
                .Join(_outline.DOFade(0f, _riseDuration));
        }

        private void ResetValues()
        {
            Color color = _display.color;
            color.a = 1f;
            _display.color = color;

            color = _outline.effectColor;
            color.a = 1f;
            _outline.effectColor = color;
            
            transform.localPosition = _startPosition;
            gameObject.SetActive(false);
        }
    }
}