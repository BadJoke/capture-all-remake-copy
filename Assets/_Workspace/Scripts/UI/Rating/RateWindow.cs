﻿using Firebase.Analytics;
using Savings;
using UnityEngine;

namespace UI.Rating
{
    [RequireComponent(typeof(Animation))]
    public class RateWindow : MonoBehaviour
    {
        [SerializeField] private int _minRate = 4;
        [SerializeField, Tooltip("In levels")] private int _reloadDelay = 4;
        [SerializeField] private Star[] _stars;
        [SerializeField] private ThanksWindow _thanks;
        [SerializeField] private string _showAnimation;
        [SerializeField] private string _hideAnimation;
        [SerializeField] private Animation _animation;

        private void OnEnable()
        {
            foreach (var star in _stars)
            {
                star.Clicked += OnStarClicked;
            }
        }

        private void OnDisable()
        {
            foreach (var star in _stars)
            {
                star.Clicked -= OnStarClicked;
            }
        }

        public void Show()
        {
            gameObject.SetActive(true);
            _animation.Play(_showAnimation);
        }

        private void Hide()
        {
            _animation.Play(_hideAnimation);
        }

        public void OnRemindClick()
        {
            SaveMaster saveMaster = SaveMaster.Instance;
            saveMaster.SaveFile.RateShowingLevel += _reloadDelay;
            saveMaster.SaveData();
            
            Hide();
        }

        private void SetRated()
        {
            SaveMaster saveMaster = SaveMaster.Instance;
            saveMaster.SaveFile.IsNotRated = false;
            saveMaster.SaveData();
        }

        private void SetDeactive()
        {
            gameObject.SetActive(false);
        }

        private void OnStarClicked(int rate)
        {
            for (int i = 0; i < _stars.Length; i++)
            {
                _stars[i].SetActive(i < rate);
            }
            
            if (rate > _minRate)
            {
                Application.OpenURL("https://play.google.com/store/apps/details?id=com.hoopsly.captureall");
            }
            else
            {
                _thanks.Show();
            }

            SetRated();

            FirebaseAnalytics.LogEvent("rate_us", new Parameter("stars_amount", rate));
            Hide();
        }
    }
}