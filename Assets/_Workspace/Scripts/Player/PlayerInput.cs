﻿using Towers.Connections;
using UI;
using UnityEngine;

namespace Player
{
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] private TowerSelector _selector;
        [SerializeField] private Blade _blade;
        [SerializeField] private LayerMask _connectionMask;
        [SerializeField] private LayerMask _groundMask;
        [SerializeField] private Wallet _wallet;
        [SerializeField] private UIHandler _uiHandler;

        private bool _isPlayerSelected;
        private Vector3 _startPoint;
        private Camera _camera;

        private void Awake()
        {
            _isPlayerSelected = false;
            _camera = Camera.main;

            _uiHandler.GameStarted += OnGameStarted;
            _uiHandler.GameEnded += OnGameEnded;
        }

        private void Update()
        {
            // для тестов!
            if (Input.GetKeyDown(KeyCode.M))
            {
                _wallet.ChangeCoinsCount(100, false);
            }

            if (Input.GetMouseButtonDown(0))
            {
                _isPlayerSelected = _selector.TryStartSelecting();
                _startPoint = GetPoint();

                if (_isPlayerSelected == false)
                {
                    _blade.Activate();
                }
            }

            if (Input.GetMouseButton(0))
            {
                if (_isPlayerSelected)
                {
                    _selector.Selecting();
                }
                else
                {
                    LineDeleting();
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (_isPlayerSelected)
                {
                    _selector.Stop();
                    _isPlayerSelected = false;
                }

                _blade.Deactivate();
            }
        }

        private void LineDeleting()
        {
            Vector3 point = GetPoint();

            if (Physics.Linecast(_startPoint, point, out RaycastHit hit, _connectionMask.value))
            {
                if (hit.transform.TryGetComponent(out Connection connection))
                {
                    connection.Deactivate();
                }
            }

            _startPoint = point;
        }

        private Vector3 GetPoint()
        {
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out RaycastHit hit, 100f, _groundMask.value);
            return hit.point;
        }

        private void OnGameStarted()
        {
            _uiHandler.GameStarted -= OnGameStarted;
            enabled = true;
        }

        private void OnGameEnded()
        {
            _uiHandler.GameEnded -= OnGameEnded;
            enabled = false;
        }
    }
}