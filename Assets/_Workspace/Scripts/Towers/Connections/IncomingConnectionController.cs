﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Towers.Connections
{
    public class IncomingConnectionController : MonoBehaviour
    {
        [SerializeField] private TowerAppearance _appearance;

        private bool _hasAttack;
        private bool _hasDefend;
        private OutcomingConnectionController _outcoming;
        private List<Connection> _connections;

        private void Start()
        {
            _outcoming = GetComponent<OutcomingConnectionController>();
            _connections = new List<Connection>();
        }

        public void TakeConnection(Connection connection)
        {
            connection.Deactivated += OnConnectionDeactivated;

            _connections.Add(connection);
            _outcoming?.ChekConnection(connection);

            CheckConnections();
        }

        public void CheckConnections()
        {
            _hasAttack = false;
            _hasDefend = false;

            List<Connection> connections = new List<Connection>(_connections);

            foreach (var connection in connections)
            {
                Tower tower = connection.Owner;

                if (tower.Type == TowerType.Attacking && connection.IsFriendly == false)
                {
                    _hasAttack = true;
                }

                if (tower.Type == TowerType.Defending && connection.IsFriendly)
                {
                    _hasDefend = true;
                }
            }

            _appearance.ChangeIcons(_hasAttack, _hasDefend);
        }

        private void OnConnectionDeactivated(Connection connection)
        {
            connection.Deactivated -= OnConnectionDeactivated;
            
            _connections.Remove(connection);

            CheckConnections();
        }
    }
}