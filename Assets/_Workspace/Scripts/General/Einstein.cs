using Towers;

public class Einstein
{
    private const float PlayerSpeed = 2.5f;
    private const float EnemySpeed = 3f;
    private const float SpeedGrowingPerLevel = 0.025f;

    public static int CalculateCostOfImprovement(int lvl)
    {
        int startPrice = 50;
        int MoneyGrowingPerLevel = 120;

        return startPrice + (lvl - 1) * MoneyGrowingPerLevel;
    }

    public static float CalculateUnitSpeed(int lvl, Fraction fraction)
    {
        return fraction == Fraction.Player ? PlayerSpeed + lvl * SpeedGrowingPerLevel : EnemySpeed;
    }

    public static float CalculateUnitSpawnPeriod(int lvl, int buildingScore, Fraction fraction)
    {
        float periodIncreasePerLevel = 0.005f;
        float modifier = 1;

        float attackRate = 0;
        if (fraction == Fraction.Player)
            attackRate = (modifier / (1 + lvl * periodIncreasePerLevel));
        else
            attackRate = modifier;
        return attackRate * (1 - buildingScore / 120f);
    }

    public static float CalculatePassiveEarning(int lvl)
    {
        int startValue = 100;
        int valueGrowingPerLevel = 500;

        return startValue + lvl * valueGrowingPerLevel;
    }

    public static int CalculateLevelCoinsRewardWhenWin(int lvl)
    {
        int startLevel = 200;
        int coinsGrowingPerLevel = 75;

        return startLevel + coinsGrowingPerLevel * lvl;
    }

    public static int CalculateLevelCoinsRewardWhenLose(int lvl)
    {
        int def = CalculateLevelCoinsRewardWhenWin(lvl);
        return def / 5;
    }
}
