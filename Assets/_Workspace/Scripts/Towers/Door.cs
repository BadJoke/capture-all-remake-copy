﻿using UnityEngine;

namespace Towers
{
    [RequireComponent(typeof(BoxCollider))]
    public class Door : MonoBehaviour
    {
        [SerializeField] private GameObject _lockImage;
        [SerializeField] private Animation _animation;
        [SerializeField] private Collider _collider;

        public void Open()
        {
            _lockImage.SetActive(false);
            _animation.Play();
            _collider.enabled = false;
        }
    }
}