﻿using General;
using Savings;
using System;
using Towers;
using UI.Rating;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIHandler : MonoBehaviour
    {
        [SerializeField] private Animation _blackoutAnim;
        [SerializeField] private Advertizer _advertizer;
        [SerializeField] private CustomButton _doubleStickmansAdButton;
        [SerializeField] private Animation _coinsPanelAnim;
        [SerializeField] private string _coinsPanelAnimOpenName;
        [SerializeField] private string _coinsPanelAnimCloseName;
        [SerializeField] private ScoreCalculator _scoreCalculator;
        [SerializeField] private FinishPanel _finishPanel;
        [SerializeField] private GameObject _secondCamera;
        [SerializeField] private GameObject _secondCameraRenderTexture;
        [SerializeField] private Text _levelNumberText;
        [SerializeField] private ScoreBar _scoreBar;
        [SerializeField] private RestartButton _restartButton;
        [SerializeField] private Menu _menu;
        [SerializeField] private GameObject _shopButton;
        [SerializeField] private PrivacyPolicy _privacyPolicy;
        [SerializeField] private RateWindow _rateWindow;

        private SaveMaster _saveMaster;
        private float _timeLevelStarted;

        public event Action GameStarted;
        public event Action GameEnded;

        private void Start()
        {
            GetComponent<Canvas>().worldCamera = Camera.main;

            _saveMaster = SaveMaster.Instance;

            int lvl = _saveMaster.SaveFile.CurrentLevel;

            _coinsPanelAnim.Play(_coinsPanelAnimOpenName);

            if (lvl == 0)
            {
                _levelNumberText.text = "";
                TutorialTurnOff();
                _coinsPanelAnim.Stop();
                StartLevel();
                //_privacyPolicy.gameObject.SetActive(true);
            }
            else
            {
                if (lvl == _saveMaster.SaveFile.RateShowingLevel && _saveMaster.SaveFile.IsNotRated)
                {
                    #if UNITY_ANDROID
                    _rateWindow.Show();
                    #elif UNITY_IOS
                    UnityEngine.iOS.Device.RequestStoreReview();
                    _saveMaster.SaveFile.IsNotRated = false;
                    _saveMaster.SaveData();
                    #endif
                }

                Invoke(nameof(SendDoubleStickmansOffer), 1f);

                _menu.gameObject.SetActive(true);
                _levelNumberText.text = "LEVEL " + lvl.ToString();
            }

            _scoreCalculator.Losed += OnLosed;
            _scoreCalculator.Wone += OnWone;
        }

        public void StartLevel()
        {
            if (_saveMaster.SaveFile.CurrentLevel == 0)
            {
                _privacyPolicy.gameObject.SetActive(false);
            }

            _scoreBar.Show();
            _restartButton.Open();
            _menu.StartLevel();
            _coinsPanelAnim.Play(_coinsPanelAnimCloseName);
            _timeLevelStarted = Time.timeSinceLevelLoad;
            _blackoutAnim.Play();
            _shopButton.SetActive(false);

            TowersController.StartLevel();
            SendLevelStartEvent();
            GameStarted?.Invoke();

            Invoke(nameof(TurnOffSecondCamera), 2);
        }

        public void Restart()
        {
            SendLevelEndEvent(LevelFinishedResult.manual_restart);
            LevelManager.Instance.LoadLevel();
        }

        public void ShowDoubleStickmansAd()
        {
            _doubleStickmansAdButton.Deactivate();
            _advertizer.ShowDoubleStickmansAd();
        }

        private void TurnOffSecondCamera()
        {
            _secondCamera.SetActive(false);
            _secondCameraRenderTexture.SetActive(false);
        }

        private void SendDoubleStickmansOffer()
        {
            _advertizer.AdOfferStickmansX2();
        }

        private void OnWone()
        {
            FinishLevel(true);

            SendLevelEndEvent(LevelFinishedResult.win);
        }

        private void OnLosed()
        {
            FinishLevel(false);

            SendLevelEndEvent(LevelFinishedResult.lose);
        }

        private void TutorialTurnOff()
        {
            _menu.gameObject.SetActive(false);
            _blackoutAnim.Play();
            TurnOffSecondCamera();
        }

        private void FinishLevel(bool win)
        {
            _saveMaster.SaveFile.LastEnterGameDate = DateTime.Now.ToString();
            _coinsPanelAnim.Play(_coinsPanelAnimOpenName);

            _scoreBar.Hide();
            _finishPanel.ShowUIInfo(win);
            _restartButton.Close();

            TowersController.StopLevel();
            GameEnded?.Invoke();
        }

        private void SendLevelStartEvent()
        {
            string level_id = "level_" + _saveMaster.SaveFile.CurrentLevel;

            print("Level started: " + level_id + ". MeasureFPS: true");
            HoopslyIntegration.Instance.RaiseLevelStartEvent(level_id, true);
        }

        private void SendLevelEndEvent(LevelFinishedResult finishedResult)
        {
            string level_id = "level_" + (_saveMaster.SaveFile.CurrentLevel - 1).ToString();
            int playtime = Mathf.RoundToInt(Time.timeSinceLevelLoad - _timeLevelStarted);

            print("Level finished: " + level_id + ". Finish result: " + finishedResult + ". Playtime: " + playtime);
            HoopslyIntegration.Instance.RaiseLevelFinishedEvent(level_id, finishedResult, playtime);
        }
    }
}