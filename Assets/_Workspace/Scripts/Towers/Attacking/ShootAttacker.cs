﻿using DG.Tweening;
using FUGames.Pooling;
using System.Collections.Generic;
using System.Linq;
using Units;
using UnityEngine;

namespace Towers.Attacking
{
    public class ShootAttacker : MonoBehaviour
    {
        [SerializeField] private float _startRadius = 5f;
        [SerializeField] private float _levelRatio = 0.75f;
        [SerializeField] private Transform _shootPoint;
        [SerializeField] private Timer _timer;
        [SerializeField] private TowerLevelManager _levelManager;
        [SerializeField] private float _radius;
        [SerializeField] private float _meshRatio = 1f;
        [SerializeField] private SpriteRenderer _radiusMesh;

        private Tower _owner;
        private Tower[] _towers;
        private IEnumerable<Tower> _targetTowers;
        private PoolManager _poolManager;

        private void Awake()
        {
            _timer.Ticking += OnTicking;
            _timer.Stopped += OnStopped;
        }

        private void OnEnable()
        {
            _levelManager.LevelCountChanged += OnLevelCountChanged;
        }

        private void OnDisable()
        {
            _levelManager.LevelCountChanged -= OnLevelCountChanged;
        }

        private void Start()
        {
            _poolManager = PoolManager.Instance;
        }

        public void Init(Tower owner, Tower[] towers)
        {
            _owner = owner;
            _towers = towers;
        }

        private void OnTicking()
        {
            IEnumerable<Tower> filteredTowers = _targetTowers.Where(tower => CheckTowerShootability(tower));

            List<IDamagable> targets = new List<IDamagable>(filteredTowers);

            foreach (Tower tower in _towers)
            {
                if (tower is UnitTower unitTower)
                {
                    targets.AddRange(unitTower.Units.Where(unit => CheckUnitShootability(unit)));
                }
            }

            if (targets.Count == 0)
            {
                return;
            }

            IDamagable currentTarget = targets[Random.Range(0, targets.Count)];

            Projectile projectile = _poolManager.Take<Projectile>();
            projectile.Init(_owner, currentTarget, _shootPoint.position);

            bool CheckTowerShootability(Tower target)
            {
                bool hasDoor = false;

                if (target is UnitTower tower)
                {
                    hasDoor = tower.HasDoor;
                }

                bool isPossible = target.CurrentFraction != _owner.CurrentFraction;

                switch (_owner.CurrentFraction)
                {
                    case Fraction.Empty:
                        return isPossible && target.Power > 1;
                    case Fraction.Player:
                        return isPossible;
                    default:
                        return isPossible && hasDoor == false;
                }
            }

            bool CheckUnitShootability(Unit unit)
            {
                return Vector3.Distance(unit.transform.position, transform.position) <= _radius + unit.Radius
                    && unit.Fraction != _owner.CurrentFraction;
            }
        }

        private void OnStopped()
        {
            _timer.Ticking -= OnTicking;
            _timer.Stopped -= OnStopped;
        }

        private void OnLevelCountChanged(int count)
        {
            _radius = _startRadius + ((count - 1) * _levelRatio);
            _radiusMesh.transform.DOScale(_radius * _meshRatio, 0.3f);

            _targetTowers = _towers.Where(tower => Vector3.Distance(tower.transform.position, transform.position) <= _radius + tower.Radius);
        }
    }
}