﻿using System;
using UnityEngine;

namespace Towers
{
    public class TowerLevelManager : MonoBehaviour
    {
        [SerializeField] private float _risingTime = 0.33f;
        [SerializeField] private float _height = 1f;
        [SerializeField] private int _levelCost = 10;
        [SerializeField] private TowerPower _power;
        [SerializeField] private TowerLevel[] _levels;
        [SerializeField] private ParticleSystem _destroyEffect;
 
        private int _currentLevel = -1;

        public event Action<int> LevelCountChanged;

        private void Awake()
        {
            foreach (TowerLevel level in _levels)
            {
                level.Hided += OnLevelHided;
            }

            _power.Changed += OnPowerChanged;
        }

        private void Start()
        {
            LevelCountChanged?.Invoke(_currentLevel + 2);
        }

        private void OnDestroy()
        {
            foreach (TowerLevel level in _levels)
            {
                level.Hided -= OnLevelHided;
            }

            _power.Changed -= OnPowerChanged;
        }

        private void OnPowerChanged(int power)
        {
            int newLevel = power / _levelCost - 1;

            if (newLevel == _currentLevel)
            {
                return;
            }

            int changingValue = newLevel > _currentLevel ? 1 : -1;

            while (CanSwitchLevel(newLevel))
            {
                if (changingValue > 0)
                {
                    _currentLevel += changingValue;
                    _levels[_currentLevel].Show(_height, _risingTime);
                }
                else
                {
                    _levels[_currentLevel].Hide(_risingTime);
                    _currentLevel += changingValue;
                }
            }

            LevelCountChanged?.Invoke(_currentLevel + 2);

            bool CanSwitchLevel(int level)
            {
                return _currentLevel != level && level > -2 && level <= _levels.Length;
            }
        }

        private void OnLevelHided()
        {
            _destroyEffect.Play();
        }
    }
}