﻿using FUGames.Pooling;
using General;
using Towers;
using UnityEngine;
using DeathEffects;

namespace Units
{
    public class StickmanGroup : Unit
    {
        [SerializeField] private UnitSkin[] _skins;

        private int[] _livesArray;

        // ---------------------------------- Inherited methods

        protected override void SpecialInitialize()
        {
            // Инициализируем скин, запускаем анимации и ставим цвет в соответсвии с фракцией
            for (int i = 0; i < _skins.Length; i++)
            {
                _skins[i].SetScale(0);
                _skins[i].SetScaleAnimated(1, Random.Range(0.2f, 0.4f));
                _skins[i].UpdateColor(Fraction);
                _skins[i].RefreshSkin(this);
            }

            // Сбрасываем жизни к исходному значению
            _livesArray = new int[_skins.Length];
            for (int i = 0; i < _livesArray.Length; i++)
            {
                _livesArray[i] = Lives;
            }
        }

        public override void Multiply(int multiplier)
        {
            for (int i = 1; i < multiplier; i++)
            {
                StickmanGroup newOne = PoolManager.Instance.Take<StickmanGroup>();
                Vector3 pos = transform.position - transform.forward * i * UnitRadius;
                newOne.InitializeClone(this, pos, true);
            }
        }

        public override void TakeDamage(IDamager damager)
        {
            if (damager.Fraction != Fraction)
            {
                int damage = damager.Damage;

                if (damager is StickmanGroup)
                {
                    damage /= 2;
                }

                for (int i = 0; i < _livesArray.Length; i++)
                {
                    if (_livesArray[i] == -1)
                    {
                        continue;
                    }
                    
                    if (_livesArray[i] <= damage)
                    {
                        _livesArray[i] = 0;
                        KillOne(i);
                        break;
                    }
                    else
                    {
                        _livesArray[i] -= damage;
                        break;
                    }
                }
            }
        }

        private void KillOne(int index)
        {
            // Смерть одного стикмана
            {
                _skins[index].Model.SetActive(false);
                PlayDeathEffect(index);
            }

            _livesArray[index] = -1;

            // Полная смерть?
            if (index == _livesArray.Length - 1)
            {
                Die(true);
            }
        }

        public override void Die(bool withEffects)
        {
            if (IsDied)
                return;

            IsDied = true;

            //Возвращаем видимость моделей всех стикманов группы
            foreach (UnitSkin sk in _skins)
            {
                sk.Model.SetActive(true);
            }

            BackToPool();
            CallDiedEvent();
        }

        private void PlayDeathEffect(int index)
        {
            DeathEffect deathEffect = PoolManager.Instance.Take<StickmanDE>();
            deathEffect.StartCountdown();
            deathEffect.transform.position = _skins[index].transform.position + Vector3.up;

            var main = deathEffect.PS.main;
            main.startColor = ColorManager.Instance.GetColorKit(Fraction).Unit.color;
            return;
        }

    }
}