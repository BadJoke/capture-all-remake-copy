﻿using General;
using Savings;
using Towers;
using UnityEngine;

namespace UI.Shop
{
    public class TowerButton : PreviewButton
    {
        [SerializeField] private TowerType _towerType;
        [SerializeField] private TowerSkin _towerSkin;
        [SerializeField] private TowerModelBase _skin;

        public TowerType TowerType => _towerType;
        public TowerSkin TowerSkin => _towerSkin;
        public TowerModelBase Skin => _skin;

        public override bool IsOpen()
        {
            SaveMaster saveMaster = SaveMaster.Instance;

            switch (_towerType)
            {
                case TowerType.Default:
                    return saveMaster.SaveFile.OpenedDefaultTowerSkins[(int)_towerSkin];
                case TowerType.Shooting:
                    return saveMaster.SaveFile.OpenedShootingTowerSkins[(int)_towerSkin];
                case TowerType.Attacking:
                    return saveMaster.SaveFile.OpenedAttackingTowerSkins[(int)_towerSkin];
                case TowerType.Defending:
                    return saveMaster.SaveFile.OpenedDefendingTowerSkins[(int)_towerSkin];
                default:
                    throw new System.ArgumentOutOfRangeException();
            }
        }
    }
}