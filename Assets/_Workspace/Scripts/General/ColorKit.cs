﻿using UnityEngine;

namespace General
{
    [CreateAssetMenu(fileName = "Kit", menuName = "Color Kit")]
    public class ColorKit : ScriptableObject
    {
        [SerializeField] private Material _tower;
        [SerializeField] private Material _unit;
        [SerializeField] private Material _line;

        public Material Tower => _tower;
        public Material Unit => _unit;
        public Material Line => _line;
    }
}