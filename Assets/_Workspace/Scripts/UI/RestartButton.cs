using General;
using UnityEngine;
using UnityEngine.UI;
using UI;

public class RestartButton : MonoBehaviour
{
    [SerializeField] private UIHandler _uiHandler;
    [SerializeField] private string _enterAnimName = "RestartButton_enter";
    [SerializeField] private string _exitAnimName = "RestartButton_exit";

    private Button _button;
    private Animation _anim;

    private void Awake()
    {
        _button = GetComponent<Button>();
        _anim = GetComponent<Animation>();
    }

    public void Open()
    {
        _button.interactable = true;
        _anim.Play(_enterAnimName);
    }

    public void Close()
    {
        _button.interactable = false;
        _anim.Play(_exitAnimName);
    }
}
