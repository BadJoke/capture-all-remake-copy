﻿using Towers.Attacking;

namespace Towers
{
    public class ShootingTower : Tower
    {
        private ShootAttacker _attacker;

        protected override void Awake()
        {
            base.Awake();

            _attacker = GetComponent<ShootAttacker>();
            _attacker.Init(this, TowersController.GetTowers(this));
        }

        public override void Begin()
        {
            Timer.Begin();
        }

        public override void Stop()
        {
            Timer.Stop();
        }
    }
}