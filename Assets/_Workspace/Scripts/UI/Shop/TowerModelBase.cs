﻿using Towers;
using UnityEngine;

namespace UI.Shop
{
    public class TowerModelBase : MonoBehaviour
    {
        [SerializeField] private GameObject[] _tops;

        private GameObject _current;

        public void Show(TowerType type)
        {
            gameObject.SetActive(true);

            GameObject newTop = _tops[(int)type];
            _current?.SetActive(false);
            newTop.SetActive(true);
            _current = newTop;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}