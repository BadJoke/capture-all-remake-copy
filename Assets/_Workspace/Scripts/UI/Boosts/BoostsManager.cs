﻿using System;
using System.Collections.Generic;
using General;
using Savings;
using UnityEngine;

namespace UI.Boosts
{
    public class BoostsManager : MonoBehaviour
    {
        [SerializeField] private UIHandler _uiHandler;
        [SerializeField] private BoostUiBuyer[] _buyers;
        [SerializeField] private BoostUiActivator[] _users;
        [SerializeField] private BoostBuyersPanel _buyersPanel;
        [SerializeField] private BoostUsersPanel _usersPanel;
        [SerializeField] private Shop.Shop _shop;

        private Save _save;
        private SaveMaster _saveMaster;
        private readonly Dictionary<AdRewardType, int> _boosts = new Dictionary<AdRewardType, int>();

        private void Awake()
        {
            if (LevelManager.Instance.CurrentLevel == 1)
            {
                gameObject.SetActive(false);
                return;
            }
            
            _uiHandler.GameStarted += OnGameStarted;
            
            _saveMaster = SaveMaster.Instance;
            _save = _saveMaster.SaveFile;
            
            _boosts.Add(AdRewardType.boost_freeze, _save.FreezeBoosts);
            _boosts.Add(AdRewardType.boost_speed_up, _save.SpeedUpBoosts);
            _boosts.Add(AdRewardType.boost_tower_count, _save.PointsBoosts);
        }

        private void OnEnable()
        {
            foreach (BoostUiBuyer buyer in _buyers)
            {
                buyer.Added += OnBoostAdded;
            }

            foreach (BoostUiActivator user in _users)
            {
                user.Used += OnBoostUsed;
                user.Added += OnBoostAdded;
            }

            _shop.Showed += _buyersPanel.Hide;
            _shop.Hided += _buyersPanel.Show;
        }

        private void OnDisable()
        {
            foreach (BoostUiBuyer buyer in _buyers)
            {
                buyer.Added -= OnBoostAdded;
            }

            foreach (BoostUiActivator user in _users)
            {
                user.Used -= OnBoostUsed;
                user.Added -= OnBoostAdded;
            }

            _shop.Showed -= _buyersPanel.Hide;
            _shop.Hided -= _buyersPanel.Show;
        }

        private void Start()
        {
            SetCountToBuyers();
            SetCountToUsers();
        }

        private void SetCountToBuyers()
        {
            foreach (BoostUiBuyer buyer in _buyers)
            {
                buyer.SetCount(_boosts[buyer.BoostType]);
            }
        }

        private void SetCountToUsers()
        {
            foreach (BoostUiActivator user in _users)
            {
                user.SetCount(_boosts[user.BoostType]);
            }
        }

        private void OnBoostAdded(AdRewardType boostType)
        {
            if (_boosts.ContainsKey(boostType))
            {
                _boosts[boostType] = ++_boosts[boostType];
            }
            else
            {
                _boosts.Add(boostType, 1);
            }
            
            switch (boostType)
            {
                case AdRewardType.boost_freeze:
                    _save.FreezeBoosts = _boosts[boostType];
                    break;
                case AdRewardType.boost_speed_up:
                    _save.SpeedUpBoosts = _boosts[boostType];
                    break;
                case AdRewardType.boost_tower_count:
                    _save.PointsBoosts = _boosts[boostType];
                    break;
                default:
                    throw new ArgumentException("Invalid boost ad type");
            }

            SetCountToBuyers();
            SetCountToUsers();
            
            _saveMaster.SaveData();
        }

        private void OnBoostUsed(AdRewardType boostType)
        {
            _boosts[boostType] = --_boosts[boostType];

            switch (boostType)
            {
                case AdRewardType.boost_freeze:
                    _save.FreezeBoosts = _boosts[boostType];
                    break;
                case AdRewardType.boost_speed_up:
                    _save.SpeedUpBoosts = _boosts[boostType];
                    break;
                case AdRewardType.boost_tower_count:
                    _save.PointsBoosts = _boosts[boostType];
                    break;
                default:
                    throw new ArgumentException("Invalid boost ad type");
            }
            
            SetCountToUsers();
            _saveMaster.SaveData();
        }

        private void OnGameStarted()
        {
            _uiHandler.GameStarted -= OnGameStarted;

            SetCountToUsers();
            
            _buyersPanel.Hide();
            _usersPanel.Show();
        }
    }
}