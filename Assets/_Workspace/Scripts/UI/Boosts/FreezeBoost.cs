﻿using System.Collections;
using System.Collections.Generic;
using Towers;
using UnityEngine;

namespace UI.Boosts
{
    public class FreezeBoost : Boost
    {
        [SerializeField] private float _duration = 5f;
        [SerializeField] private ParticleSystem _snow;

        private ParticleSystem.MainModule _snowMainModule;
        private WaitForSeconds _delay;
        private IEnumerable<Tower> _enemyTowers;
        
        private void Start()
        {
            _snowMainModule = _snow.main;
            _delay = new WaitForSeconds(_duration);
        }

        public override void Activate()
        {
            StartCoroutine(FreezeRoutine());
        }

        private IEnumerator FreezeRoutine()
        {
            Freeze();

            yield return _delay;
            
            Unfreeze();
        }

        private void Freeze()
        {
            _snowMainModule.loop = true;
            _snow.Play();
            _enemyTowers = TowersController.GetEnemiesTowers();

            foreach (Tower tower in _enemyTowers)
            {
                tower.Freeze();
            }
        }

        private void Unfreeze()
        {
            _snowMainModule.loop = false;
            _enemyTowers = TowersController.GetEnemiesTowers();

            foreach (Tower tower in _enemyTowers)
            {
                tower.Unfreeze();
            }
        }
    }
}