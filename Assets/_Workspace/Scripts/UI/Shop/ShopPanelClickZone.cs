﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Shop
{
    public class ShopPanelClickZone : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private Transform _panel;

        public void OnPointerDown(PointerEventData eventData)
        {
            _panel.SetAsLastSibling();
        }
    }
}