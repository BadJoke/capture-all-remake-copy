﻿using DG.Tweening;
using Savings;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class LocationBar : MonoBehaviour
    {
        [SerializeField] private Sprite[] _locationsIcons;
        [SerializeField] private Image _currentLocationIcon;
        [SerializeField] private Image _nextLocationIcon;
        [SerializeField] private Image _bar;
        [SerializeField] private Sprite[] _locations;
        [SerializeField] private SpriteRenderer _levelLocation;

        private int _currentLocation;
        private int _nextLocation;

        private const int LocationSize = 15;
        private const int TutorialIndex = 0;

        private void Start()
        {
            int currentLevel = SaveMaster.Instance.SaveFile.CurrentLevel;

            if (currentLevel == TutorialIndex)
            {
                return;
            }

            currentLevel--;

            int locationLevel = currentLevel % LocationSize;

            _currentLocation = currentLevel / LocationSize % _locations.Length;
            _nextLocation = (_currentLocation + 1) % _locations.Length;

            _levelLocation.sprite = _locations[_currentLocation];
            _currentLocationIcon.sprite = _locationsIcons[_currentLocation];
            _nextLocationIcon.sprite = _locationsIcons[_nextLocation];

            _bar.fillAmount = (float)(locationLevel - 1) / (LocationSize - 1);
            _bar.DOFillAmount((float)locationLevel / (LocationSize - 1), 0.33f);
        }
    }
}