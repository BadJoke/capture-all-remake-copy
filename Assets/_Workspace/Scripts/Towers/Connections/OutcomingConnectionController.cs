﻿using General;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Towers.Connections
{
    public class OutcomingConnectionController : MonoBehaviour
    {
        [SerializeField] private UnitTower _owner;
        [SerializeField] private Timer _timer;
        [SerializeField] private TowerLevelManager _towerLevelManager;
        [SerializeField] private Connection _connectionPrefab;
        [SerializeField] private LayerMask _wallMask; 

        private int _currentLimit;
        private Settings _settings;
        private Tower[] _towers;
        private List<Connection> _connections = new List<Connection>();

        private const int MaxLimit = 3;

        public event Action<int, int> CountChanged;

        public bool IsMaxConnection => _connections.Count == _currentLimit;
        public List<Connection> Connections => _connections;

        private bool CanDeactivateConnection => IsMaxConnection && _currentLimit > 1;

        private void OnEnable()
        {
            _timer.Targeting += OnTargeting;
            _towerLevelManager.LevelCountChanged += OnLevelCountChanged;
        }

        private void Start()
        {
            _settings = Settings.Instance;
            _towers = TowersController.GetTowers(_owner);
        }

        private void OnDisable()
        {
            _timer.Targeting -= OnTargeting;
            _towerLevelManager.LevelCountChanged -= OnLevelCountChanged;
        }

        public void ConnectTo(Tower target)
        {
            if (_connections.Exists(connection => connection.Target == target))
            {
                return;
            }

            if (IsMaxConnection)
            {
                _connections[0].Deactivate();
            }

            Connection connection = Instantiate(_connectionPrefab, transform);
            connection.Init(_owner, target);
            connection.Deactivated += OnConnectionDeactivated;
            
            target.TakeConnection(connection);

            _connections.Add(connection);
            CountChanged?.Invoke(_connections.Count, _currentLimit);
        }

        public void ChekConnection(Connection checkable)
        {
            List<Connection> connections = new List<Connection>(_connections);

            foreach (var connection in connections)
            {
                if (connection.Target == checkable.Owner)
                {
                    if (checkable.Owner.CurrentFraction == _owner.CurrentFraction)
                    {
                        _connections.Remove(connection);
                        connection.Deactivate();
                    }
                    else
                    {
                        connection.SetPair(checkable);
                        checkable.SetPair(connection);
                    }

                    break;
                }
            }
        }

        public void RemoveAll()
        {
            while(_connections.Count > 0)
            {
                _connections[0].Deactivate();
            }

            CountChanged?.Invoke(_connections.Count, _currentLimit);
        }

        private void OnLevelCountChanged(int levels)
        {
            _currentLimit = levels > MaxLimit ? MaxLimit : levels;

            while(_connections.Count > _currentLimit)
            {
                Connection connection = _connections[0];
                _connections.Remove(connection);
                connection.Deactivate();
            }

            CountChanged?.Invoke(_connections.Count, _currentLimit);
        }

        private void OnTargeting()
        {
            if (TargetingImpossibility())
            {
                return;
            }

            IEnumerable<Tower> reachableTargets = _towers.Where(tower => CheckReachable(tower));
            int count = reachableTargets.Count();

            if (count == 0)
            {
                return;
            }

            Tower target = reachableTargets.ElementAt(Random.Range(0, count));
            int index = _connections.FindIndex(connection => connection.Target == target);

            if (index == -1)
            {
                if (IsMaxConnection)
                {
                    _connections[0].Deactivate();
                }

                ConnectTo(target);
            }
            else
            {
                if (CanDeactivateConnection)
                {
                    _connections[index].Deactivate();
                }
            }
            
            bool TargetingImpossibility()
            {
                return _owner.CurrentFraction < Fraction.Enemy1 || _settings.IsFreePlay;
            }

            bool CheckReachable(Tower target)
            {
                Vector3 start = transform.position;
                Vector3 end = target.transform.position;
                bool hasDoor = false;

                bool hasWall = false;

                if (Physics.Linecast(start, end, out RaycastHit hit, _wallMask) && hit.transform != target.transform)
                {
                    hasWall = true;
                }

                if (target is UnitTower tower)
                {
                    hasDoor = tower.HasDoor;
                }

                return hasWall == false && hasDoor == false;
            }
        }

        private void OnConnectionDeactivated(Connection connection)
        {
            connection.Deactivated -= OnConnectionDeactivated;

            _connections.Remove(connection);

            CountChanged?.Invoke(_connections.Count, _currentLimit);
        }
    }
}