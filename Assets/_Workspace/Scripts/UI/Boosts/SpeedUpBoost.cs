﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Towers;
using UnityEngine;

namespace UI.Boosts
{
    public class SpeedUpBoost : Boost
    {
        [SerializeField] private float _modifier = 2f;
        [SerializeField] private float _duration = 5f;

        private bool _isActive = false;
        private WaitForSeconds _delay;
        private List<Tower> _playerTowers;
        private List<Tower> _otherTowers;
        
        private void Start()
        {
            _delay = new WaitForSeconds(_duration);
            
            _playerTowers = TowersController.GetTowersByFraction(Fraction.Player).ToList();
            _otherTowers = TowersController.GetTowersExceptFraction(Fraction.Player).ToList();

            foreach (Tower tower in _playerTowers)
            {
                tower.FractionChanged += OnPlayerTowerFractionChanged;
            }
            
            foreach (Tower tower in _otherTowers)
            {
                tower.FractionChanged += OnOtherTowerFractionChanged;
            }
        }
        
        public override void Activate()
        {
            StartCoroutine(SpeedUpRoutine());
        }

        private IEnumerator SpeedUpRoutine()
        {
            _isActive = true;
            
            foreach (Tower tower in _playerTowers)
            {
                tower.SetModifier(_modifier);
            }
            
            yield return _delay;
            
            _isActive = false;
            
            foreach (Tower tower in _playerTowers)
            {
                tower.ResetModifier();
            }
        }

        private void OnPlayerTowerFractionChanged(Fraction fraction, Tower tower)
        {
            _playerTowers.Remove(tower);
            _otherTowers.Add(tower);

            if (_isActive)
            {
                tower.ResetModifier();
            }
        }
        
        private void OnOtherTowerFractionChanged(Fraction fraction, Tower tower)
        {
            if (fraction != Fraction.Player)
            {
                return;
            }
            
            _otherTowers.Remove(tower);
            _playerTowers.Add(tower);
            
            if (_isActive )
            {
                tower.SetModifier(_modifier);
            }
        }
    }
}