﻿using Towers;
using Towers.Connections;
using UnityEngine;

namespace UI
{
    public class Tutorial : MonoBehaviour
    {
        [SerializeField] private OutcomingConnectionController _mainTower;
        [SerializeField] private TowerPower _doorTower;
        [SerializeField] private TutorialScreen[] _screens;
        [SerializeField] private UIHandler _handler;

        private bool _isFirstScreenPlayed = false;
        private bool _isSecondScreenPlayed = false;
        private bool _isThirdScreenPlayed = false;

        private void OnEnable()
        {
            _mainTower.CountChanged += OnCountChanged;
            _doorTower.FractionChanged += OnFractionChanged;
        }

        private void Awake()
        {
            _handler.GameStarted += OnGameStarted;
        }

        private void OnDisable()
        {
            _mainTower.CountChanged -= OnCountChanged;
            _doorTower.FractionChanged -= OnFractionChanged;
        }

        private void OnGameStarted()
        {
            _handler.GameStarted -= OnGameStarted;
            _screens[0].Activate();
        }

        private void OnCountChanged(int connectionCount, int limit)
        {
            if (connectionCount == 1 && _isFirstScreenPlayed == false)
            {
                _screens[0].Deactivate();
                _isFirstScreenPlayed = true;
            }

            if (connectionCount == 0 && _isFirstScreenPlayed)
            {
                _screens[1].Deactivate();
            }

            if (connectionCount == 0 && _isThirdScreenPlayed == false && _isSecondScreenPlayed)
            {
                _screens[2].Activate();
                _isThirdScreenPlayed = true;
            }

            if (connectionCount == 1 && _isThirdScreenPlayed)
            {
                _screens[2].Deactivate();
                gameObject.SetActive(false);
            }

            if (connectionCount == 2 && _isFirstScreenPlayed)
            {
                _screens[1].Deactivate();
                _isThirdScreenPlayed = true;
            }
        }

        private void OnFractionChanged(Fraction fraction)
        {
            _isSecondScreenPlayed = true;

            if (_mainTower.Connections.Count == 0)
            {
                _screens[2].Activate();
                _isThirdScreenPlayed = true;
                return;
            }

            _screens[1].Activate();
        }    
    }
}