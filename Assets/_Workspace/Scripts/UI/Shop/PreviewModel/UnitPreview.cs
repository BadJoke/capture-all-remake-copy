﻿using General;
using System.Linq;
using UnityEngine;

namespace UI.Shop
{
    public class UnitPreview : PreviewModel
    {
        private GameObject _newSkin;
        private GameObject _currentSkin;

        public override void Init()
        {
            base.Init();

            UnitButton button = Buttons.FirstOrDefault(
                b => ((UnitButton) b).SkinStickman == Settings.PlayerStickmanSkin) as UnitButton;

            if (button == null)
            {
                return;
            }
            
            CurrentButton = button;
            _newSkin = button.Skin;
            _currentSkin = button.Skin;

            if (_currentSkin)
            {
                _currentSkin.SetActive(true);
            }
        }

        public override bool IsCurrent()
        {
            return IsCurrent(CurrentButton);
        }

        public override bool IsOpen()
        {
            if (CurrentButton == null)
            {
                return false;
            }

            UnitButton button = CurrentButton as UnitButton;
            return button != null && button.IsOpen();
        }

        public override void SetValueToSettings()
        {
            Settings.PlayerStickmanSkin = ((UnitButton) CurrentButton).SkinStickman;
        }

        public override void AddToOpened()
        {
            SaveMaster.SaveFile.OpenedStickmanSkins[(int)((UnitButton) CurrentButton).SkinStickman] = true;
            SaveMaster.SaveData();
        }

        public void SetCurrentSkin()
        {
            PreviewButton unitButton =  Buttons.FirstOrDefault(
                button => ((UnitButton) button).SkinStickman == Settings.PlayerStickmanSkin);

            OnButtonClick(unitButton);
        }

        protected override bool IsCurrent(PreviewButton button)
        {
            if (button)
            {
                return Settings.PlayerStickmanSkin == ((UnitButton) button).SkinStickman;
            }

            return Settings.PlayerStickmanSkin == SkinStickman.Default;
        }

        protected override void DisableCurrent()
        {
            if (_currentSkin)
            {
                _currentSkin.SetActive(false);
            }
        }

        protected override void EnableNew()
        {
            if (_newSkin)
            {
                _newSkin.SetActive(true);
            }
            
            _currentSkin = _newSkin;
        }

        protected override void OnButtonClick(PreviewButton button)
        {
            UnitButton unitButton = button as UnitButton;

            if (CurrentButton != null)
            {
                CurrentButton.Deselect();
            }

            if (unitButton)
            {
                unitButton.Select();
            }

            CurrentButton = unitButton;

            _newSkin = unitButton ? unitButton.Skin : null;

            base.OnButtonClick(button);
        }
    }
}