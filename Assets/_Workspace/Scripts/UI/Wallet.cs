﻿using Savings;
using System;
using UnityEngine;

namespace UI
{
    public class Wallet : MonoBehaviour
    {
        [SerializeField] private Showman _showman;
        [SerializeField] private MoneyAnimator _moneyAnimator;

        private Save _saveFile;
        private SaveMaster _saveMaster;

        public event Action<int> MoneyChanged;

        public int Money { get; private set; }

        private void Awake()
        {
            _saveMaster = SaveMaster.Instance;
            LoadCoins();
        }

        public void Buy(int cost, bool showmanAnim)
        {
            if (showmanAnim)
            {
                _showman.PlayLevelUpAnimation();
            }

            _moneyAnimator.AnimatePay();
            ChangeCoinsCount(-cost, false);
        }

        public void ChangeCoinsCount(int amount, bool isAnimated)
        {
            int oldAmount = Money;
            Money += amount;

            _saveFile.Money = Money;
            _saveMaster.SaveData();

            if (isAnimated)
            {
                _moneyAnimator.AnimateGain();
            }

            _moneyAnimator.AnimateCoinText(oldAmount, Money);
            MoneyChanged?.Invoke(Money);
        }

        private void LoadCoins()
        {
            _saveFile = _saveMaster.SaveFile;
            Money = _saveFile.Money;
            _moneyAnimator.SetMoneyWithoutAnim(Money);
        }
    }
}