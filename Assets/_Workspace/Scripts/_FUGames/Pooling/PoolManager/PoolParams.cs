﻿using System;
using UnityEngine;

namespace FUGames.Pooling
{
    [Serializable]
    public struct PoolParams
    {
        [SerializeField] private int _startSize;
        [SerializeField] private PoolObject _template;

        public int StartSize => _startSize;
        public PoolObject Template => _template;
    }
}
