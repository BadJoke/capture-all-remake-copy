﻿using Towers.Connections;
using UnityEngine;

namespace Towers
{
    public class IncrementalPower : TowerPower
    {
        [SerializeField] private Timer _timer;
        [SerializeField] private OutcomingConnectionController _connectionController;

        protected new void Start()
        {
            _timer.Ticking += OnTicking;
            _timer.Stopped += OnStopped;

            base.Start();
        }

        private void OnTicking()
        {
            if (_connectionController.Connections.Count == 0)
            {
                Increase();
            }
        }

        private void OnStopped()
        {
            _timer.Ticking -= OnTicking;
            _timer.Stopped -= OnStopped;
        }
    }
}