﻿using UnityEngine;

namespace UI.Shop
{
    public class ShopModelController : MonoBehaviour
    {
        [SerializeField] private PreviewModel _unit;
        [SerializeField] private PreviewModel _tower;
        [SerializeField] private ShopButton _modelButton;

        private PreviewModel _currentModel;

        private void OnEnable()
        {
            _unit.Changed += OnModelChanged;
            _tower.Changed += OnModelChanged;
        }

        private void Start()
        {
            _currentModel = _unit;
            _unit.Init();
            _tower.Init();

            RefreshModels();
        }

        private void OnDisable()
        {
            _unit.Changed -= OnModelChanged;
            _tower.Changed -= OnModelChanged;
        }

        public void SetSelectedModelValue()
        {
            _currentModel.SetValueToSettings();

            RefreshModels();
        }

        public void AddToOpenedCurrentModel()
        {
            _currentModel.AddToOpened();
            SetSelectedModelValue();
        }

        public void SetUnitModel()
        {
            if (_currentModel == _unit && _unit.IsCurrent())
            {
                return;
            }

            (_unit as UnitPreview)?.SetCurrentSkin();

            OnModelChanged(_unit);
        }

        private void RefreshModels()
        {
            _unit.Refresh();
            _tower.Refresh();
        }

        private void OnModelChanged(PreviewModel model)
        {
            _modelButton.SetModel(model);
            _currentModel.Hide(model.Show);
            _currentModel = model;

            RefreshModels();
        }
    }
}