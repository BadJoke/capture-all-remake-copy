﻿using System;
using System.Collections.Generic;
using Towers;
using UnityEngine;

namespace General
{
    public class ScoreCalculator : MonoBehaviour
    {
        private bool _hasEnemy;
        private Tower[] _towers;

        public event Action<int, Dictionary<Fraction, int>> PowersChanged;
        public event Action Wone;
        public event Action Losed;

        private void Start()
        {
            _hasEnemy = false;
            _towers = TowersController.GetTowers(null);

            foreach (var tower in _towers)
            {
                if (tower.CurrentFraction == Fraction.Enemy1 || tower.CurrentFraction == Fraction.Enemy2)
                {
                    _hasEnemy = true;
                }

                tower.PowerChanged += OnTowerPowerChanged;
            }

            RecalculateProgress();
        }

        private void RecalculateProgress()
        {
            int totalPower = 0;
            int emptyPower = 0;

            Dictionary<Fraction, int> fractionPowers = new Dictionary<Fraction, int>();
            fractionPowers.Add(Fraction.Player, 0);

            foreach (var tower in _towers)
            {
                if (tower.CurrentFraction == Fraction.Empty)
                {
                    emptyPower += tower.Power;
                    continue;
                }

                if (fractionPowers.TryGetValue(tower.CurrentFraction, out int power))
                {
                    power += tower.Power;

                    fractionPowers.Remove(tower.CurrentFraction);
                    fractionPowers.Add(tower.CurrentFraction, power);
                }
                else
                {
                    fractionPowers.Add(tower.CurrentFraction, tower.Power);
                }

                totalPower += tower.Power;
            }

            totalPower += emptyPower;

            if (fractionPowers[Fraction.Player] == 0)
            {
                StopTowersListening();
                Losed?.Invoke();
            }

            int modifier = _hasEnemy ? emptyPower : 0;

            if (totalPower - modifier == fractionPowers[Fraction.Player])
            {
                LevelManager.Instance.SaveProgress();
                emptyPower = 0;
                StopTowersListening();
                Wone?.Invoke();
            }

            fractionPowers.Add(Fraction.Empty, emptyPower);

            PowersChanged?.Invoke(totalPower, fractionPowers);
        }

        private void StopTowersListening()
        {
            foreach (var tower in _towers)
            {
                tower.PowerChanged -= OnTowerPowerChanged;
            }
        }

        private void OnTowerPowerChanged(int power)
        {
            RecalculateProgress();
        }
    }
}
