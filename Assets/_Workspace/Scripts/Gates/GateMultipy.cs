using Units;
using UnityEngine;
using UnityEngine.UI;

namespace Gates
{
    public class GateMultipy : Gate
    {
        [Header("���������:")]
        [Range(2, 20)]
        [SerializeField] private int multiplier = 2;
        [Header("--------------------")]
        [SerializeField] private Text textComponent;

        private void OnValidate()
        {
            textComponent.text = "x" + multiplier;
        }

        public override void UnitEntered(Unit unit)
        {
            if (unit.IsMultiplied == false)
            {
                unit.Multiply(multiplier);
            }
        }
    }
}
