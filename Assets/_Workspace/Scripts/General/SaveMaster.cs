using UnityEngine;
using System;
using System.IO;

namespace Savings
{
    public class SaveMaster : MonoBehaviour
    {
        private string _path;

        public Action GameBeginToLoad;
        public Action GameLoaded;
        public Action GameBeginToSave;
        public Action GameSaved;
        
        public Save SaveFile { get; private set; }

        public static SaveMaster Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
            
            InitializePath();
            LoadData();
        }

        private void OnDestroy()
        {
            SaveData();
        }

        public void SaveData()
        {
            GameBeginToSave?.Invoke();

            File.WriteAllText(_path, JsonUtility.ToJson(SaveFile));

            GameSaved?.Invoke();
        }

        private void LoadData()
        {
            GameBeginToLoad?.Invoke();

            if (File.Exists(_path))
            {
                SaveFile = JsonUtility.FromJson<Save>(File.ReadAllText(_path));
            }
            else
            {
                SaveFile = new Save();
            }

            GameLoaded?.Invoke();
        }

        private void InitializePath()
        {
#if !UNITY_EDITOR
            _path = Path.Combine(Application.persistentDataPath, "Save.json");
#else
            _path = Path.Combine(Application.dataPath, "Save.json");
#endif
        }
    }
}

