using System;
using UnityEngine;
using Units;

namespace Gates
{
    public abstract class Gate : MonoBehaviour
    {
        [SerializeField] private float length = 0.5f, width = 2f;
        public float Length => length;
        public float Width => width;

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position + transform.forward * length / 2, transform.position - transform.forward * length / 2);
            Gizmos.DrawLine(transform.position + transform.right * width / 2, transform.position - transform.right * width / 2);
        }

        public abstract void UnitEntered(Unit unit);
    }
}
