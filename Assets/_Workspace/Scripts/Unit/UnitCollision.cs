﻿using Gates;
using System.Collections.Generic;
using Towers;
using UnityEngine;

namespace Units
{
    public class UnitCollision : MonoBehaviour
    {
        [SerializeField] private float _unitRadius = 0.5f;
        [SerializeField] private bool _effectsOnEnterIntoBuilding;

        private Unit _unit;
        private Gate _lastGate;

        public Gate[] Gates { get; private set; }
        public float UnitRadius => _unitRadius;

        private void Awake()
        {
            _unit = GetComponent<Unit>();
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, _unitRadius);
        }

        public void SetGates(Gate[] newGates)
        {
            Gates = newGates;
        }

        public void CheckCollision()
        {
            ComputeGatesCollision();
            ComputeTowerCollision();

            if (_unit.Target is UnitTower tower)
            {
                ComputeUnitCollision(tower);
            }
        }

        private void ComputeUnitCollision(UnitTower tower)
        {
            IEnumerable<Unit> enemyUnits = tower.Units;

            foreach (Unit enemy in enemyUnits)
            {
                if (CheckHitCondition(enemy))
                {
                    _unit.TakeDamage(enemy);
                    enemy.TakeDamage(_unit);
                    
                    break;
                }

                bool CheckHitCondition(Unit unit)
                {
                    return
                        unit.IsDied == false
                        && unit.Owner == _unit.Target
                        && unit.Target == _unit.Owner
                        && Mathf.Abs(unit.CoveredDistance - _unit.RemainingDistance) < _unitRadius;
                }
            }
        }

        private void ComputeGatesCollision()
        {
            foreach (Gate gate in Gates)
            {
                if (CheckGateInteraction(gate) == false)
                {
                    continue;
                }

                if (_lastGate == gate)
                {
                    return;
                }
                
                _lastGate = gate;
                gate.UnitEntered(_unit);

                return;
            }

            if (_lastGate)
            {
                _lastGate = null;
            }
        }

        private void ComputeTowerCollision()
        {
            if (CheckDistanceTo(_unit.Target.transform.position))
            {
                _unit.Target.TakeDamage(_unit);
                _unit.Die(_effectsOnEnterIntoBuilding);
            }
        }

        private bool CheckDistanceTo(Vector3 targetPos)
        {
            float magnitude = (targetPos - transform.position).sqrMagnitude;
            return magnitude < _unitRadius * _unitRadius;
        }

        private bool CheckGateInteraction(Gate theGate)
        {
            Vector3 dir = transform.position - theGate.transform.position;
            float projectionZ = Vector3.Project(dir, theGate.transform.forward).magnitude;
            float projectionX = Vector3.Project(dir, theGate.transform.right).magnitude;
            return projectionX < theGate.Width / 2 && projectionZ < theGate.Length / 2;
        }
    }
}