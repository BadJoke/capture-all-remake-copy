﻿using System;
using DG.Tweening;
using UnityEngine;

namespace UI.Shop
{
    public class Shop : MonoBehaviour
    {
        [SerializeField] private AnimationClip _showClip;
        [SerializeField] private AnimationClip _hideClip;
        [SerializeField] private Animation _animation;
        [SerializeField] private RectTransform _renderTexture;
        [SerializeField] private GameObject _shopObject;

        private const float MenuSize = 1f;
        private const float MenuPosition = 179f;
        private const float ShopSize = 0.5f;
        private const float ShopPosition = 479f;

        public event Action Showed; 
        public event Action Hided; 

        private void Start()
        {
            _showClip.legacy = true;
            _hideClip.legacy = true;
        }

        public void Show()
        {
            _shopObject.SetActive(true);
            _renderTexture.DOScale(ShopSize, 0.15f);
            _renderTexture.DOAnchorPosY(ShopPosition, 0.15f);

            _animation.clip = _showClip;
            _animation.Play();
            
            Showed?.Invoke();
        }

        public void Hide()
        {
            _renderTexture.DOScale(MenuSize, 0.15f);
            _renderTexture.DOAnchorPosY(MenuPosition, 0.15f);

            _animation.clip = _hideClip;
            _animation.Play();
            
            Hided?.Invoke();
        }

        private void Deactivate()
        {
            _shopObject.SetActive(false);
        }
    }
}
