using FUGames.Pooling;
using General;
using UnityEngine;
using DeathEffects;

namespace Units
{
    public class Stickman : Unit
    {
        [SerializeField] private UnitSkin _skin;

        public UnitSkin Skin => _skin;

        protected override void SpecialInitialize()
        {
            _skin.SetScale(0);
            _skin.SetScaleAnimated(1, Random.Range(0.1f, 0.4f));
            _skin.ForceClearRelativePosition();
            _skin.UpdateColor(Fraction);
            _skin.RefreshSkin(this);
        }

        public override void Multiply(int multiplier)
        {
            if (multiplier < 10)
            {
                _skin.SetRelativePosition(0);

                for (int i = 1; i < multiplier; i++)
                {
                    Stickman newOne = PoolManager.Instance.Take<Stickman>();
                    float relativePos = 0;

                    if (i == multiplier - 1 && i % 2 == 1)
                        relativePos = 0;
                    else if (i % 2 == 1)
                        relativePos = UnitRadius;
                    else
                        relativePos = -UnitRadius;

                    Vector3 pos = transform.position - transform.forward * (1 + (i - 1) / 2) * UnitRadius;
                    newOne.InitializeClone(this, pos, true);
                    newOne.Skin.SetRelativePosition(relativePos);
                }
            }
            else
            {
                for (int i = 1; i < multiplier; i++)
                {
                    Stickman newOne = PoolManager.Instance.Take<Stickman>();
                    float relativePos = Mathf.Lerp(-UnitRadius, UnitRadius, Random.Range(0, 1f))*1.5f;
                    Vector3 pos = transform.position - (transform.forward * i / 2) * UnitRadius;
                    newOne.InitializeClone(this, pos, true);
                    newOne.Skin.SetRelativePosition(relativePos);
                }
            }
        }

        private void PlayDeathEffect()
        {
            DeathEffect deathEffect = PoolManager.Instance.Take<StickmanDE>();
            deathEffect.StartCountdown();
            deathEffect.transform.position = transform.position + Vector3.up;
            var main = deathEffect.PS.main;
            main.startColor = ColorManager.Instance.GetColorKit(Fraction).Unit.color;
        }

        public override void TakeDamage(IDamager damager)
        {
            if (damager.Fraction != Fraction)
            {
                int damage = damager.Damage;

                if (damager is StickmanGroup)
                {
                    damage /= 2;
                }

                Lives -= damage;
                if (Lives < 1)
                {
                    Die(true);
                }
            }
        }

        public override void Die(bool withEffects)
        {
            if (IsDied)
                return;

            IsDied = true;

            if (withEffects)
                PlayDeathEffect();

            CallDiedEvent();
            BackToPool();
        }
    }
}