using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Boosts
{
    [RequireComponent(typeof(CustomButton))]
    public class BoostUiBuyer : MonoBehaviour
    {
        [SerializeField] private int _cost = 1000;
        [SerializeField] private Text _costDisplay;
        [SerializeField] private Text _countDisplay;
        [SerializeField] private Wallet _wallet;
        [SerializeField] private Boost _boost;
        [SerializeField] private Advertizer _advertizer;
        [SerializeField] private GameObject _costObject;
        [SerializeField] private GameObject _adObject;

        private bool _isAdState = false;
        private bool _isOfferSent = false;
        private CustomButton _button;

        public event Action<AdRewardType> Added;

        public AdRewardType BoostType => _boost.RewardType;

        private void Awake()
        {
            _button = GetComponent<CustomButton>();
        }

        private void OnEnable()
        {
            _wallet.MoneyChanged += OnWalletMoneyChanged;
        }

        private void OnDisable()
        {
            _wallet.MoneyChanged -= OnWalletMoneyChanged;
        }

        private void Start()
        {
            _costDisplay.text = _cost.ToString();
            
            SetView(_cost > _wallet.Money);
        }

        public void SetCount(int value)
        {
            _countDisplay.text = value.ToString();
        }
        
        public void OnClick()
        {
            if (_isAdState)
            {
                _advertizer.ShowBoostAd(_boost.RewardType);
                _advertizer.BoostTaken += OnBoostTaken;
                _button.Deactivate();
            }
            else
            {
                _wallet.Buy(_cost, true);
                Added?.Invoke(_boost.RewardType);
            }
        }

        private void SetView(bool isAd)
        {
            if (isAd == _isAdState)
            {
                return;
            }

            _isAdState = isAd;
            
            _costObject.SetActive(isAd == false);
            _adObject.SetActive(isAd);

            if (isAd && _isOfferSent == false)
            {
                _isOfferSent = true;
                _advertizer.BoostAdOffer(_boost.RewardType);
            }
        }
        
        private void OnWalletMoneyChanged(int amount)
        {
            SetView(_cost > amount);
        }

        private void OnBoostTaken(AdRewardType rewardType)
        {
            if (rewardType != _boost.RewardType)
            {
                return;
            }
            
            _advertizer.BoostTaken -= OnBoostTaken;
            
            Added?.Invoke(rewardType);
        }
    }
}