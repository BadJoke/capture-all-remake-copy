﻿using System;
using UnityEngine;

namespace UI.Boosts
{
    public abstract class Boost : MonoBehaviour
    {
        [SerializeField] private BoostType _type;

        public AdRewardType RewardType
        {
            get
            {
                return _type switch
                {
                    BoostType.Freeze => AdRewardType.boost_freeze,
                    BoostType.SpeedUp => AdRewardType.boost_speed_up,
                    BoostType.TowerPoints => AdRewardType.boost_tower_count,
                    _ => throw new ArgumentOutOfRangeException()
                };
            }
        }

        public abstract void Activate();
    }
}