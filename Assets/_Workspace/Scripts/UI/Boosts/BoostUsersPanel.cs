using DG.Tweening;
using UnityEngine;

namespace UI.Boosts
{
    public class BoostUsersPanel : MonoBehaviour
    {
        [SerializeField] private float _yPosition = 350f;
        [SerializeField] private float _duration = 1f;
        
        public void Show()
        {
            gameObject.SetActive(true);
            ((RectTransform) transform).DOAnchorPosY(_yPosition, _duration);
        }
        
        public void Hide()
        {
            ((RectTransform) transform).DOAnchorPosY(-_yPosition, _duration)
                .onComplete += () => {  gameObject.SetActive(false); };
        }
    }
}