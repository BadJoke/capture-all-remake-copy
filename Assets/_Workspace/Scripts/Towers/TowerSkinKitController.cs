﻿using General;
using UnityEngine;

namespace Towers
{
    public class TowerSkinKitController : MonoBehaviour
    {
        [SerializeField] private TowerSkinKit[] _kits;

        private Settings _settings;

        private void Start()
        {
            _settings = Settings.Instance;
        }

        public TowerSkinKit GetKit(Fraction fraction, TowerType type)
        {
            TowerSkin skin = TowerSkin.Default;

            if (fraction == Fraction.Empty && type == TowerType.Default)
            {
                skin = _settings.EmptyDefaultTower;
            }
            else if (fraction == Fraction.Empty && type == TowerType.Shooting)
            {
                skin = _settings.EmptyShootingTower;
            }
            else if (fraction == Fraction.Empty && type == TowerType.Attacking)
            {
                skin = _settings.EmptyAttackingTower;
            }
            else if (fraction == Fraction.Empty && type == TowerType.Defending)
            {
                skin = _settings.EmptyDefendingTower;
            }

            if (fraction == Fraction.Player && type == TowerType.Default)
            {
                skin = _settings.PlayerDefaultTower;
            }
            else if (fraction == Fraction.Player && type == TowerType.Shooting)
            {
                skin = _settings.PlayerShootingTower;
            }
            else if (fraction == Fraction.Player && type == TowerType.Attacking)
            {
                skin = _settings.PlayerAttackingTower;
            }
            else if (fraction == Fraction.Player && type == TowerType.Defending)
            {
                skin = _settings.PlayerDefendingTower;
            }

            if (fraction == Fraction.Enemy1 && type == TowerType.Default)
            {
                skin = _settings.Enemy1DefaultTower;
            }
            else if (fraction == Fraction.Enemy1 && type == TowerType.Shooting)
            {
                skin = _settings.Enemy1ShootingTower;
            }
            else if (fraction == Fraction.Enemy1 && type == TowerType.Attacking)
            {
                skin = _settings.Enemy1AttackingTower;
            }
            else if (fraction == Fraction.Enemy1 && type == TowerType.Defending)
            {
                skin = _settings.Enemy1DefendingTower;
            }

            if (fraction == Fraction.Enemy2 && type == TowerType.Default)
            {
                skin = _settings.Enemy2DefaultTower;
            }
            else if (fraction == Fraction.Enemy2 && type == TowerType.Shooting)
            {
                skin = _settings.Enemy2ShootingTower;
            }
            else if (fraction == Fraction.Enemy2 && type == TowerType.Attacking)
            {
                skin = _settings.Enemy2AttackingTower;
            }
            else if (fraction == Fraction.Enemy2 && type == TowerType.Defending)
            {
                skin = _settings.Enemy2DefendingTower;
            }

            return _kits[(int)skin];
        }
    }
}