using FUGames.Pooling;
using System.Collections;
using UnityEngine;

public class FlyingCoin : PoolObject
{
    [SerializeField] private AnimationCurve _sizeAnimCurve;

    private Vector3 _startPos;
    private Vector3 _targetPos;
    private float _duration;

    public void Initialize(Vector3 startWorldSpace, Vector3 finishWorldSpace, float duration, Transform canvas)
    {
        transform.SetParent(canvas);
        transform.position = startWorldSpace;
        
        _startPos = startWorldSpace;
        _targetPos = finishWorldSpace;
        _duration = duration;

        StartCoroutine(Moving());
    }

    private IEnumerator Moving()
    {
        float progress = 0f;

        while (progress < 1f)
        {
            SetValues(progress);

            yield return null;

            progress += Time.deltaTime / _duration;
        }
        
        SetValues(1f);
        BackToPool();
    }

    private void SetValues(float progress)
    {
        transform.localScale = Vector3.one * _sizeAnimCurve.Evaluate(progress);
        transform.position = Vector3.Lerp(_startPos, _targetPos, progress);
    }
}
