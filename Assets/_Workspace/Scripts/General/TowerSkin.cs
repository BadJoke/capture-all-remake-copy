﻿using System;

namespace General
{
    [Serializable]
    public enum TowerSkin
    {
        Default,
        Light,
        Dark
    }
}

