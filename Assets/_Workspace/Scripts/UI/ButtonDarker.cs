using UnityEngine;

public class ButtonDarker : MonoBehaviour
{
    [SerializeField] private CustomButton _button;
    [SerializeField] private GameObject[] _images;

    private void OnEnable()
    {
        _button.Activated += OnButtonActivated;
        _button.Deactivated += OnButtonDeactivated;
    }

    private void OnDisable()
    {
        _button.Activated -= OnButtonActivated;
        _button.Deactivated -= OnButtonDeactivated;
    }

    private void SetActive(bool active)
    {
        foreach (GameObject image in _images)
        {
            image.SetActive(active);
        }
    }
    
    private void OnButtonDeactivated()
    {
        SetActive(true);
    }

    private void OnButtonActivated()
    {
        SetActive(false);
    }
}