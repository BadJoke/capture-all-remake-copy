﻿using Towers;
using UnityEngine;

namespace Player
{
    public class SelectionLine : MonoBehaviour
    {
        [SerializeField] private LineRenderer _line;
        [SerializeField] private SpriteRenderer _start;
        [SerializeField] private SpriteRenderer _end;
        [SerializeField] private Material _default;
        [SerializeField] private Material _blocked;
        [SerializeField] private LayerMask _wallMask;

        private bool _isBlocked = false;
        private UnitTower _owner;
        private Transform _target;
        private Vector3 _lineOffset = new Vector3(0f, 0.05f, 0f);
        private Vector3[] _zeroSelection =
        {
            Vector3.zero,
            Vector3.zero
        };

        public void SetStart(UnitTower owner)
        {
            _owner = owner;

            var startPosition = _owner.transform.position + _lineOffset;

            _line.SetPosition(0, startPosition);
            _start.transform.position = startPosition;
            _end.transform.position = startPosition;

            _start.enabled = true;
            _end.enabled = true;
        }

        public bool SetPosition(Vector3 position, Transform target)
        {
            _target = target;
            position += _lineOffset;

            _line.SetPosition(1, position);
            _end.transform.position = position;

            return CheckWay(position);
        }

        public void SetDefault()
        {
            _start.enabled = false;
            _end.enabled = false;
            _isBlocked = false;
            _line.SetPositions(_zeroSelection);

            _line.material = _default;
        }

        private bool CheckWay(Vector3 position)
        {
            Vector3 start = _line.GetPosition(0);

            if (CheckBlocking(position, start))
            {
                if (_isBlocked)
                {
                    return false;
                }

                _isBlocked = true;
                _line.material = _blocked;

                return false;
            }
            else
            {
                if (_isBlocked)
                {
                    _isBlocked = false;
                    _line.material = _default;
                }

                return true;
            }

            bool CheckBlocking(Vector3 position, Vector3 start)
            {
                return (Physics.Linecast(start, position, out RaycastHit hit, _wallMask.value) && hit.transform != _target) || _owner.IsMaxConnections;
            }
        }
    }
}